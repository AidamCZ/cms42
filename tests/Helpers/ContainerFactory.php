<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Test\Helpers;

use Nette\Configurator;
use Nette\DI\Container;

class ContainerFactory
{

    /** @var Container */
    private static $container;

    final public static function getContainer(bool $new = false): Container
    {
        return (!$new && self::$container !== null) ? self::$container : self::create();
    }

    private static function create(): Container
    {
        $configurator = new Configurator();
        $configurator->setDebugMode(true);
        $configurator->enableTracy(__DIR__ . '/../../log');
        $configurator->setTempDirectory(__DIR__ . '/../_temp');
        $configurator->addParameters([
            'appDir' => __DIR__ . '/../../app',
            'wwwDir' => __DIR__ . '/../../www', //Because of CLI errors (bin/console path)
            'pluginsDir' => __DIR__ . '/../../plugins',
            'console' => __DIR__ . '/../bin/console',
            'rootDir' => __DIR__ . '/../..',
        ]);
        $configurator->addConfig(__DIR__ . '/../../app/config/config.neon');
        $configurator->addConfig(__DIR__ . '/../../app/config/config.local.neon');
        $configurator->addConfig(__DIR__ . '/../config/tests.neon');

//        $loader = require __DIR__ . '/../../vendor/autoload.php';
//        $configurator->onCompile[] = function ($_, \Nette\DI\Compiler $compiler) use ($loader) {
//            $compiler->addExtension('plugin', new \Plugins\DI\PluginsRegistrarExtension(__DIR__ . '/../../www', $loader));
//        };

        self::$container = $configurator->createContainer();
        return self::$container;
    }

}
