<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

require_once __DIR__ . '/../vendor/autoload.php';


Tester\Environment::setup();
define('TEMP_DIR', __DIR__ . '/_temp/cache/' . (isset($_SERVER['argv']) ? md5(serialize($_SERVER['argv'])) : getmypid()));
date_default_timezone_set('Europe/Prague');
//Tester\Helpers::purge(TEMP_DIR);
@chmod(TEMP_DIR, 0777);
$_SERVER['REQUEST_TIME'] = 1234567890;
$_ENV = $_GET = $_POST = $_FILES = [];
