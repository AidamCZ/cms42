<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Test\Mocks;

use Kdyby\Doctrine\Connection;

/**
 * Class ConnectionMock
 * @package Test\Mocks
 * @method onConnect(ConnectionMock $self)
 * @deprecated
 */
class ConnectionMock extends Connection
{

    /** @var callable[] @deprecated */
    public $onConnect = [];

    /** @deprecated */
    public function connect(): void
    {
        parent::connect();
        if ($this->onConnect) {
            $this->onConnect($this);
        }
    }

}
