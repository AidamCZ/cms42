<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Test\Mocks;

use Nette\Application\IResponse;
use Nette\Application\Request;
use Nette\Application\UI\Presenter;

/** @method onStartup(PresenterMock $self) */
class PresenterMock extends Presenter
{

    /** @var callable[] */
    public $onStartup = [];

    public function run(Request $request): ?IResponse
    {
        $this->autoCanonicalize = FALSE;
        return parent::run($request);
    }

    public function startup(): void
    {
        if ($this->getParameter('__terminate') === TRUE) {
            $this->terminate();
        }
        parent::startup();
        $this->onStartup($this);
    }

    public function afterRender(): void
    {
        $this->terminate();
    }

    public function isAjax(): bool
    {
        return FALSE;
    }

    /**
     * @inheritdoc
     */
    public function link($destination, $args = []): string
    {
        if (!\is_array($args)) {
            $args = \array_slice(\func_get_args(), 1);
        }
        $params = urldecode(http_build_query($args, '', ', '));
        $params = $params ? "($params)" : '';
        return "plink|$destination$params";
    }

}
