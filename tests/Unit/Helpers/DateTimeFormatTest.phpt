<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Test\Unit\Helpers;

use App\Filters\DateTimeFormat;
use DateTimeImmutable;
use DateTimeZone;
use Mockery;
use Settings\SettingsFacade;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../../bootstrap.php';

/** @testCase */
class DateTimeFormatTest extends TestCase
{

    private const OPTION_DATETIME_FORMAT = 'datetime_format',
        OPTION_TIMEZONE = 'timezone';

    /** @var DateTimeFormat */
    private $filter;

    public function testCustomFormat(): void
    {
        $settings = Mockery::mock(SettingsFacade::class);
        $settings
            ->shouldReceive('getOptionValue')
            ->withArgs([self::OPTION_DATETIME_FORMAT])
            ->andReturn('Y H:i-s');
        $settings
            ->shouldReceive('getOptionValue')
            ->withArgs([self::OPTION_TIMEZONE])
            ->andReturn('Pacific/Wallis');
        $this->filter = new DateTimeFormat($settings);
        $formatted = $this->filter->__invoke(new DateTimeImmutable('1984-02-05 05:59:31', new DateTimeZone('UTC')));
        Assert::same($formatted, '1984 17:59-31');
    }

}

(new DateTimeFormatTest())->run();
