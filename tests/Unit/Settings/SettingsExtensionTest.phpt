<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Test\Unit\Settings;

use Settings\SettingsFacade;
use Test\Helpers\ContainerFactory;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../../bootstrap.php';

/**
 * @testCase
 */
class SettingsExtensionTest extends TestCase
{

    public function testSettingsInjection(): void
    {
        $settingsFacade = ContainerFactory::getContainer()->getByType(SettingsFacade::class);
        Assert::type('Settings\SettingsFacade', $settingsFacade);
    }

}

(new SettingsExtensionTest())->run();
