<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Test\Unit\Plugins\Remove;

use App\Helpers\FileSystem;
use Mockery;
use Plugins\AllPluginsContainer;
use Plugins\Install\Installer;
use Plugins\Plugin;
use Plugins\Remove\Remover;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../../../bootstrap.php';

/**
 * @testCase
 */
class RemoverTest extends TestCase
{

    private const PLUGINS_ARRAY = [
        [1, 'Medvídek', 'medvidek', 'puu', 'Medvídek Pú je hlavní postava – personifikovaný medvídek v dětské knize', 'Walt Disney', '0'],
        [20, 'Oslík ze Shreka', 'shrek', 'oslik', 'Už tam budem?', 'Někdo', '2001'],
    ],
        PLUGINS_DIR_MOCK = __DIR__ . '/plugins',
        PLUGINS_SETUP_DIR = __DIR__ . '/plugins-setup';

    /** @var Plugin[] */
    private $plugins;

    /** @var Remover */
    private $remover;

    /** @var AllPluginsContainer */
    private $allPluginsContainer;

    /** @var Installer */
    private $installer;

    protected function setUp(): void
    {
        foreach (self::PLUGINS_ARRAY as $item) {
            $plugin = new Plugin();
            $plugin->setId($item[0]);
            $plugin->setName($item[1]);
            $plugin->setIdentifier($item[2], $item[3]);
            $plugin->setDescription($item[4]);
            $plugin->setAuthor($item[5]);
            $plugin->setVersion($item[6]);
            $this->plugins[$plugin->getIdentifier()] = $plugin;
        }
        $this->allPluginsContainer = new AllPluginsContainer();
        $this->allPluginsContainer->setAllPlugins($this->plugins);

        $this->installer = Mockery::mock(Installer::class);
        $this->installer->shouldReceive('uninstall')->once();

        $this->remover = new Remover(self::PLUGINS_DIR_MOCK, $this->installer, $this->allPluginsContainer);

        FileSystem::copy(self::PLUGINS_SETUP_DIR, self::PLUGINS_DIR_MOCK);
    }

    protected function tearDown(): void
    {
        FileSystem::purge(self::PLUGINS_DIR_MOCK);
    }

    public function testSuccessRemove(): void
    {
        $pluginCount = \count($this->plugins);
        Assert::equal($pluginCount, $this->countDownloadedPlugins());
        foreach ($this->plugins as $plugin) {
            $this->remover->remove($plugin->getIdentifier());
            $pluginCount--;
            Assert::equal($pluginCount, $this->countDownloadedPlugins());
            Assert::false(file_exists(self::PLUGINS_DIR_MOCK . '/' . $plugin->getIdentifier()));
        }
    }

    private function countDownloadedPlugins(): int
    {
        return \count($this->getDownloadedPlugins());
    }

    /** @return string[] */
    private function getDownloadedPlugins(): array
    {
        return array_diff(scandir(self::PLUGINS_DIR_MOCK, \SCANDIR_SORT_NONE), ['.', '..']);
    }

}

(new RemoverTest())->run();
