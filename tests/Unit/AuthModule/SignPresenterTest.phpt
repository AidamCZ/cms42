<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Test\Unit\AuthModule;

require __DIR__ . '/../../bootstrap.php';

use Nette\Application\Responses\RedirectResponse;
use Nette\Application\Responses\TextResponse;
use Nette\Http\IResponse;
use Test\Traits\TPresenter;
use Tester\Assert;
use Tester\DomQuery;
use Tester\TestCase;

/**
 * @testCase
 */
class SignPresenterTest extends TestCase
{

    use TPresenter;

    public function testRenderIn(): void
    {
        /** @var IResponse|TextResponse $response */
        $response = $this->checkAction('Auth:Sign:in');

        $html = (string)$response->getSource();
        $title = DomQuery::fromHtml($html)->find('title')[0];
        Assert::contains('Přihlásit se', (string)$title);
    }

    public function testSignIn(): void
    {
        /** @var RedirectResponse $response */
        $response = $this->checkForm('Auth:Sign:in', 'POST', [
            'username' => 'user',
            'password' => 'user',
            'remember' => '0',
            '_do' => 'signIn-signInForm-submit',
            '_submit' => 'Přihlásit',
        ]);
        Assert::notContains('sign/in', $response->getUrl());
    }

}

(new SignPresenterTest())->run();
