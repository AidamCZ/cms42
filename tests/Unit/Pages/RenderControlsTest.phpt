<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Test\Unit\Pages;

use Contact\Components\ContactForm\IContactFormFactory;
use Pages\Exceptions\PresenterIsNotSet;
use Pages\Helpers\RenderControls;
use Settings\Components\SettingsForm\ISettingsFormFactory;
use Test\Helpers\ContainerFactory;
use Test\Traits\TPresenter;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../../bootstrap.php';

/**
 * @testCase
 */
class RenderControlsTest extends TestCase
{

    use TPresenter;

    /** @var RenderControls */
    private $renderControls;

    public function __construct()
    {
        $container = ContainerFactory::getContainer();
        $this->renderControls = $container->getByType(RenderControls::class);
    }

    public function testRenderPresenterIsNotSetException(): void
    {
        Assert::exception(function (): void {
            $this->renderControls->render('');
        }, PresenterIsNotSet::class);
    }

    public function testRenderNothingBadSyntax(): void
    {
        $this->renderControls->setPresenter($this->createPresenter('Pages:Front:Pages'));
        $result = $this->renderControls->render('{control ' . md5(IContactFormFactory::class) . '}');
        Assert::same('{control ' . md5(IContactFormFactory::class) . '}', $result);
    }

    public function testRenderNothingNotRenderable(): void
    {
        $this->renderControls->setPresenter($this->createPresenter('Pages:Front:Pages'));
        $result = $this->renderControls->render('[__' . md5(ISettingsFormFactory::class) . '__]');
        Assert::same('[__' . md5(ISettingsFormFactory::class) . '__]', $result);
    }

}

(new RenderControlsTest())->run();
