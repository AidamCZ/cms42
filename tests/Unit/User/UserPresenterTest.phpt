<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Test\Unit\User;

use Nette\Application\IResponse;
use Nette\Application\Responses\TextResponse;
use Test\Traits\TPresenter;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../../bootstrap.php';

/**
 * @testCase
 */
class UserPresenterTest extends TestCase
{

    use TPresenter;

    protected function setUp(): void
    {
        $this->fakeLogin(1, 'admin');
    }

    public function testRenderDefault(): void
    {
        /** @var IResponse|TextResponse $response */
        $response = $this->checkAction('Users:Admin:Users:');
        $html = (string)$response->getSource();
        Assert::contains('<div class="user-grid', $html);
    }

}

(new UserPresenterTest())->run();
