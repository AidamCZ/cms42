<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Test\Unit\Components;

use Settings\Components\SettingsBar\SettingsBar;
use Test\Traits\TComponent;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../../bootstrap.php';

/**
 * @testCase
 */
class SettingsBarTest extends TestCase
{

    use TComponent;

    public function testRender(): void
    {
        $settingsBar = new SettingsBar([
            'permissions' => 'Permice',
            'default' => 'Default',
            ':Dashboard:Admin:Dashboard:' => 'DashBoard',
        ]);
        $html = $this->renderOutput($settingsBar);
        Assert::contains('title="Settings - Default" class="settings-bar__item">Default</a>', $html);
        Assert::contains('title="Settings - Permice" class="settings-bar__item">Permice</a>', $html);
        Assert::contains('title="Settings - DashBoard" class="settings-bar__item">DashBoard</a>', $html);
    }

}

(new SettingsBarTest())->run();
