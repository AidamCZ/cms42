<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Test\Unit\Components;

use Navigation\Components\AdminMenu\AdminMenu;
use Test\Traits\TComponent;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../../bootstrap.php';

/**
 * @testCase
 */
class AdminMenuTest extends TestCase
{

    use TComponent;

    public function testRender(): void
    {
        $mainMenu = new AdminMenu();
        $html = $this->renderOutput($mainMenu);
        Assert::contains('<ul class="sidebar-menu scrollable pos-r">
    <li class="nav-item mT-30 active">
        <a class="sidebar-link" href="', $html);
    }

}

(new AdminMenuTest())->run();
