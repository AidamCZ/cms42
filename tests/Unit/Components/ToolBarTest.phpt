<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Test\Unit\Components;

use App\Components\Admin\ToolBar\ToolBar;
use Kdyby\Doctrine\EntityRepository;
use Mockery;
use Test\Traits\TComponent;
use Test\Traits\TPresenter;
use Tester\Assert;
use Tester\TestCase;
use Users\User;

require __DIR__ . '/../../bootstrap.php';

/**
 * @testCase
 */
class ToolBarTest extends TestCase
{

    use TComponent;
    use TPresenter;

    public function testRenderGuest(): void
    {
//        $em = Mockery::mock(EntityManager::class);

        $control = new ToolBar();
        $html = $this->renderOutput($control);
        Assert::equal('', $html);
    }

    public function testRenderAdmin(): void
    {
        $this->fakeLogin(1, 'user', ['username' => 'user1']);

        $userRepository = Mockery::mock(EntityRepository::class);
        $userRepository
            ->shouldReceive('findOneBy')
            ->once()
            ->andReturn(new User('user1', 'user1user1'));

//        $em = Mockery::mock(EntityManager::class);
//        $em->shouldReceive('getRepository')
//            ->once()
//            ->andReturn($userRepository);

        $control = new ToolBar();
        $html = $this->renderOutput($control);

        Assert::contains(
            '<div class="row">
            <div class="col-auto text-left">
                <a class="text-white" href="plink|:Dashboard:Admin:Dashboard:" data-ajax="false" title="Administration | CMS42">Administration</a>
            </div>
            <div class="col text-right">
                <span class="text-white">Logged as user1</span>
            </div>
        </div>
    </div>',
            $html
        );
    }

}

(new ToolBarTest())->run();
