<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Test\Unit\Components;

use Test\Helpers\ContainerFactory;
use Test\Traits\TComponent;
use Tester\Assert;
use Tester\TestCase;
use Users\Components\SignInForm\ISignInFormFactory;

require __DIR__ . '/../../bootstrap.php';

/**
 * @testCase
 */
class SignFormTest extends TestCase
{

    use TComponent;

    public function testRender(): void
    {
        $container = ContainerFactory::getContainer();
        $control = $container->getByType(ISignInFormFactory::class);
        $html = $this->renderOutput($control->create());
        Assert::contains('<form action="plink|this" method="post" id="frm-SignInForm-signInForm"', $html);
    }

}

(new SignFormTest())->run();
