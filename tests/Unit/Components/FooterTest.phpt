<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Test\Unit\Components;

require __DIR__ . '/../../bootstrap.php';

use App\Components\Footer\Footer;
use Test\Traits\TComponent;
use Tester\Assert;
use Tester\TestCase;

/**
 * @testCase
 */
class FooterTest extends TestCase
{

    use TComponent;

    public function testFooterRender(): void
    {
        $control = new Footer();
        $html = $this->renderOutput($control);
        Assert::contains(date('Y'), $html);
    }

}

(new FooterTest())->run();
