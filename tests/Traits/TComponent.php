<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Test\Traits;

require __DIR__ . '/../Mocks/PresenterMock.php';

use Nette\ComponentModel\IComponent;
use Test\Helpers\ContainerFactory;
use Test\Mocks\ApplicationRequestMock;
use Test\Mocks\PresenterMock;
use Tester\Assert;

/**
 * Trait TComponent
 * @package Test\Helpers
 */
trait TComponent
{

    /** @var null|PresenterMock */
    private $presenterMock;

    /**
     * @param array|mixed[] $parameters
     * @throws \Exception
     */
    protected function renderOutput(IComponent $control, ?string $expected = null, array $parameters = []): string
    {
        ob_start();
        if (!$control->getParent()) {
            $this->attachPresenter($control);
        }
        $control->render(...$parameters);
        $output = ob_get_clean();
        if ($expected) {
            Assert::match($expected, $output);
        }

        return $output;
    }

    private function attachPresenter(IComponent $control, ?string $name = null): void
    {
        if ($name === null) {
            $name = $control->getReflection()->getShortName();
//                if (preg_match('~class@anonymous.*~', $name)) {
//                    $name = md5($name);
//                }
        }
        $this->presenterMock = new PresenterMock();

        ContainerFactory::getContainer()->callInjects($this->presenterMock);
        $this->presenterMock->onStartup[] = function (PresenterMock $presenter) use ($control, $name): void {
            try {
                $presenter->removeComponent($control);
            } catch (\InvalidArgumentException $exception) {
            }
            $presenter->addComponent($control, $name);
        };
        $this->presenterMock->run(new ApplicationRequestMock());
    }

}
