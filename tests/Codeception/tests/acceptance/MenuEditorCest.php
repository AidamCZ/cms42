<?php declare(strict_types=1);

use Codeception\Util\Locator;

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

class MenuEditorCest
{

    public function _before(AcceptanceTester $I): void
    {
        $I->loginAsRoot();
    }

    public function testMenuEdit(AcceptanceTester $I): void
    {
        $I->amOnPage('/admin/menu');
        $I->dontSee('testName');

        //Create testName
        $I->click('New Menu');
        $I->seeCurrentUrlEquals('/admin/menu/new');
        $I->fillField('name', 'testName');
        $I->click('Save');

        //Check if is created
        $I->dontSeeInCurrentUrl('new');
        $I->see('testName');

        //Check that cannot create duplicate
        $I->amOnPage('/admin/menu/new');
        $I->fillField('name', 'testName');
        $I->click('Save');
        $I->see('Menu with this name (testName) already exists');

        //Cannot save empty
        $I->fillField('name', '');
        $I->click('Save');
        $I->see('This field is required.');
        $I->seeInCurrentUrl('/admin/menu/new');

        //Back button
        $I->click('Back to Menu list');

        //Edit
        $I->click(Locator::lastElement('.col-md-4.card.text-dark.mb-3 a.menu-list__edit'));
        $I->seeInCurrentUrl('/admin/menu/edit');
        $I->fillField('name', 'testicek2');
        $I->click('Save');
        $I->seeInCurrentUrl('/admin/menu');
        $I->dontSee('testMenu');
        $I->see('testicek2');

        //Delete
        $I->click(Locator::lastElement('.col-md-4.card.text-dark.mb-3 a.menu-list__delete'));
        $I->dontSee('testicek2');
    }

    public function testMenuSelect(AcceptanceTester $I): void
    {
        //initial menus
        $I->amOnPage('/admin/menu/new');
        $I->fillField('name', 'ZselectMenu1');
        $I->click('Save');
        $I->amOnPage('/admin/menu/new');
        $I->fillField('name', 'ZselectMenu2');
        $I->click('Save');

        $I->seeNumberOfElements('.menu-list__selected', 1);
        $I->click('Select');
        $I->seeNumberOfElements('.menu-list__selected', 1);

        //deletion
        $I->click(Locator::firstElement('.menu-list__select'));
        $I->click(Locator::lastElement('.col-md-4.card.text-dark.mb-3 a.menu-list__delete'));
        $I->click(Locator::lastElement('.col-md-4.card.text-dark.mb-3 a.menu-list__delete'));
    }

}
