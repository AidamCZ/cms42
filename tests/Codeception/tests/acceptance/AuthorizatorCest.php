<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

class AuthorizatorCest
{

    public function testDisAllow(AcceptanceTester $I): void
    {
        $I->amOnPage('/en/admin/dashboard');
        $I->seeInCurrentUrl('sign/in');
        $I->see('You must login first to view this page.');
    }
}
