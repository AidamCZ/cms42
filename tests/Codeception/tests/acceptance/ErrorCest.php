<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

class ErrorCest
{

    public function test404(AcceptanceTester $I): void
    {
        $I->amOnPage('/neexistuje404');
        $I->seePageNotFound();
        $I->see('404');

        $I->click('Úvodní stránka');
        $I->seeCurrentUrlEquals('/');
    }
//TODO selenium
//    public function test403(AcceptanceTester $I): void
//    {
//        $I->amOnPage('/.htaccess');
//        $I->see('403');
//    }
}
