<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

class HomepageCest
{
    public function testHomepageRender(AcceptanceTester $I): void
    {
        $I->amOnPage('/');
        $I->see('Úvodní stránka');
        $I->seeInTitle('Úvodní strana');

        $I->amOnPage('en');
        $I->see('Home page');
//        $I->seeInTitle('Home Page'); //TODO multijazyčné
    }
}
