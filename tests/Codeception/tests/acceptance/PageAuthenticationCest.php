<?php declare(strict_types=1);

use Nette\Utils\Random;

class PageAuthenticationCest
{

    public function _before(AcceptanceTester $I): void
    {
        $I->loginAsRoot();
    }

    public function authenticationTest(AcceptanceTester $I): void
    {
        $url = 'test-url' . Random::generate();
        $title = 'test-title' . Random::generate();
        $I->amOnPage('/admin/pages/edit');
        $I->fillField('title', $title);
        $I->fillField('url', $url);
        $I->click('input[name="saveAndRedirect"]');
        $I->seeInCurrentUrl('/' . $url);
        $I->see($title);
        $I->click('Edit');

        $I->uncheckOption('passwordNone');
        $I->fillField('password', 'password123');
        $I->click('input[name="saveAndRedirect"]');

        $I->seeInCurrentUrl('protected-page');
        $I->fillField('password', 'badPassword');
        $I->click('Potvrdit');
        $I->see('Zadané heslo je špatně, zkuste to prosím znovu');

        $I->fillField('password', 'password123');
        $I->click('Potvrdit');
        $I->seeInCurrentUrl('/' . $url);
    }
}
