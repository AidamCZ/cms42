<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

class SignInCest
{

    public function successLoginAndLogoutTest(AcceptanceTester $I): void
    {
        $I->amOnPage('/en/sign/in');

        $I->see('Sign in');
        $I->dontSee('Log out'); //check if menu contains button

        $I->fillField('Username', 'user');
        $I->fillField('Password', 'user');
        $I->click('Log in');
//        $I->seeInCurrentUrl('/admin/dashboard');
        $I->see('Administration');
        $I->see('Logged as user');
//        $I->amOnPage('/en/sign/in');
//        $I->seeInCurrentUrl('/admin/dashboard'); //check redirect
        $I->amOnPage('/en');

        $I->dontSee('Sign in');
        $I->see('Log out'); //check if menu contains button

        $I->click('Log out');
        $I->seeInCurrentUrl('/sign/in');
    }

    public function badLoginAndPasswordTest(AcceptanceTester $I): void
    {
        $I->amOnPage('/en/sign/in');
        $I->fillField('Username', 'badLogin');
        $I->fillField('Password', 'user');
        $I->click('Log in');
        $I->seeInCurrentUrl('/en/sign/in');
        $I->see('Username is incorrect, try it once more please');
    }

    public function badPasswordTest(AcceptanceTester $I): void
    {
        $I->amOnPage('/en/sign/in');
        $I->fillField('Username', 'user');
        $I->fillField('Password', 'badPassword');
        $I->click('Log in');
        $I->seeInCurrentUrl('/en/sign/in');
        $I->see('Password is incorrect, try it once more please');
    }
}
