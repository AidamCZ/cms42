<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

class LayoutLinksCest
{

    public function testHomepageLink(AcceptanceTester $I): void
    {
        $I->amOnPage('/contact');
        $I->click('.navbar-brand');
        $I->see('Úvodní');
    }
}
