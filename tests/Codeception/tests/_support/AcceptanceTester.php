<?php declare(strict_types=1);


/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = NULL)
 * @SuppressWarnings(PHPMD)
 */
class AcceptanceTester extends \Codeception\Actor
{
    use _generated\AcceptanceTesterActions;


    public function loginAsRoot(): void
    {
        $I = $this;

        $I->wantTo('Login');
        $I->sendPOST('sign/in', [
            'username' => 'root',
            'password' => 'root',
            'remember' => '1',
            '_do' => 'signIn-signInForm-submit',
        ]);
        $I->dontSeeInCurrentUrl('sign/in');
    }

    public function logout(): void
    {
        $I = $this;

        $I->amOnPage('sign/out');
    }
}
