<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace App\Helpers;

use Nette\Utils\Html;

class CheckboxListInput extends \Czubehead\BootstrapForms\Inputs\CheckboxListInput
{

    public const RENDER_VERTICAL = 'd-block',
        RENDER_INLINE = 'd-inline-block';

    /** @var string */
    private $render;

    /** @inheritdoc */
    public function __construct($label = null, ?array $items = null, string $render = self::RENDER_VERTICAL)
    {
        parent::__construct($label, $items);
        $this->render = $render;
    }

    /** @inheritdoc This method is same as inherited, but uses \App\Helpers\CheckboxInput class */
    public function getControl()
    {
        parent::getControl();
        $fieldset = Html::el('fieldset', [
            'disabled' => $this->isControlDisabled(),
        ]);

        $baseId = $this->getHtmlId();
        $c = 0;
        foreach ($this->items as $value => $caption) {
            $wrapper = Html::el('div', ['class' => $this->render]);
            $line = CheckboxInput::makeCheckbox($this->getHtmlName(), $baseId . $c, $caption, $this->isValueSelected($value),
                $value, FALSE, $this->isValueDisabled($value));

            $wrapper->addHtml($line);
            $fieldset->addHtml($wrapper);
            $c++;
        }

        return $fieldset;
    }

}
