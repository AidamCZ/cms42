<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace App\Filters;

use DateTime;
use DateTimeImmutable;
use DateTimeInterface;
use DateTimeZone;
use Settings\SettingsFacade;

class DateTimeFormat
{

    private const OPTION_DATETIME_FORMAT = 'datetime_format',
        OPTION_TIMEZONE = 'timezone',
        DEFAULT_DATETIME_FORMAT = 'Y-m-d H:i:s';

    /** @var string */
    private $format;

    /** @var DateTimeZone|null */
    private $timezone;

    public function __construct(SettingsFacade $settingsFacade)
    {
        $this->format = $settingsFacade->getOptionValue(self::OPTION_DATETIME_FORMAT) ?: self::DEFAULT_DATETIME_FORMAT;
        $timezoneOption = $settingsFacade->getOptionValue(self::OPTION_TIMEZONE);
        $this->timezone = $timezoneOption ? new DateTimeZone($timezoneOption): null;
    }

    public function __invoke(?DateTimeInterface $dateTime = null): string
    {
        if ($dateTime === null) {
            $dateTime = new DateTimeImmutable();
        }
        if ($this->timezone !== null && ($dateTime instanceof DateTimeImmutable || $dateTime instanceof DateTime)) {
            $dateTimeNew = $dateTime->setTimezone($this->timezone);
            return $dateTimeNew->format($this->format);
        }
        return $dateTime->format($this->format);
    }

}
