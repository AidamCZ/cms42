<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace App\AuthModule\Presenters;

use App\Components\Flashes\Flashes;
use App\FrontModule\Presenters\BasePresenter;
use Nette\Security\AuthenticationException;
use Users\Components\SignInForm\ISignInFormFactory;
use Users\Components\SignInForm\SignInForm;

class SignPresenter extends BasePresenter
{

    /** @var string @persistent */
    public $backlink;

    public function actionIn(): void
    {
        if ($this->getUser()->isLoggedIn()) {
            $this->redirect(':Front:Homepage:');
        }
        $this->setTitle('sign.in.title', true);
    }

    public function actionOut(): void
    {
        $this->getUser()->logout(true); //TODO logout on same page
        $this->flashMessage($this->translator->translate('sign.out.flash'));
        $this->fullRedirect('in'); //TODO logout as handle
    }

    protected function createComponentSignIn(ISignInFormFactory $factory): SignInForm
    {
        $control = $factory->create();
        $control->onSignIn[] = function (SignInForm $control): void {
            $this->flashMessage($this->translator->translate('forms.signIn.flash.success'), Flashes::SUCCESS);
            $this->restoreRequest($this->backlink ?? ':Front:Homepage:');
            $this->redirect(':Front:Homepage:'); //to be sure :P
        };
        $control->onBadLogin[] = function (SignInForm $control, AuthenticationException $exception): void {
            $this->flashMessage($this->translator->translate($this->translator->translate($exception->getMessage())),
                Flashes::ERROR);
            $this->postGet('this');
            $this->sendPayload();
        };
        return $control;
    }

}
