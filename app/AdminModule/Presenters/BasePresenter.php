<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace App\AdminModule\Presenters;

use App\Components\Admin\Tutorial\Hint;
use App\Components\Flashes\Flashes;
use App\Components\TAdminComponents;
use Kdyby\Translation\Translator;
use Monolog\Logger;
use Nette\Security\IUserStorage;
use Nextras\Application\UI\SecuredLinksPresenterTrait;
use Nittro\Bridges\NittroUI\Presenter;
use Security\Authorizator;

abstract class BasePresenter extends Presenter
{

    use TAdminComponents;
    use SecuredLinksPresenterTrait;

    /** @var Logger @inject */
    public $monolog;

    /** @var Translator @inject */
    public $translator;

    /** @phpcsSuppress SlevomatCodingStandard.TypeHints.TypeHintDeclaration.MissingParameterTypeHint */
    public function checkRequirements($element): void
    {
        parent::checkRequirements($element);
        $user = $this->getUser();
        if (!$user->isLoggedIn()) {
            if ($user->logoutReason === IUserStorage::INACTIVITY) {
                $this->flashMessage($this->translator->translate('sign.out.inactivity'), Flashes::ERROR);
            } else {
                $this->flashMessage($this->translator->translate('sign.out.noLogin'), Flashes::ERROR);
            }
            $this->monolog->addError('HTTP 401 - Unauthorized! Not logged in');
            $this->fullRedirect(':Auth:Sign:in', ['backlink' => $this->storeRequest()]);
            $this->terminate();
        }
        if (!$user->isAllowed($this->getName() . ':' . $this->getAction(), Authorizator::READ)) {
            $this->monolog->addError('HTTP 403 - Forbidden! No Permissions');
            $this->flashMessage($this->translator->translate('sign.out.denied'), Flashes::ERROR);
            $this->fullRedirect(':Auth:Sign:in', ['backlink' => $this->storeRequest()]);
            $this->terminate();
        }
    }

    public function findLayoutTemplateFile(): ?string
    {
        if ($this->layout === false) {
            return null;
        }
        return __DIR__ . '/templates/@layout.latte';
    }

    protected function checkPermissions(string $privilege): void
    {
        if (!$this->user->isAllowed($this->getName() . ':' . $this->getAction(), $privilege)) {
            $this->flashMessage('You do not have enought permissions to do that', Flashes::ERROR);
            $this->redirect('this');
            $this->sendPayload();
            return;
        }
    }

    protected function startup(): void
    {
        parent::startup();
        if ($this->request->hasFlag(\Nette\Application\Request::RESTORED)) { // Redirect after AJAX login
            $this->disallowAjax();
            $this->redirect('this');
        }
        $this->setDefaultSnippets(['pageContent', 'title', 'tutorial']);
    }

    protected function beforeRender(): void
    {
        $this->template->host = $this->getHttpRequest()->getUrl()->getHost();
    }

    protected function setTitle(string $title): void
    {
        $this['title']->setTitle($title);
    }

    protected function addHint(string $title, string $hint): void
    {
        $this['tutorial']->addHint(new Hint($title, $hint));
    }

    /**
     * Performs full HTTP redirect without AJAX
     * @param array|mixed[] $args
     */
    protected function fullRedirect(string $destination, array $args = [], int $code = 302): void
    {
        $this->disallowAjax();
        $this->redirect($code, $destination, $args);
    }

}
