<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace App\Components\Footer;

use App\Components\BaseControl;

class Footer extends BaseControl
{

    private const TEMPLATE = __DIR__ . '/templates/Footer.latte',
        ADMIN_TEMPLATE = __DIR__ . '/templates/AdminFooter.latte';

    public function render(): void
    {
        $this->template->year = date('Y');
        $this->template->setFile($this->getChangedTemplate() ?? self::TEMPLATE);
        $this->template->render();
    }

    public function renderAdmin(): void
    {
        $this->template->year = date('Y');
        $this->template->setFile(self::ADMIN_TEMPLATE);
        $this->template->render();
    }

}
