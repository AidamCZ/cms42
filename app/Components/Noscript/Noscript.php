<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace App\Components\Noscript;

use App\Components\BaseControl;

class Noscript extends BaseControl
{

    private const TEMPLATE = __DIR__ . '/templates/Noscript.latte';

    public function render(): void
    {
        $this->template->setFile($this->getChangedTemplate() ?? self::TEMPLATE);
        $this->template->render();
    }

}
