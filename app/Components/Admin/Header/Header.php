<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace App\Components\Admin\Header;

use App\Components\Admin\QuickActions\IQuickActionsFactory;
use App\Components\Admin\QuickActions\QuickActions;
use App\Components\BaseControl;

class Header extends BaseControl
{

    private const TEMPLATE = __DIR__ . '/templates/Header.latte';

    public function render(): void
    {
        $this->template->setFile(self::TEMPLATE);
        $this->template->render();
    }

    protected function createComponentQuickActions(IQuickActionsFactory $factory): QuickActions
    {
        return $factory->create();
    }

}
