<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace App\Components\Admin\QuickActions;

use App\Components\Admin\QuickActions\Actions\ResetAction;
use App\Components\Admin\QuickActions\Actions\RestartAction;
use App\Components\Admin\QuickActions\Actions\SupportAction;
use App\Components\BaseControl;
use App\Helpers\RestartService;
use Security\Authorizator;

//TODO custom actions (plugins), Testy (Unit)
class QuickActions extends BaseControl
{

    private const TEMPLATE = __DIR__ . '/templates/QuickActions.latte';

    /** @var Action[] */
    private $actions = [];

    public function __construct(RestartService $restartService)
    {
        parent::__construct();
        //Set default actions
        $this->addAction(new RestartAction($restartService));
        $this->addAction(new ResetAction($restartService));
        $this->addAction(new SupportAction());
    }

    public function addAction(Action $action): void
    {
        $this->actions[] = $action;
    }

    public function render(): void
    {
        $this->template->actions = $this->actions;
        $this->template->setFile(self::TEMPLATE);
        $this->template->render();
    }

    /** @secured */
    public function handleClick(string $key): void
    {
        $this->checkPermissions(Authorizator::DELETE, 'Settings:Admin:Settings');
        if (array_key_exists($key, $this->actions)) {
            $action = $this->actions[$key];
            $action->process($this);
        }
        $this->redirect('this');
    }

}
