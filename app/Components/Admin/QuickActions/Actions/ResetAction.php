<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace App\Components\Admin\QuickActions\Actions;

use App\Components\Admin\QuickActions\CustomAction;
use App\Components\Admin\QuickActions\QuickActions;
use App\Components\Flashes\Flashes;
use App\Helpers\RestartService;
use Nette\Utils\Html;

class ResetAction implements CustomAction
{

    /** @var RestartService */
    private $restartService;

    public function __construct(RestartService $restartService)
    {
        $this->restartService = $restartService;
    }

    public function process(QuickActions $control): void
    {
        $this->restartService->refreshClassmap();
        $this->restartService->cleanCache();
        $this->restartService->dbSchema();
        $this->restartService->fixtures();
        $control->flashMessage('CMS was restored to factory default', Flashes::SUCCESS);
    }

    public function getTitle(): string
    {
        return 'Reset';
    }

    public function getImageSrc(): string
    {
        return 'images/reset.svg';
    }

    public function getElement(string $link, string $basePath): Html
    {
        $el = Html::el('li', ['class' => 'dropdown-item']);
        $a = Html::el('a', [
            'href' => htmlspecialchars_decode($link),
            'title' => 'Reset',
            'class' => 'd-b td-n pY-5 bgcH-grey-100 c-grey-700',
            'data-prompt' => 'Are you sure you want to reset entire system? It will delete all
            your data included articles and settings',
            'data-confirm' => 'Yes',
            'data-cancel' => 'No',
        ]);
        $img = Html::el('img', [
            'class' => 'w-2r',
            'src' => $basePath . '/images/reset.svg',
            'alt' => 'Reset',
        ]);
        $span = Html::el('span', [
            'class' => 'ml-2',
        ])->addText('Reset');
        $a->addHtml($img);
        $a->addHtml($span);
        $el->addHtml($a);
        return $el;
    }

}
