<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace App\Components\Admin\Tutorial;

use App\Components\BaseControl;

class Tutorial extends BaseControl
{

    private const TEMPLATE = __DIR__ . '/Tutorial.latte';

    /** @var Hint[] */
    private $hints = [];

    public function render(): void
    {
        $this->template->setFile(self::TEMPLATE);

        $this->template->hints = $this->hints;

        if ($this->hints) {
            $this->template->render();
        }
    }

    /** @param Hint[] $hints */
    public function setHints(array $hints): void
    {
        $this->hints = $hints;
    }

    public function addHint(Hint $hint): void
    {
        $this->hints[] = $hint;
    }

}
