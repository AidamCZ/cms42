<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace App\Components\Admin\Sidebar;

use App\Components\BaseControl;
use Navigation\Components\AdminMenu\AdminMenu;
use Navigation\Components\AdminMenu\IAdminMenuFactory;

class Sidebar extends BaseControl
{

    private const TEMPLATE = __DIR__ . '/Sidebar.latte';

    public function render(): void
    {
        $this->template->host = $this->presenter->getHttpRequest()->getUrl()->getHost();
        $this->template->setFile(self::TEMPLATE);
        $this->template->render();
    }

    protected function createComponentAdminMenu(IAdminMenuFactory $factory): AdminMenu
    {
        return $factory->create();
    }

}
