<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace App\Components\Admin\ToolBar;

use App\Components\BaseControl;

class ToolBar extends BaseControl
{

    private const TEMPLATE = __DIR__ . '/ToolBar.latte';

    public function render(): void
    {
        $colors = [
            'bg-primary',
            'bg-success',
            'bg-info',
            'bg-warning',
            'bg-danger',
        ];
        $this->template->color = $colors[array_rand($colors)];
        $this->template->setFile(self::TEMPLATE);
        $this->template->render();
    }

}
