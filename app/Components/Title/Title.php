<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace App\Components\Title;

use App\Components\BaseControl;
use Settings\SettingsFacade;

/**
 * Class TitleControl
 * @package App\Components
 */
class Title extends BaseControl
{

    private const TEMPLATE = __DIR__ . '/Title.latte';

    /** @var SettingsFacade */
    private $settingsFacade;

    /** @var string */
    private $title = '';

    public function __construct(SettingsFacade $settingsFacade)
    {
        parent::__construct();
        $this->settingsFacade = $settingsFacade;
    }

    public function render(): void
    {
        $this->template->title = $this->getTitle();
        $this->template->setFile(self::TEMPLATE);
        $this->template->render();
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * Získává separator a siteName z nastavení a přidá je k titlu
     * @return string
     */
    private function getTitle(): string
    {
        $siteName = $this->settingsFacade->getOptionValue('site_name');
        $separator = $this->settingsFacade->getOptionValue('title_separator');
        if (!$separator) {
            $separator = $this->title ? ' | ' : ''; //pokud není nastaven separátor ale je nastaven title, použiji rouru
        } else {
            $separator = sprintf(' %s ', trim($separator)); //add spaces
        }
        return sprintf('%s%s%s', $this->title, $siteName && $this->title ? $separator : '', $siteName ?: '');
    }

}
