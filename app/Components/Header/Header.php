<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace App\Components\Header;

use App\Components\BaseControl;
use Settings\SettingsFacade;

class Header extends BaseControl
{

    private const TEMPLATE = __DIR__ . '/templates/Header.latte';

    /** @var SettingsFacade */
    private $settingsFacade;

    public function __construct(SettingsFacade $settingsFacade)
    {
        parent::__construct();
        $this->settingsFacade = $settingsFacade;
    }

    public function render(): void
    {
        $this->template->siteName = $this->settingsFacade->getOptionValue('site_name');
        $this->template->setFile($this->getChangedTemplate() ?? self::TEMPLATE);
        $this->template->render();
    }

}
