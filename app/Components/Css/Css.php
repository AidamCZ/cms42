<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace App\Components\Css;

use App\Components\Asset;
use MatthiasMullie\Minify;
use WebLoader;

/**
 * Class Css
 * @package App\Components\Css
 * TODO ValueObject
 */
class Css extends Asset
{

    private const
        TEMPLATE = __DIR__ . '/templates/Css.latte',
        ADMIN_TEMPLATE = __DIR__ . '/templates/CssAdmin.latte';

    /** @var string[] */
    private $styles = [];

    /** @var string[] */
    private $vendorStyles = [];

    /** @var string[] */
    private $externalStyles = [];

    /** @var string[] */
    private $adminStyles = [];

    /** @var string[] */
    private $externalAdminStyles = [];

    /** @var string */
    private $media = 'screen';

    public function render(): void
    {
        $this->template->setFile(self::TEMPLATE);
        $this->template->render();
    }

    public function renderAdmin(): void
    {
        $this->template->setFile(self::ADMIN_TEMPLATE);
        $this->template->render();
    }

    protected function createComponentCssLoader(): WebLoader\Nette\CssLoader
    {
        $files = new WebLoader\FileCollection();
        $files->addFiles($this->styles);

        return $this->createCssLoader($files, false);
    }

    protected function createComponentCssVendorLoader(): WebLoader\Nette\CssLoader
    {
        $files = new WebLoader\FileCollection();
        $files->addRemoteFiles($this->externalStyles);
        $files->addFiles($this->vendorStyles);

        return $this->createCssLoader($files);
    }

    protected function createComponentCssAdminLoader(): WebLoader\Nette\CssLoader
    {
        $files = new WebLoader\FileCollection();
        $files->addRemoteFiles($this->externalAdminStyles);
        $files->addFiles($this->adminStyles);

        return $this->createCssLoader($files);
    }

    private function createCssLoader(WebLoader\FileCollection $files, bool $nonce = true): WebLoader\Nette\CssLoader
    {
        $compiler = WebLoader\Compiler::createCssCompiler($files, $this->wwwDir . '/temp');
        if ($this->minify) {
            $compiler->addFilter(function ($code) {
                $minifier = new Minify\CSS();
                $minifier->add($code);
                return $minifier->minify();
            });
        }
        $control = new CssLoader($compiler, $this->template->basePath . '/temp');
        $control->setMedia($this->media);
        $control->setNonce($nonce);
        return $control;
    }

    public function addStyle(string $style): void
    {
        $this->styles[] = $style;
    }

    public function addVendorStyle(string $vendorStyle): void
    {
        $this->vendorStyles[] = $vendorStyle;
    }

    public function addExternalStyle(string $externalStyle): void
    {
        $this->externalStyles[] = $externalStyle;
    }

    public function addAdminStyle(string $style): void
    {
        $this->adminStyles[] = $style;
    }

    public function addExternalAdminStyle(string $externalStyle): void
    {
        $this->externalAdminStyles[] = $externalStyle;
    }
    //TODO addStandaloneStyle + JS + async defer :)
    public function setMedia(string $media): void
    {
        $this->$media = $media;
    }

}
