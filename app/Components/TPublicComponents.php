<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace App\Components;

use Kdyby\Autowired\AutowireComponentFactories;

trait TPublicComponents
{

    use AutowireComponentFactories;

    protected function createComponentMeta(\App\Components\Meta\IMetaFactory $factory): \App\Components\Meta\Meta
    {
        return $factory->create();
    }

    protected function createComponentTitle(\App\Components\Title\ITitleFactory $factory): \App\Components\Title\Title
    {
        return $factory->create();
    }

    protected function createComponentFooter(\App\Components\Footer\IFooterFactory $factory): \App\Components\Footer\Footer
    {
        return $factory->create();
    }

    protected function createComponentNoscript(\App\Components\Noscript\INoscriptFactory $factory): \App\Components\Noscript\Noscript
    {
        return $factory->create();
    }

    protected function createComponentCss(\App\Components\Css\ICssFactory $factory): \App\Components\Css\Css
    {
        return $factory->create();
    }

    protected function createComponentJs(\App\Components\Js\IJsFactory $factory): \App\Components\Js\Js
    {
        return $factory->create();
    }

    protected function createComponentFavicon(\App\Components\Favicon\IFaviconFactory $factory): \App\Components\Favicon\Favicon
    {
        return $factory->create();
    }

    protected function createComponentHeader(\App\Components\Header\IHeaderFactory $factory): \App\Components\Header\Header
    {
        return $factory->create();
    }

    protected function createComponentMainMenu(\Navigation\Components\MainMenu\IMainMenuFactory $factory): \Navigation\Components\MainMenu\MainMenu
    {
        return $factory->create();
    }

    protected function createComponentFlashes(\App\Components\Flashes\IFlashesFactory $factory): \App\Components\Flashes\Flashes
    {
        return $factory->create();
    }

    protected function createComponentToolBar(\App\Components\Admin\ToolBar\IToolBarFactory $factory): \App\Components\Admin\ToolBar\ToolBar
    {
        return $factory->create();
    }

    protected function createComponentLocaleChanger(\Locale\Components\LocaleChanger\ILocaleChangerFactory $factory): \Locale\Components\LocaleChanger\LocaleChanger
    {
        return $factory->create();
    }

}
