<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace App\Components;

abstract class Asset extends BaseControl
{

    /** @var string */
    protected $wwwDir;

    /** @var bool */
    protected $minify = true;

    /** @var bool */
    protected $concatenate = true; //TODO

    public function __construct(string $wwwDir)
    {
        parent::__construct();
        $this->wwwDir = $wwwDir;
    }

    public function setMinify(bool $minify = true): void
    {
        $this->minify = $minify;
    }

    public function setConcatenate(bool $concatenate = true): void
    {
        $this->minify = $concatenate;
    }

}
