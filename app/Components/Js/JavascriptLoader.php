<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace App\Components\Js;

use Nette\Utils\Html;
use WebLoader\Nette\WebLoader;

class JavascriptLoader extends WebLoader
{

    /** @var bool */
    private $nonce = true;
    /** @var bool */
    private $async = false;
    /** @var bool */
    private $defer = false;

    public function setNonce(bool $nonce): JavascriptLoader
    {
        $this->nonce = $nonce;
        return $this;
    }

    public function setAsync(bool $async): JavascriptLoader
    {
        $this->async = $async;
        return $this;
    }

    public function setDefer(bool $defer): JavascriptLoader
    {
        $this->defer = $defer;
        return $this;
    }

    /**
     * Get html element including generated content
     * @param string $source
     * @return \Nette\Utils\Html
     * @phpcsSuppress SlevomatCodingStandard.TypeHints.TypeHintDeclaration
     */
    public function getElement($source): Html
    {
        //Shut up, I do not care about your opinion about this beautiful code :)
        $nonce = null;
        if ($this->nonce) {
            $csp = $this->presenter->getHttpResponse()->getHeader('Content-Security-Policy');
            if ($csp && preg_match('#\s\'nonce-([\w+/]+=*)\'#', $csp, $m)) {
                $nonce = $m[1];
            }
        }
        return
            Html::el('script', [
                'type' => 'application/javascript',
                'src' => $source,
                'nonce' => $nonce,
                'async' => $this->async,
                'defer' => $this->defer,
            ]);
    }

}
