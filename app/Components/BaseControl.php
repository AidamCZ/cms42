<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace App\Components;

use App\Components\Datagrid\Datagrid;
use App\Components\Flashes\Flashes;
use App\Extensions\IControl;
use Nette\Application\UI\Control;
use Nextras\Application\UI\SecuredLinksControlTrait;
use Nittro\Bridges\NittroUI\Presenter;

/**
 * @method onBadPermissions(BaseControl $self)
 * @method onSuccess(BaseControl $self)
 */
abstract class BaseControl extends Control implements IControl
{

    use TPublicComponents;
    use SecuredLinksControlTrait;

    /** @var callable[] */
    public $onBadPermissions = [];

    /** @var callable[] */
    public $onSuccess = [];

    /** @var string|null */
    private $changedTemplate;

    /** @return null|string Custom template */
    protected function getChangedTemplate(): ?string
    {
        return $this->changedTemplate;
    }

    protected function checkPermissions(string $privilege, ?string $resource = null): void
    {
        $presenter = $this->getPresenter();
        $resource = $resource ?? $presenter->getName() . ':' . $presenter->getAction();
        if (!$presenter->user->isAllowed($resource, $privilege)) {
            $this->onBadPermissions($this);
            $presenter->flashMessage('You do not have enought permissions to do that', Flashes::ERROR);
            $presenter->redirect('this');
            $presenter->sendPayload();
            return;
        }
    }

    protected function getDatagrid(): Datagrid
    {
        //TODO default datasource callback?
        $grid = new Datagrid();
        $grid->addCellsTemplate(__DIR__ . '/Datagrid/@bootstrap.datagrid.latte');
        return $grid;
    }

    /**
     * @param array|mixed[] $args
     * @throws \Nette\Application\AbortException
     */
    protected function postGet(string $destination, array $args = []): void
    {
        /** @var Presenter $presenter */
        $presenter = $this->presenter;
//        if ($presenter) {
        $presenter->postGet($destination, $args);
//        }
    }

    /**
     * @param string $path Path to template.latte
     * TODO addTemplate
     */
    public function changeTemplate(string $path): void
    {
        $this->changedTemplate = $path;
    }

}
