<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace App\Extensions;

use App\Components\Css\ICssFactory;
use App\Components\Css\Providers\ICssAdminProvider;
use App\Components\Css\Providers\ICssProvider;
use App\Components\Css\Providers\ICssVendorProvider;
use Nette\Utils\Validators;

class CssExtension extends AssetExtension
{

    public function beforeCompile(): void
    {
        $builder = $this->getContainerBuilder();
        $definition = $builder->getDefinitionByType(ICssFactory::class);

        if (!$this->config['minify']) {
            $definition->addSetup('setMinify', [false]);
        }
        if (!$this->config['concatenate']) {
            $definition->addSetup('setConcatenate', [false]);
        }

        /** @var ICssVendorProvider $extension */
        foreach ($this->compiler->getExtensions(ICssVendorProvider::class) as $extension) {
            $vendorStyles = $extension->getCssVendorFiles();
            foreach ($vendorStyles as $vendorStyle) {
                if (Validators::isUrl($vendorStyle)) {
                    $definition->addSetup('addExternalStyle', [$vendorStyle]);
                } else {
                    $definition->addSetup('addVendorStyle', [$vendorStyle]);
                }
            }
        }

        /** @var ICssProvider $extension */
        foreach ($this->compiler->getExtensions(ICssProvider::class) as $extension) {
            $styles = $extension->getCssFiles();
            foreach ($styles as $style) {
                $definition->addSetup('addStyle', [$style]);
            }
        }

        /** @var ICssAdminProvider $extension */
        foreach ($this->compiler->getExtensions(ICssAdminProvider::class) as $extension) {
            $adminStyles = $extension->getCssAdminFiles();
            foreach ($adminStyles as $adminStyle) {
                if (Validators::isUrl($adminStyle)) {
                    $definition->addSetup('addExternalAdminStyle', [$adminStyle]);
                } else {
                    $definition->addSetup('addAdminStyle', [$adminStyle]);
                }
            }
        }

        $definition->addSetup('addStyle', [$this->wwwDir . '/assets/front.css']);
        $definition->addSetup('addAdminStyle', [$this->wwwDir . '/assets/admin.css']);
    }

}
