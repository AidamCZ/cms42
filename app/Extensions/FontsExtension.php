<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace App\Extensions;

use App\Exceptions\DirectoryNotFound;
use App\Exceptions\FileNotFound;
use App\Extensions\Providers\IFontsProvider;
use App\Helpers\FileSystem;
use Nette\Reflection\ClassType;
use Plugins\IPlugin;

class FontsExtension extends CompilerExtension
{

    /** @var string */
    private $wwwDir;

    public function __construct(string $wwwDir)
    {
        $this->wwwDir = $wwwDir;
    }

    public function beforeCompile(): void
    {
        $extensions = $this->compiler->getExtensions(IFontsProvider::class);
        if (!$extensions) {
            return;
        }
        FileSystem::purge($this->wwwDir . '/fonts', false, true);
        /** @var IFontsProvider $extension */
        foreach ($extensions as $extension) {
            $fonts = $extension->getFonts();
            $reflection = ClassType::from(\get_class($extension));
            /** @var IPlugin $extension */
            if ($reflection->implementsInterface(IPlugin::class)) {
                $plugin = $extension->getPlugin();
                $destination = $this->wwwDir . '/plugins/' . $plugin->getIdentifier();
            } else {
                $destination = $this->wwwDir . '/images';
            }
            if (\is_array($fonts)) {
                foreach ($fonts as $path) {
                    if (!file_exists($path)) {
                        throw new FileNotFound(sprintf('File %s does not exists', $path));
                    }
                    FileSystem::copy($path, $destination);
                }
            } elseif (file_exists($fonts) && is_dir($fonts)) {
                FileSystem::copy($fonts, $destination);
            } else {
                throw new DirectoryNotFound(sprintf('Path %s does not exists or it is not a directory', $fonts));
            }
        }
    }

}
