<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace App\Extensions;

abstract class AssetExtension extends CompilerExtension
{

    /** @var string */
    protected $wwwDir;

    /** @var array|mixed[] */
    private $defaults = [
        'minify' => true,
        'concatenate' => true, //TODO
    ];

    public function __construct(string $wwwDir)
    {
        $this->wwwDir = $wwwDir;
    }

    public function loadConfiguration(): void
    {
        $this->validateConfig($this->defaults);
    }

}
