<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace App\Extensions;

use App\Exceptions\DirectoryNotFound;
use App\Exceptions\FileNotFound;
use App\Extensions\Providers\IImagesProvider;
use App\Helpers\FileSystem;
use Nette\Reflection\ClassType;
use Plugins\IPlugin;

class ImageExtension extends CompilerExtension
{

    /** @var string */
    private $wwwDir;

    public function __construct(string $wwwDir)
    {
        $this->wwwDir = $wwwDir;
    }

    public function beforeCompile(): void
    {
        $extensions = $this->compiler->getExtensions(IImagesProvider::class);
        if (!$extensions) {
            return;
        }
        FileSystem::purge($this->wwwDir . '/images', false, true);
        /** @var IImagesProvider $extension */
        foreach ($extensions as $extension) {
            $images = $extension->getImages();
            $reflection = ClassType::from(\get_class($extension));
            /** @var IPlugin $extension */
            $isPlugin = $reflection->implementsInterface(IPlugin::class);
            if ($isPlugin) {
                $plugin = $extension->getPlugin();
                $destination = $this->wwwDir . '/plugins/' . $plugin->getIdentifier();
            } else {
                $destination = $this->wwwDir . '/images';
            }
            if (\is_array($images)) {
                foreach ($images as $path) {
                    if (!file_exists($path)) {
                        throw new FileNotFound(sprintf('File %s does not exists', $path));
                    }
                    FileSystem::copy($path, $destination);
                }
            } elseif (file_exists($images) && is_dir($images)) {
                if ($isPlugin) {
                    $destination .= '/' . basename($images); //add folder name if it is plugin
                }
                FileSystem::copy($images, $destination);
            } else {
                throw new DirectoryNotFound(sprintf('Path %s does not exists or it is not a directory', $images));
            }
        }
    }

}
