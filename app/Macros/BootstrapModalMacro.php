<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace App\Macros;

use Latte\CompileException;
use Latte\Compiler;
use Latte\MacroNode;
use Latte\Macros\MacroSet;
use Latte\PhpWriter;
use function array_shift;

class BootstrapModalMacro extends MacroSet
{

    public static function install(Compiler $compiler): BootstrapModalMacro
    {
        $macroset = new static($compiler);
        $macroset->addMacro('modal', [$macroset, 'modalBegin'], [$macroset, 'modalEnd']);
        return $macroset;
    }

    public function modalBegin(MacroNode $node, PhpWriter $writer): string
    {
        if (!$node->args) {
            throw new CompileException('Modal must get parameter ID. None given');
        }
        $args = $node->args;
        $args = explode(',', $args);
        $id = trim((string)array_shift($args), " \t\n\r\0\x0B\'\"");
        $header = $args ? trim((string)array_shift($args), " \t\n\r\0\x0B\'\"") : $id;
        return $writer->write("echo '
<div class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" id=\"$id-modal\" aria-labelledby=\"$id\" aria-hidden=\"true\">
    <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <h5 class=\"modal-title\" id=\"$id\">$header</h5>
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                    <span aria-hidden=\"true\">&times;</span>
                </button>
            </div>
            <div class=\"modal-body\">'");
    }

    public function modalEnd(MacroNode $node, PhpWriter $writer): string
    {
        return $writer->write("echo '
            </div>
            <div class=\"modal-footer\">
                <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>
            </div>
        </div>
    </div>
</div>'");
    }

}
