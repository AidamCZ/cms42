<?php declare(strict_types = 1);

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180223215020 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX UNIQ_D035FA873CEE7BEE ON options');
        $this->addSql('ALTER TABLE options ADD plugin_id INT DEFAULT NULL, DROP plugin');
        $this->addSql('ALTER TABLE options ADD CONSTRAINT FK_D035FA87EC942BCF FOREIGN KEY (plugin_id) REFERENCES plugin (id)');
        $this->addSql('CREATE INDEX IDX_D035FA87EC942BCF ON options (plugin_id)');
        $this->addSql('CREATE UNIQUE INDEX key_plugin ON options (option_key, plugin_id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE options DROP FOREIGN KEY FK_D035FA87EC942BCF');
        $this->addSql('DROP INDEX IDX_D035FA87EC942BCF ON options');
        $this->addSql('DROP INDEX key_plugin ON options');
        $this->addSql('ALTER TABLE options ADD plugin VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci COMMENT \'Identifier of plugin or null if basic app option\', DROP plugin_id');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D035FA873CEE7BEE ON options (option_key)');
    }
}
