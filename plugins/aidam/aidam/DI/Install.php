<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Aidam\Aidam\DI;

use Plugins\Install\AbstractInstall;

class Install extends AbstractInstall
{

//    /** @var string */
//    private $wwwDir;

//    public function __construct(string $wwwDir)
//    {
//        $this->wwwDir = $wwwDir ?? '';
//    }

    public function install(): void
    {
//        $dir = $this->wwwDir . '/plugins/' . AidamExtension::PLUGIN_NAME;
//
//        FileSystem::copy(__DIR__ . '/../assets', $dir);
    }

    public function uninstall(): void
    {
//        FileSystem::delete($this->wwwDir . '/plugins/' . AidamExtension::PLUGIN_NAME);
    }

}
