<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Aidam\Aidam\DI;

use App\Components\Css\Providers\ICssProvider;
use App\Components\Css\Providers\ICssVendorProvider;
use App\Components\Footer\Providers\IFooterTemplateProvider;
use App\Components\Header\Providers\IHeaderTemplateProvider;
use App\Components\Js\Providers\IJsProvider;
use App\Components\Js\Providers\IJsVendorProvider;
use App\Components\Noscript\Providers\INoscriptTemplateProvider;
use Locale\Components\LocaleChanger\Providers\ILocaleChangerTemplateProvider;
use Navigation\Components\MainMenu\Providers\IMainMenuTemplateProvider;
use Plugins\Plugin;
use Plugins\PluginExtension;

//TODO depends on Assets: jQuery, Bootstrwap, Popper, FontAwesome, Nittro
class AidamExtension extends PluginExtension implements ICssProvider, ICssVendorProvider, IJsProvider,
    IJsVendorProvider, IFooterTemplateProvider, IHeaderTemplateProvider, INoscriptTemplateProvider,
    IMainMenuTemplateProvider, ILocaleChangerTemplateProvider
{

    public function loadConfiguration(): void
    {
        $this->parseConfig($this->getContainerBuilder(), __DIR__ . '/services.neon');
    }

    public function getPlugin(): Plugin
    {
        $plugin = new Plugin();
        $plugin->setName('Aidam Template');
        $plugin->setIdentifier('aidam', 'aidam');
        $plugin->setDescription('Nejlepší template široko daleko');
        $plugin->setAuthor('Adam Zelycz');
        $plugin->setVersion('1.0');
        return $plugin;
    }

    public function getCssFiles(): array
    {
        $dir = \dirname(__DIR__) . '/assets/css';
        return [
            $dir . '/style.css',
        ];
    }

    public function getCssVendorFiles(): array
    {
        $dir = \dirname(__DIR__) . '/assets/css';
        return [
            $dir . '/bootstrap.css',
            $dir . '/font-awesome.css',
        ];
    }

    public function getJsVendorFiles(): array
    {
        $dir = \dirname(__DIR__) . '/assets/js';
        return [
            'https://code.jquery.com/jquery-3.3.1.min.js',
            $dir . '/popper.min.js',
            $dir . '/bootstrap.min.js',
        ];
    }

    public function getHeaderTemplateProvider(): string
    {
        return \dirname(__DIR__) . '/templates/Header.latte';
    }

    public function getNoscriptTemplate(): string
    {
        return \dirname(__DIR__) . '/templates/Noscript.latte';
    }

    public function getFooterTemplate(): string
    {
        return \dirname(__DIR__) . '/templates/Footer.latte';
    }

    public function getMainMenuTemplate(): string
    {
        return \dirname(__DIR__) . '/templates/MainMenu.latte';
    }

    public function getJsFiles(): array
    {
        $dir = \dirname(__DIR__) . '/assets/js';
        return [
            $dir . '/main.js',
        ];
    }

    public function getLocaleChangerTemplate(): string
    {
        return \dirname(__DIR__) . '/templates/LocaleChanger.latte';
    }

}
