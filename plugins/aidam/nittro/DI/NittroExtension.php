<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Aidam\Nittro\DI;

use App\Components\Css\Providers\ICssAdminProvider;
use App\Components\Css\Providers\ICssVendorProvider;
use App\Components\Js\Providers\IJsAdminProvider;
use App\Components\Js\Providers\IJsVendorProvider;
use Plugins\Plugin;
use Plugins\PluginExtension;

class NittroExtension extends PluginExtension implements ICssVendorProvider, IJsVendorProvider,
    ICssAdminProvider, IJsAdminProvider
{

    public function getPlugin(): Plugin
    {
        $plugin = new Plugin();
        $plugin->setName('Nittro Framework');
        $plugin->setIdentifier('aidam', 'nittro');
        $plugin->setDescription('Nittro JS Framework integration for CMS42');
        $plugin->setAuthor('Adam Zelycz');
        $plugin->setVersion('1.0');
        return $plugin;
    }

    /** @return string[] */
    public function getCssVendorFiles(): array
    {
        return [__DIR__ . '/../assets/nittro.min.css'];
    }

    /** @return string[] */
    public function getJsVendorFiles(): array
    {
        return [__DIR__ . '/../assets/nittro.min.js'];
    }

    /** @return string[] */
    public function getCssAdminFiles(): array
    {
        return [__DIR__ . '/../assets/nittro.min.css'];
    }

    /** @return string[] */
    public function getJsAdminFiles(): array
    {
        return [
            __DIR__ . '/../assets/nittro.min.js',
            __DIR__ . '/../assets/nittro.nextras.datagrid.js',
        ];
    }

}
