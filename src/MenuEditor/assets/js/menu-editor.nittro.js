function menuEditorInit() {
    let editors = $('.jq-menu-edit-div');
    editors.each(function (index, e) {
        //CONFIG
        const config = JSON.parse(e.getAttribute('data-config'));

        //GENERATING ID
        let list = $(this).find('.jq-menu-edit-ul')[0];
        if (!list) {
            console.log('No jq-menu-edit-ul class found');
            return;
        }
        const id = 'jq-menu-edit-ul-' + index;
        list.setAttribute('id', id);

        //EDITOR CONSTRUCT
        let editor = new MenuEditor(id, config);

        //DEFAULT DATA
        let hiddenField = $(this).find('input[type="hidden"]')[0];
        if (!hiddenField) {
            console.log('No hidden field found');
            return;
        }
        let defaultData = JSON.parse(hiddenField.getAttribute('value'));
        editor.setData(defaultData);

        //EDIT FORM
        $(this).append('' +
            '<div class="col-md-6">' +
            '<div class="form-horizontal jq-menu-edit-form" id="jq-menu-edit-form-' + index + '">' +
            '<div class="form-group">' +
            '<label for="jq-menu-edit-form-text-' + index + '" class="col-sm-2 control-label">Text</label>' +
            '<div class="col-sm-10">' +
            '<div class="input-group"> ' +
            '<input type="text" class="form-control item-menu" name="text" id="jq-menu-edit-form-text-' + index + '" placeholder="Text"> ' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label for="jq-menu-edit-form-href-' + index + '" class="col-sm-2 control-label">URL</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control item-menu" id="jq-menu-edit-form-href-' + index + '" name="href" placeholder="URL">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label for="jq-menu-edit-form-target-' + index + '" class="col-sm-2 control-label">Target</label>' +
            '<div class="col-sm-10">' +
            '<select name="target" id="jq-menu-edit-form-target-' + index + '" class="form-control item-menu">' +
            '<option value="_self">Self</option>' +
            '<option value="_blank">Blank</option>' +
            '<option value="_top">Top</option>' +
            '</select>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="panel-footer"> ' +
            '<button type="button" id="jq-menu-edit-form-button-update-' + index + '" class="btn btn-primary" disabled="disabled"><i class="fa fa-refresh"></i> Update</button> ' +
            '<button type="button" id="jq-menu-edit-form-button-add-' + index + '" class="btn btn-success"><i class="fa fa-plus"></i> Add</button> ' +
            '</div></div>');
        editor.setForm($('#jq-menu-edit-form-' + index));

        //BUTTONS
        editor.setUpdateButton($('#jq-menu-edit-form-button-update-' + index));

        let form = $(this).parent().closest('form')[0];
        form.onsubmit = function () {
            hiddenField.value = editor.getString();
        };

        $('#jq-menu-edit-form-button-update-' + index).click(function () {
            editor.update();
        });

        $('#jq-menu-edit-form-button-add-' + index).click(function () {
            editor.add();
        });
    });
}

window._stack.push([function (di, DOM) {
    let setup = false;
    let router = di.getService('router');
    let page = di.getService('page');

    router.getDOMRoute('.jq-menu-edit-div')
        .on('match', function (evt) {
            if (setup) return;
            setup = true;

            page.getSnippet('snippet--pageContent')
                .setup(function () {
                    menuEditorInit();
                })
                .teardown(function () {
                    setup = false;
                });
        });
}, {
    DOM: 'Utils.DOM'
}]);
