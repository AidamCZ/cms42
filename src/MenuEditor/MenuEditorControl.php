<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace MenuEditor;

use Nette\Forms\Container;
use Nette\Forms\Controls\HiddenField;
use Nette\Utils\Html;
use Nette\Utils\Json;

class MenuEditorControl extends HiddenField
{

    /** @var array|mixed[] */
    private $config;

    /** @param array|mixed[] $config */
    public function __construct(array $config)
    {
        parent::__construct();
        $this->config = $config;
        $this->setDefaultValue(Json::encode([]));
    }

    public function getControl(): Html
    {
        /** @var \Nette\Forms\Controls\HiddenField $control */
        $hidden = parent::getControl();
        $editor = Html::el('ul', [
            'class' => 'sortableLists list-group jq-menu-edit-ul col-md-6',
        ]);
        $control = Html::el('div', [
            'data-config' => $this->config,
            'class' => 'jq-menu-edit-div row',
        ]);
//        $form = new Form();
//        $form->addText('text', 'Text');
//        $form->addText('href', 'URL');
//        $form->addSelect('target', 'Target', [
//            '_blank' => 'Blank',
//            '_self' => 'Self',
//            '_parent' => 'Parent',
//            '_top' => 'Top'
//        ])->setDefaultValue('_self');
//        if (array_key_exists('formRenderer', $this->config)) {
//            $renderer = new $this->config['formRenderer'];
//            if ($renderer instanceof IFormRenderer) {
//                $form->setRenderer($renderer);
//            }
//        }

//        ob_start();
//        $form->render();
//        $formHtml = ob_get_clean();
        $control->addHtml($hidden);
        $control->addHtml($editor);
        return $control;
    }

    /**
     * @param array|mixed[] $config
     * @param string $method
     */
    public static function register(array $config, string $method = 'addMenuEditor'): void
    {
        Container::extensionMethod($method, function (Container $container, string $name, ?string $value = null) use ($config) {
            $control = new MenuEditorControl($config);
            if ($value !== null) {
                $control->setDefaultValue($value);
            }
            $container[$name] = $control;
            return $container[$name];
        });
    }

//    /**
//     * @param MenuItem[] $value
//     * @inheritdoc
//     */
//    public function setDefaultValue($value)
//    {
//        Validators::everyIs($value, MenuItem::class);
//        parent::setDefaultValue(MenuEditorParser::encode($value));
//    }

}
