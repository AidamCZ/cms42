<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Stats\DI;

use App\Extensions\CompilerExtension;
use Kdyby;
use Kdyby\Doctrine\DI\IEntityProvider;
use Stats\Command\LoadCommand;

class StatsExtension extends CompilerExtension implements IEntityProvider
{

    /** @var array|string[] */
    private $commands = [
        LoadCommand::class,
    ];

    public function loadConfiguration(): void
    {
        $this->parseConfig($this->getContainerBuilder(), __DIR__ . '/services.neon');

        $containerBuilder = $this->getContainerBuilder();

        foreach ($this->commands as $i => $command) {
            $containerBuilder->addDefinition($this->prefix('cli.' . $i))
                ->addTag(Kdyby\Console\DI\ConsoleExtension::TAG_COMMAND)
                ->setInject(false)// lazy injects
                ->setClass($command);
        }
    }

    public function beforeCompile(): void
    {
        $this->setPresenterMapping(
            $this->getContainerBuilder(), ['Stats' => 'Stats\\*Module\\Presenters\\*Presenter']
        );
    }

    public function getEntityMappings(): array
    {
        return [
            'Stats' => __DIR__ . '/../',
            'Stats\Loader' => __DIR__ . '/../Loader',
        ];
    }

}
