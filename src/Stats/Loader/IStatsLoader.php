<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Stats\Loader;

use Stats\Stat;

interface IStatsLoader
{

    /** @return Stat[] */
    public function load(): array;

    public function clean(): void;

}
