<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Stats\Components\TotalStats;

use App\Components\BaseControl;
use App\Components\TFormFactory;
use App\Helpers\CheckboxListInput;
use Czubehead\BootstrapForms\Enums\RenderMode;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Form;
use Stats\Stat;

class TotalStats extends BaseControl
{

    use TFormFactory;

    private const TEMPLATE = __DIR__ . '/TotalStats.latte';

    /** @var string @persistent */
    public $columns;

    /** @var EntityManager */
    private $em;

    /** @var string[] */
    private $groupable;

    /** @param string[] $groupableColumns */
    public function __construct(array $groupableColumns, EntityManager $em)
    {
        parent::__construct();
        $this->em = $em;
        $this->groupable = $groupableColumns;
    }

    public function render(): void
    {
        $columns = $this->getColumns();
        $results = [];
        foreach ($columns as $column) {
            if (!\array_key_exists($column, $this->groupable)) {
                continue;
            }
            $results[$column] = [
                'title' => $this->groupable[$column],
                'result' => $this->em->getRepository(Stat::class)->createQueryBuilder('s', 's.id')
                    ->select('s.' . $column . ', COUNT(s) as count')->orderBy('count', 'DESC')->groupBy('s.' . $column)
                    ->getQuery()->getResult(),
            ];
        }
        uasort($results, function (array $a, array $b): int {
            $a = \count($a['result']);
            $b = \count($b['result']);
            if ($a === $b) {
                return 0;
            }
            return ($a < $b) ? -1 : 1;
        });
        $this->template->results = $results;
        $this->template->setFile(self::TEMPLATE);
        $this->template->render();
    }

    protected function createComponentSelectForm(): Form
    {
        $form = $this->getForm(RenderMode::Inline);

        $columns = $this->getColumns();
        $form->addCheckboxList('columns', null, $this->groupable, CheckboxListInput::RENDER_INLINE)
            ->setDefaultValue($columns);
        $form->addSubmit('submit', 'Submit');
        $form->onSuccess[] = [$this, 'selectFormSucceeded'];

        return $form;
    }

    /** @param array|mixed[] $values*/
    public function selectFormSucceeded(Form $form, array $values): void
    {
        $this->setColumns($values['columns']);
        $this->redrawControl('totalStats');
        $this->postGet('this');
    }

    /** @return string[] */
    private function getColumns(): array
    {
        return $this->columns === null ? [] : \explode('&', $this->columns);
    }

    /** @param mixed[] $columns */
    private function setColumns(array $columns): void
    {
        $this->columns = \implode('&', $columns);
    }

}
