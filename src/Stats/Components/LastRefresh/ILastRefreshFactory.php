<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Stats\Components\LastRefresh;

interface ILastRefreshFactory
{

    public function create(): LastRefresh;

}
