<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Stats\Components\LastRefresh;

use App\Components\BaseControl;
use Kdyby\Doctrine\EntityManager;
use Stats\Loader\LoadStamp;

class LastRefresh extends BaseControl
{

    private const TEMPLATE = __DIR__ . '/LastRefresh.latte';

    /** @var EntityManager */
    private $em;

    public function __construct(EntityManager $em)
    {
        parent::__construct();
        $this->em = $em;
    }

    public function render(): void
    {
        $this->template->lastRefresh = $this->em->getRepository(LoadStamp::class)->findOneBy([], ['dateTime' => 'DESC']);
        $this->template->setFile(self::TEMPLATE);
        $this->template->render();
    }

}
