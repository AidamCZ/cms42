<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace ReCaptcha\DI;

use App\Components\Js\Providers\IJsVendorProvider;
use App\Extensions\CompilerExtension;

class ReCaptchaExtension extends CompilerExtension implements IJsVendorProvider
{

    /** @return string[] */
    public function getJsVendorFiles(): array
    {
        return [
            array_key_exists('aidam/nittro', $this->compiler->getExtensions())
                ? __DIR__ . '/../assets/recaptcha.nittro.js' : 'https://www.google.com/recaptcha/api.js',
        ];
//        return [];
    }

}
