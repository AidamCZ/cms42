/**
 * This file is part of the ReCaptchaControl package
 *
 * @license  MIT
 * @author   Petr Kessler (https://kesspess.cz)
 * @link     https://github.com/uestla/ReCaptchaControl
 */

window._stack.push([function (di, DOM) {
    let setup = false;
    let router = di.getService('router');
    let page = di.getService('page');

    router.getDOMRoute('.g-recaptcha')
        .on('match', function (evt) {
            if (setup) return;
            setup = true;
            page.getSnippet('snippet--pageContent')
                .setup(function () {
                    const renderAll = function () {
                        const captchas = document.querySelectorAll('.g-recaptcha');
                        Array.prototype.forEach.call(captchas, function (el, i) {
                            if (el.children.length) {
                                return;
                            }

                            grecaptcha.render(el, {}, true);
                        });
                    };

                    const callbackName = 'g_onRecaptchaLoad';
                    window[callbackName] = function () {
                        renderAll();
                    };

                    const lang = document.documentElement.lang || 'en';

                    let script = document.createElement('script');
                    script.src = 'https://www.google.com/recaptcha/api.js'
                        + '?onload=' + callbackName
                        + '&render=explicit'
                        + '&hl=' + lang;
                    document.getElementsByTagName('body')[0].appendChild(script);
                })
                .teardown(function () {
                    setup = false;
                });
        });
}, {
    DOM: 'Utils.DOM'
}]);
