<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Contact\Components\ContactForm;

use App\Components\BaseControl;
use App\Components\Flashes\Flashes;
use App\Components\TFormFactory;
use Kdyby\Translation\Translator;
use Monolog\Logger;
use Nette\Application\UI\Form;
use Nette\Mail\IMailer;
use Nette\Mail\Message;
use Nette\Mail\SendException;
use Nette\Utils\ArrayHash;
use Nette\Utils\Validators;
use Settings\SettingsFacade;

/**
 * @method onSuccess(ContactForm $self)
 */
class ContactForm extends BaseControl
{

    use TFormFactory;

    private const TEMPLATE = __DIR__ . '/ContactForm.latte';

    /** @var Translator */
    private $translator;

    /** @var SettingsFacade */
    private $settingsFacade;

    /** @var Logger */
    private $monolog;

    /** @var IMailer */
    private $mailer;

    public function __construct(
        Translator $translator,
        SettingsFacade $settingsFacade,
        IMailer $mailer,
        Logger $monolog)
    {
        parent::__construct();
        $this->translator = $translator;
        $this->settingsFacade = $settingsFacade;
        $this->monolog = $monolog;
        $this->mailer = $mailer;
    }

    public function render(): void
    {
        $this->template->setFile(self::TEMPLATE);
        $this->template->render();
    }

    protected function createComponentContactForm(): Form
    {
        $form = new Form();
        $form->setTranslator($this->translator->domain('forms.contact'));

        $form->addText('name', 'name.label')
            ->setHtmlAttribute('autofocus')
            ->setRequired('name.required');

        $form->addText('email', 'email.label')
            ->setRequired('email.required')
            ->addRule(Form::EMAIL, 'email.invalid')
            ->setHtmlType('email');

        $form->addTextArea('message', 'message.label')
            ->setRequired('message.required');

        $form->addReCaptcha('recaptcha', 'recaptcha.caption');

        $form->onSuccess[] = [$this, 'contactFormSucceeded'];
        $form->addSubmit('send', 'submit.caption');

        return $form;
    }

    public function contactFormSucceeded(Form $form, ArrayHash $values): void
    {
        $to = $this->settingsFacade->getOptionValue('email');
        if (!$to || !Validators::isEmail($to)) {
            $this->monolog->addCritical('Cannot send mail because Email address is not set or it is invalid',
                (array)$form->getValues(true));
            $this->presenter->flashMessage($this->translator->translate('contact.error'), Flashes::ERROR);
        } else {
            $mail = new Message();
            $mail->setFrom($values->email)
                ->addTo($to)
                ->setSubject('Message from contact Form')
                ->setBody($values->message);

            try {
                $this->mailer->send($mail);
                $this->onSuccess($this);
                $this->presenter->flashMessage($this->translator->translate('contact.sent'), Flashes::SUCCESS);
                $this->presenter->redirect('this');
            } catch (SendException $exception) {
                $this->monolog->addCritical(sprintf('Cannot send Email. Error: %s', $exception->getMessage()),
                    (array)$form->getValues(true)); //TODO I should not log user data
                $this->presenter->flashMessage($this->translator->translate('contact.error'), Flashes::ERROR);
            }
        }
        $this->postGet('this');
    }

}
