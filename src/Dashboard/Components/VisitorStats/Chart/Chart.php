<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Dashboard\Components\VisitorStats\Chart;

use App\Components\BaseControl;
use App\Components\TFormFactory;
use Czubehead\BootstrapForms\Enums\RenderMode;
use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;
use Nette\Utils\Json;
use Nette\Utils\Validators;
use Stats\StatsFacade;
use function array_shift;

class Chart extends BaseControl
{

    use TFormFactory;

    private const TEMPLATE = __DIR__ . '/Chart.latte';

    /** @var string @persistent */
    public $by;

    /** @var string|int|null @persistent */
    public $limit;

    /** @var bool|string|null @persistent */
    public $unique;

    /** @var StatsFacade */
    private $statsFacade;

    /** @var string[] */
    private $allowedValues;

    /** @var string[] */
    private $formats;

    public function __construct(StatsFacade $statsFacade)
    {
        parent::__construct();

        $this->statsFacade = $statsFacade;

        if ($this->by === null || !array_key_exists($this->by, $this->allowedValues)) {
            $this->by = StatsFacade::COUNT_BY_DAY;
        }

        $this->limit = $this->limit !== null && Validators::isNumericInt($this->limit) ? (int)$this->limit : 12;

        $this->allowedValues = [
            StatsFacade::COUNT_BY_DAY => 'Day',
            StatsFacade::COUNT_BY_WEEK => 'Week',
            StatsFacade::COUNT_BY_MONTH => 'Month',
            StatsFacade::COUNT_BY_YEAR => 'Year',
        ];
        $this->formats = [
            StatsFacade::COUNT_BY_DAY => 'D',
            StatsFacade::COUNT_BY_WEEK => 'W',
            StatsFacade::COUNT_BY_MONTH => 'M',
            StatsFacade::COUNT_BY_YEAR => 'Y',
        ];
    }

    public function render(): void
    {
        $this->template->setFile(self::TEMPLATE);
        $values = $this->statsFacade->countBy($this->by, (int)$this->limit, (bool)$this->unique);
        $values = array_reverse($values); // switch values from end to start (chart rendering)
        $labels = [];
        $data = [];
        foreach ($values as $value) {
            $dateTime = new \DateTimeImmutable(array_shift($value));
            $labels[] = $dateTime->format($this->formats[$this->by]);
            $data[] = array_shift($value);
        }
        $chart = [
            'type' => 'line',
            'data' => [
                'labels' => $labels,
                'datasets' => [
                    [
                        'label' => 'Series A',
                        'backgroundColor' => 'rgba(237, 231, 246, 0.5)',
                        'borderColor' => 'rgba(255, 99, 132, 0.2)',
                        'pointBackgroundColor' => 'rgba(54, 162, 235, 0.2)',
                        'borderWidth' => 2,
                        'data' => $data,
                    ],
                ],
            ],
            'options' => [
                'legend' => [
                    'display' => false,
                ],
            ],
        ];
        $this->template->data = Json::encode($chart);
        $this->template->render();
    }

//    public function render(): void
//    {
//        $this->template->setFile(self::TEMPLATE);
//        $values1 = $this->statsFacade->countBy($this->by, (int)$this->limit, false);
//        $values2 = $this->statsFacade->countBy($this->by, (int)$this->limit, true);
//        $labels = [];
//        $data = [];
//        $data2 = [];
//        foreach ($values1 as $value) {
//            $dateTime = new \DateTimeImmutable(array_shift($value));
//            $labels[] = $dateTime->format($this->formats[$this->by]);
//            $data[] = array_shift($value);
//        }
//        foreach ($values2 as $value) {
//            $data2[] = array_values($value)[1];
//        }
//        $chart = [
//            'type' => 'line',
//            'data' => [
//                'labels' => $labels,
//                'datasets' => [
//                    [
//                        'label' => 'Series A',
//                        'backgroundColor' => 'rgba(237, 231, 246, 0.5)',
//                        'borderColor' => 'rgba(255, 99, 132, 0.2)',
//                        'pointBackgroundColor' => 'rgba(54, 162, 235, 0.2)',
//                        'borderWidth' => 2,
//                        'data' => $data,
//                    ], [
//                        'label' => 'Series B',
//                        'backgroundColor' => 'rgba(137, 131, 146, 0.5)',
//                        'borderColor' => 'rgba(155, 199, 232, 0.2)',
//                        'pointBackgroundColor' => 'rgba(154, 262, 135, 0.2)',
//                        'borderWidth' => 2,
//                        'data' => $data2,
//                    ],
//                ],
//            ],
//            'options' => [
//                'legend' => [
//                    'display' => false,
//                ],
//            ],
//        ];
//        $this->template->data = Json::encode($chart);
//        $this->template->render();
//    }

    protected function createComponentCountByForm(): Form
    {
        $form = $this->getAdminForm(RenderMode::Inline);

        $form->addSelect('countBy', 'By', $this->allowedValues)
            ->setDefaultValue($this->by);

        $form->addInteger('limit', 'Limit')
            ->setDefaultValue($this->limit);

        $form->addCheckbox('unique', 'Unique')
            ->setDefaultValue((bool)$this->unique);

        $form->addSubmit('select', 'Select');
        $form->onSuccess[] = [$this, 'countByFormSucceeded'];

        return $form;
    }

    public function countByFormSucceeded(Form $form, ArrayHash $values): void
    {
        $this->by = $values->countBy;
        $this->limit = $values->limit;
        $this->unique = $values->unique;
        $this->redirect('this');
    }

}
