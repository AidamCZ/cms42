<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Dashboard\Components\VisitorStats\Panels;

/** Value object */
class Panel
{

    /** @var string */
    private $title;

    /** @var PanelValue[] */
    private $values = [];

    public function __construct(string $title)
    {
        $this->title = $title;
    }

    /** @return PanelValue[] */
    public function getValues(): array
    {
        return $this->values;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): Panel
    {
        $this->title = $title;
        return $this;
    }

    public function addValue(PanelValue $value): Panel
    {
        $this->values[] = $value;
        return $this;
    }

    public function cleanValues(): Panel
    {
        $this->values = [];
        return $this;
    }

}
