<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Security\Components\Roles;

use App\Components\BaseControl;
use App\Components\TFormFactory;
use Czubehead\BootstrapForms\Enums\RenderMode;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Form;
use Nette\Application\UI\Multiplier;
use Nette\Caching\Cache;
use Nette\Caching\IStorage;
use Security\Authorizator;
use Security\Role;

/**
 * Class Roles
 * @package Security\Components\Roles
 * @method onEdit(string $name)
 * @method onExistingName(string $name)
 * @method onCreate(string $name)
 * @method onDelete(string $name)
 */
class Roles extends BaseControl
{

    use TFormFactory;

    private const TEMPLATE = __DIR__ . '/Roles.latte';

    /** @var callable[] */
    public $onEdit;

    /** @var callable[] */
    public $onExistingName;

    /** @var callable[] */
    public $onCreate;

    /** @var callable[] */
    public $onDelete;

    /** @var EntityManager */
    private $em;

    /** @var Role[] */
    private $roles;

    /** @var string[] */
    private $rolePairs;

    /** @var Cache */
    private $cache;

    public function __construct(EntityManager $em, IStorage $storage)
    {
        parent::__construct();
        $this->cache = new Cache($storage, Authorizator::CACHE_NAMESPACE);
        $this->em = $em;
        $this->roles = $em->getRepository(Role::class)->findAll();
        $this->rolePairs[] = '';
        /** @var Role $role */
        foreach ($this->roles as $role) {
            $this->rolePairs[$role->getId()] = $role->getName();
        }
    }

    public function render(): void
    {
        $this->template->roles = $this->roles;
        $this->template->setFile(self::TEMPLATE);
        $this->template->render();
    }

    /** @secured */
    public function handleDelete(int $id): void
    {
        $this->checkPermissions(Authorizator::DELETE);
        /** @var Role $role */
        $role = $this->em->getRepository(Role::class)->find($id);
        if ($role !== null) {
            $this->em->remove($role);
            $this->em->flush();
            $this->cleanCache();
            $this->onDelete($role->getName());
        }
        $this->redirect('this');
    }

    protected function createComponentEditForm(): Multiplier
    {
        return new Multiplier(function ($index) {
            /** @var Role $role */
            $role = $this->roles[$index];
            $parentRole = $role->getParent();
            $form = $this->getForm(RenderMode::Inline);
            $form->addProtection();
            $form->addHidden('role_id')
                ->setDefaultValue($role->getId());
            $form->addText('name')
                ->setRequired()
                ->setDefaultValue($role->getName());
            $form->addSelect('parent', null, $this->rolePairs)
                ->setDefaultValue($parentRole ? $parentRole->getId() : '0');
            $form->addSubmit('edit', 'Edit');
            $form->onSuccess[] = [$this, 'editFormSucceeded'];
            return $form;
        });
    }

    /** @param array|mixed[] $values */
    public function editFormSucceeded(Form $form, array $values): void
    {
        $this->checkPermissions(Authorizator::UPDATE);
        /** @var Role $role */
        $role = $this->em->getRepository(Role::class)->find($values['role_id']);
        $role->setName($values['name']);
        if ($values['parent'] === 0) {
            $role->setParent(null);
        } else {
            $role->setParent($values['parent']);
        }
        try {
            $this->em->persist($role);
            $this->em->flush($role);
            $this->cleanCache();
            $this->onEdit($role->getName());
        } catch (UniqueConstraintViolationException $exception) {
            $this->onExistingName($role->getName());
        }
        $this->redirect('this');
    }

    protected function createComponentNewForm(): Form
    {
        $form = $this->getForm(RenderMode::Inline);
        $form->addProtection();
        $form->addText('name')
            ->setRequired();
        $form->addSelect('parent', null, $this->rolePairs);
        $form->addSubmit('new', 'New');
        $form->onSuccess[] = [$this, 'newFormSucceeded'];
        return $form;
    }

    /** @param array|mixed[] $values */
    public function newFormSucceeded(Form $form, array $values): void
    {
        $this->checkPermissions(Authorizator::CREATE);
        $role = new Role();
        $role->setName($values['name']);
        if ($values['parent'] !== 0) {
            /** @var Role $parent */
            $parent = $this->em->getRepository(Role::class)->find($values['parent']);
            $role->setParent($parent);
        }
        try {
            $this->em->persist($role);
            $this->em->flush($role);
            $this->cleanCache();
            $this->onCreate($role->getName());
        } catch (UniqueConstraintViolationException $exception) {
            $this->onExistingName($role->getName());
        }
        $this->redirect('this');
    }

    private function cleanCache(): void
    {
        $this->cache->clean([Cache::TAGS => [
            Authorizator::CACHE_NAMESPACE . '/permissions',
            Authorizator::CACHE_NAMESPACE . '/roles',
            Authorizator::CACHE_NAMESPACE . '/resources',
        ]]);
    }

}
