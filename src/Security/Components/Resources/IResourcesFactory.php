<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Security\Components\Resources;

interface IResourcesFactory
{

    public function create(): Resources;

}
