<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Security\DI;

use App\Extensions\CompilerExtension;
use Kdyby\Doctrine\DI\IEntityProvider;

class SecurityExtension extends CompilerExtension implements IEntityProvider
{

    public function loadConfiguration(): void
    {
        $this->parseConfig($this->getContainerBuilder(), __DIR__ . '/services.neon');
    }

    public function getEntityMappings(): array
    {
        return ['Security' => __DIR__ . '/../'];
    }

}
