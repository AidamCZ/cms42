<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Security;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Kdyby\Doctrine\Entities\MagicAccessors;

/**
 * @ORM\Entity
 * @ORM\Table(name="permissions")
 * @method bool getAllow()
 * @method bool getRead()
 * @method bool getCreate()
 * @method bool getUpdate()
 * @method bool getDelete()
 * @method Role getRole()
 * @method \Security\Resource getResource()
 */
class Permission
{

    use MagicAccessors;
    use Identifier;

    /**
     * @ORM\Column(type="boolean")
     * @var bool
     */
    protected $allow = true;

    /**
     * @ORM\Column(type="boolean", name="permission_read")
     * @var bool
     */
    protected $read = false;

    /**
     * @ORM\Column(type="boolean", name="permission_create")
     * @var bool
     */
    protected $create = false;

    /**
     * @ORM\Column(type="boolean", name="permission_update")
     * @var bool
     */
    protected $update = false;

    /**
     * @ORM\Column(type="boolean", name="permission_delete")
     * @var bool
     */
    protected $delete = false;

    /**
     * @ORM\ManyToOne(targetEntity="Security\Role", cascade={"persist"})
     * @ORM\JoinColumn(name="role_id", referencedColumnName="id", onDelete="CASCADE")
     * @var Role
     */
    protected $role;

    /**
     * @ORM\ManyToOne(targetEntity="Security\Resource", cascade={"persist"})
     * @ORM\JoinColumn(name="resource_id", referencedColumnName="id", onDelete="CASCADE")
     * @var \Security\Resource
     */
    protected $resource;

    public function getMethod(): string
    {
        return $this->getAllow() ? 'allow' : 'deny';
    }

    public function setResource(\Security\Resource $resource): Permission
    {
        $this->resource = $resource;
        return $this;
    }

    public function setRole(Role $role): Permission
    {
        $this->role = $role;
        return $this;
    }

    public function setAllow(bool $allow = true): Permission
    {
        $this->allow = $allow;
        return $this;
    }

    public function setCreate(bool $create = true): Permission
    {
        $this->create = $create;
        return $this;
    }

    public function setRead(bool $read = true): Permission
    {
        $this->read = $read;
        return $this;
    }

    public function setUpdate(bool $update = true): Permission
    {
        $this->update = $update;
        return $this;
    }

    public function setDelete(bool $delete = true): Permission
    {
        $this->delete = $delete;
        return $this;
    }

}
