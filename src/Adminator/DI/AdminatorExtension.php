<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Adminator\DI;

use App\Components\Css\Providers\ICssAdminProvider;
use App\Components\Js\Providers\IJsAdminProvider;
use App\Extensions\CompilerExtension;
use App\Extensions\Providers\IImagesProvider;

//TODO depends on Assets: Bootstrap, jQuery, Popper, FontAwesome
class AdminatorExtension extends CompilerExtension implements IImagesProvider, ICssAdminProvider, IJsAdminProvider
{

    public function getImages(): string
    {
        return \dirname(__DIR__) . '/assets/images';
    }

    public function getJsAdminFiles(): array
    {
        $dir = \dirname(__DIR__) . '/assets/js';
        return [
//            $dir . '/vendor.js',
//            $dir . '/docs.js',
            $dir . '/bootstrap.js',
            $dir . '/masonry.js',
            $dir . '/search.js',
            $dir . '/sidebar.js',
            $dir . '/email.js',
            $dir . '/utils.js',
            $dir . '/chart.js',
        ];

//import './masonry';
//import './charts';
//import './popover';
//import './scrollbar';
//import './search';
//import './sidebar';
//import './skycons';
//import './vectorMaps';
//import './chat';
//import './datatable';
//import './datepicker';
//import './email';
//import './fullcalendar';
//import './googleMaps';
//import './utils';
    }

    public function getCssAdminFiles(): array
    {
        $dir = \dirname(__DIR__) . '/assets/css';
        return [
            $dir . '/adminator.css',
        ];
    }

}
