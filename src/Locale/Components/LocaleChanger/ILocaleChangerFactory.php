<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Locale\Components\LocaleChanger;

interface ILocaleChangerFactory
{

    public function create(): LocaleChanger;

}
