<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Locale\Components\LocaleGrid;

use App\Components\BaseControl;
use App\Components\Datagrid\Datagrid;
use Kdyby\Doctrine\EntityManager;
use Locale\Exceptions\BadCodeException;
use Locale\Exceptions\LocaleWithCodeAlreadyExists;
use Locale\Locale;
use Locale\LocaleFacade;
use Nette\Caching\Cache;
use Nette\Caching\IStorage;
use Nette\Forms\Container;
use Nette\Utils\ArrayHash;
use Security\Authorizator;
use Url\Router;

/**
 * @method onExistingCode(string $code)
 * @method onBadCode(string $code, BadCodeException $exception)
 */
class LocaleGrid extends BaseControl
{

    private const TEMPLATE = __DIR__ . '/templates/LocaleGrid.latte';

    /** @var callable[] */
    public $onExistingCode;

    /** @var callable[] */
    public $onBadCode;

    /** @var EntityManager */
    private $em;

    /** @var Cache */
    private $cache;

    /** @var LocaleFacade */
    private $localeFacade;

    public function __construct(EntityManager $em, IStorage $storage, LocaleFacade $localeFacade)
    {
        parent::__construct();
        $this->cache = new Cache($storage);
        $this->em = $em;
        $this->localeFacade = $localeFacade;
    }

    public function render(): void
    {
        $this->template->setFile(self::TEMPLATE);
        $this->template->render();
    }

    protected function createComponentGrid(): Datagrid
    {
        $grid = $this->getDatagrid();
        $grid->addColumn('code', 'Code')->enableSort();
        $grid->addColumn('name', 'Language')->enableSort();
        $grid->addColumn('default', 'Default');
        $grid->setRowPrimaryKey('id');
        $grid->addCellsTemplate(__DIR__ . '/templates/@cells.datagrid.latte');

        $grid->setEditFormCallback([$this, 'saveLocale']);
        $grid->setAddCallback([$this, 'addLocale']);
        $grid->setDeleteCallback([$this, 'deleteLocale']);
        $grid->setDataSourceCallback(function ($filter, $order) {
            $orderBy = [];
            if (\is_array($order) && \count($order) === 2) {
                $orderBy[$order[0]] = $order[1];
            }
            return $this->em->getRepository(Locale::class)->findBy($filter, $orderBy);
//            return $this->em->getRepository(Locale::class)->createQueryBuilder('l')->whereCriteria($filter)->getQuery()->getResult();
        });

        $grid->setEditFormFactory(function ($row) {
            $form = $this->getDefaultFormFactory();
            /** @var Locale $row */
            !$row ?: $form->setDefaults([
                'code' => $row->getCode(),
                'name' => $row->getName(),
                'default' => $row->getDefault(),
            ]);
            return $form;
        });

        $grid->setAddFormFactory(function () {
            return $this->getDefaultFormFactory();
        });

        return $grid;
    }

    public function addLocale(Container $container): void
    {
        $this->checkPermissions(Authorizator::CREATE);
        /** @var ArrayHash $values */
        $values = $container->getValues();
        try {
            $locale = $this->localeFacade->fillValues($values);
            $this->localeFacade->save($locale);
            $this->cleanCache();
        } catch (LocaleWithCodeAlreadyExists $exception) {
            $this->onExistingCode($values['code']);
        } catch (BadCodeException $exception) {
            $this->onBadCode($values['code'], $exception);
        }
    }

    public function deleteLocale(string $id): void
    {
        $this->checkPermissions(Authorizator::DELETE);
        $locale = $this->em->getRepository(Locale::class)->find($id);
        if ($locale) {
            $this->em->remove($locale);
            $this->em->flush();
            $this->cleanCache();
        }
    }

    public function saveLocale(Container $form): void
    {
        $this->checkPermissions(Authorizator::UPDATE);
        /** @var ArrayHash $values */
        $values = $form->getValues();
        /** @var null|Locale $locale */
        $locale = $this->em->getRepository(Locale::class)->find($values['id']);
        if ($locale === null) {
            return;
        }
        try {
            $this->localeFacade->save($this->localeFacade->fillValues($values, $locale));
            $this->cleanCache();
        } catch (LocaleWithCodeAlreadyExists $exception) {
            $this->onExistingCode($values['code']);
        } catch (BadCodeException $exception) {
            $this->onBadCode($values['code'], $exception);
        }
    }

    private function getDefaultFormFactory(): Container
    {
        $form = new Container();
        $form->addText('code')->setRequired();
        $form->addText('name')->setRequired();
        $form->addCheckbox('default');
        return $form;
    }

    private function cleanCache(): void
    {
        $this->cache->clean([Cache::TAGS => ['locales']]);
        $this->cache->clean([Cache::TAGS => [Router::CACHE_NAMESPACE . '/locales']]);
    }

}
