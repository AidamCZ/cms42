window._stack.push([function (di, DOM) {
    let setup = false;
    let router = di.getService('router');
    let page = di.getService('page');

    router.getDOMRoute('.translations-row__delete')
        .on('match', function (evt) {
            if (setup) return;
            setup = true;

            page.getSnippet('snippet--pageContent')
                .setup(function () {
                    translations()
                })
                .teardown(function () {
                    setup = false;
                });
        });
}, {
    DOM: 'Utils.DOM'
}]);

function translations() {
    //DELETION
    deleteRow($('.translations-row__delete'));

    //ADDING
    let add_box = $('#translations-add');
    let form = $('#frm-translationsEditor-form');
    let fieldsets = $('#translations-wrapper fieldset');

    let addRowsCount = 0;
    $('#translations-add-button').on('click', function () {
        addRowsCount++;
        //Key
        let keyRow = '<div class="translations__row translations__row-add-key"> <a data-rows-count="' + addRowsCount + '" href="#" data-ajax="false" class="text-danger translations-row__delete"><i class="fa fa-close"></i></a> <input class="form-control translations-add-input" name="add-row-' + addRowsCount + '-key" id="add-row-' + addRowsCount + '-key" data-rows-count="' + addRowsCount + '"></div>'
        for (let i = 1; i < fieldsets.length; i++) {
            let localeCode = fieldsets[i].dataset.localeCode;
            let localeRow = '<div class="row"> <div class="w-90p translations__row"><input name="locale-add-' + localeCode + '-' + addRowsCount + '" id="locale-add-' + localeCode + '-' + addRowsCount + '" data-rows-count="' + addRowsCount + '" data-locale-code="' + localeCode + '" class="form-control add-row-input-locale"></div></div>';
            fieldsets.eq(i).find('legend').first().after(localeRow);
        }
        fieldsets.first().find('legend').first().after(keyRow);
        fieldsets.first().find('.translations-row__delete').first().on('click', function () {
            $(this).parent().remove();
            $('input.add-row-input-locale[data-rows-count="' + $(this).attr('data-rows-count') + '"]').parent().parent().remove();
        });
        let keyInput = $('#add-row-' + addRowsCount + '-key');
        keyInput.on('keyup', function () {
            if (keyInput.val().match(/^[a-zA-Z0-9._\-]+$/g) === null) {
                keyInput.css('background-color', 'LightCoral');
                return false; //break
            }
            $('.translations__row-key').each(function (index, el) {
                if (keyInput.val() === '' || $(this).text().replace(/ /g, '') === keyInput.val()) {
                    keyInput.css('background-color', 'LightCoral');
                    return false; //break
                }
                keyInput.css('background-color', 'Chartreuse');
            });
            let same = 0;
            $('.translations__row-add-key').each(function (index, el) {
                if (keyInput.val() === '' || $(this).find('input').first().val().replace(/ /g, '') === keyInput.val()) {
                    same++;
                }
            });
            keyInput.css('background-color', same > 1 ? 'LightCoral' : 'Chartreuse');
        });
    });

    form.on('submit', function () {
        let additions = {};
        $('.translations-add-input').each(function (index, el) {
            if ($(this).css('background-color') !== 'rgb(127, 255, 0)') { //haha
                return true;
            }
            let key = $(this).val();
            let rowsCount = el.dataset.rowsCount;

            $('.add-row-input-locale[data-rows-count="' + rowsCount + '"]').each(function (index, el) {
                const localeCode = el.dataset.localeCode;
                // let fullKey = localeCode + '_' + substrReplace(key, '.', '_');
                let fullKey = localeCode + '_' + key;
                additions[fullKey] = $(this).val();
            });
        });
        add_box.val(JSON.stringify(additions));
    })
}

function deleteRow(elements) {
    let delete_box = $('#translations-delete');
    elements.on('click', function () {
        let key = $(this).next().text();
        let currentValue = delete_box.val();
        let sep = currentValue === '' ? '' : ';';
        delete_box.val(currentValue + sep + key);
        $(this).parent().remove();
        $('input[title="' + key + '"]').parent().parent().remove();
    });
}
