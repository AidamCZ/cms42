<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Locale\AdminModule\Presenters;

use App\AdminModule\Presenters\BasePresenter;
use App\Components\Flashes\Flashes;
use Kdyby\Doctrine\EntityManager;
use Locale\Components\LocaleGrid\ILocaleGridFactory;
use Locale\Components\LocaleGrid\LocaleGrid;
use Locale\Components\TranslationsEditor\ITranslationsEditorFactory;
use Locale\Components\TranslationsEditor\TranslationsEditor;
use Locale\Exceptions\BadCodeException;
use Locale\Locale;
use Locale\Resources\ILangsResources;
use function implode;

class TranslationsPresenter extends BasePresenter
{

    /** @var EntityManager */
    private $em;

    /** @var ILangsResources */
    private $resources;

    /** @var string */
    private $resource;

    public function __construct(ILangsResources $resources, EntityManager $em)
    {
        parent::__construct();
        $this->em = $em;
        $this->resources = $resources;
    }

    public function actionList(): void
    {
        $this->setTitle('Translations list');
        $this->addHint('Default language', 'Note that you can set only one default language.');
        $this->addHint('Edit Translations', 'Open editor by clicking');
    }

    public function renderList(): void
    {
        $this->template->langs = $this->resources->list();
    }

    public function actionEdit(string $resource): void
    {
        $this->setTitle('Translations edit');
        if (!$this->resources->isValidResource($resource)) {
            $this->flashMessage('Lang not found', Flashes::ERROR);
            $this->redirect('list');
        }
        $this->resource = $resource;
    }

    protected function createComponentLocaleGrid(ILocaleGridFactory $factory): LocaleGrid
    {
        $control = $factory->create();
        $control->onBadCode[] = function (string $code, BadCodeException $exception): void {
            $message = $exception->getMessage() . '. ' . $code . ' given';
            $this->monolog->addError($message);
            $this->flashMessage($message, Flashes::ERROR);
            $this->sendPayload();
        };
        $control->onExistingCode[] = function (string $code): void {
            $message = sprintf('Locale with code "%s" already exists', $code);
            $this->monolog->addError($message);
            $this->flashMessage($message, Flashes::ERROR);
            $this->sendPayload();
        };
        return $control;
    }

    protected function createComponentTranslationsEditor(ITranslationsEditorFactory $factory): TranslationsEditor
    {
        $control = $factory->create($this->resource, $this->em->getRepository(Locale::class)->findAll());
        $control->onSave[] = function (array $success, array $errors): void {
            if ($errors) {
                $errorsStrings = [];
                /**
                 * @var \Exception $exception
                 * @var Locale $locale
                 */
                foreach ($errors as [$locale, $exception]) {
                    $errorsStrings[] = $locale->getName();
                    $this->monolog->addError($exception->getMessage());
                }
                $this->flashMessage('Error occurred while trying to save these translations: ' .
                    implode(', ', $errorsStrings), Flashes::ERROR);
            }
            if ($success) {
                $successStrings = [];
                /** @var Locale $locale */
                foreach ($success as $locale) {
                    $successStrings[] = $locale->getName();
                }
                $this->flashMessage('These translations were saved: ' . implode(', ', $successStrings), Flashes::SUCCESS);
            }
            $this->postGet('this');
            $this->sendPayload();
        };
        return $control;
    }

}
