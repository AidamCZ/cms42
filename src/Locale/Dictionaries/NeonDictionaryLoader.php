<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Locale\Dictionaries;

use Locale\Exceptions\DictionaryLoadException;
use Locale\Locale;
use function file_exists;
use function file_get_contents;

class NeonDictionaryLoader implements IDictionaryLoader
{

    /** @var string */
    private $langsDir;

    /** @var NeonDictionaryParser  */
    private $parser;

    public function __construct(string $langsDir)
    {
        $this->langsDir = $langsDir;
        $this->parser = new NeonDictionaryParser();
    }

    /**
     * @return string[]
     * @throws DictionaryLoadException
     */
    public function load(Locale $locale, string $resource): array
    {
        $code = $locale->getCode();
        $file = $this->langsDir . '/' . $resource . '/' . $resource . '.' . $code . '.neon';
        if (!file_exists($file)) {
            throw new DictionaryLoadException(sprintf('File %s not found', $file));
        }
        $content = file_get_contents($file);
        if ($content === false) {
            throw new DictionaryLoadException(sprintf('Cannot read content of file %s', $file));
        }
        return $this->parser->decode($content);
    }

}
