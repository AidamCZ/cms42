<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Locale\Dictionaries;

use Locale\Exceptions\DictionarySaveException;
use Locale\Locale;

interface IDictionarySaver
{

    /**
     * @param string[] $values
     * @throws DictionarySaveException
     */
    public function save(Locale $locale, string $resource, array $values): void;

}
