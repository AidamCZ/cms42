<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Locale\Resources;

use App\Helpers\FileSystem;

class DirectoryLangsResources implements ILangsResources
{

    /** @var string */
    private $langsDir;

    public function __construct(string $langsDir)
    {
        $this->langsDir = $langsDir;
    }

    /** @return string[] */
    public function list(): array
    {
        return FileSystem::scanDir($this->langsDir);
    }

    public function isValidResource(string $resource): bool
    {
        return \in_array($resource, $this->list(), true);
    }

}
