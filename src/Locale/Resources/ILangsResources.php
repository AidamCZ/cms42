<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Locale\Resources;

interface ILangsResources
{

    /** @return string[] */
    public function list(): array;

    public function isValidResource(string $resource): bool;

}
