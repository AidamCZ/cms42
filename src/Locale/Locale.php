<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Locale;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Kdyby\Doctrine\Entities\MagicAccessors;
use Locale\Exceptions\BadCodeException;
use function preg_match;

/**
 * @ORM\Entity
 * @ORM\Table(name="locales")
 * @method string getName()
 * @method string getCode()
 * @method bool getDefault()
 * @method setName(string $name)
 * @method setDefault(bool $default)
 */
class Locale
{

    use MagicAccessors;
    use Identifier;

    /**
     * @ORM\Column(type="string", unique=true)
     * @var string
     */
    protected $code;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    protected $name;

    /**
     * @ORM\Column(type="boolean", name="default_locale")
     * @var bool
     */
    protected $default = false;

    public function __construct(string $code, string $name, bool $default = false)
    {
        $this->setCode($code);
        $this->name = $name;
        $this->default = $default;
    }

    public function setCode(string $code): void
    {
        if (!preg_match('/[a-zA-Z0-9-]/', $code)) {
            throw new BadCodeException('Code must be alphanumeric string without special symbols (only "-" is allowed)');
        }
        $this->code = $code;
    }

}
