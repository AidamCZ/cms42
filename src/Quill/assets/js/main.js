function quillInit() {
    // let sourceButton = document.querySelector('.ql-showHtml');
    // sourceButton.addEventListener('click', function() {
    //     if (txtArea.style.display === '') {
    //         let html = txtArea.value;
    //         self.quill.pasteHTML(html)
    //     }
    //     txtArea.style.display = txtArea.style.display === 'none' ? '' : 'none'
    // });

    let editors = document.querySelectorAll('.quill-editor');
    if (editors.length) {
        for (let i = 0; i < editors.length; ++i) { //loop in case of multiple editors on page
            let options = JSON.parse(editors[i].parentElement.getAttribute('data-config'));
            let quill = new Quill(editors[i], options);
            let form = editors[i];
            //TODO source
            // var txtArea = document.createElement('textarea');
            // txtArea.style.cssText = "width: 100%;margin: 0px;background: rgb(29, 29, 29);box-sizing: border-box;color: rgb(204, 204, 204);font-size: 15px;outline: none;padding: 20px;line-height: 24px;font-family: Consolas, Menlo, Monaco, &quot;Courier New&quot;, monospace;position: absolute;top: 0;bottom: 0;border: none;display:none"
            //
            // var htmlEditor = quill.addContainer('ql-custom')
            // htmlEditor.appendChild(txtArea)
            //
            // var myEditor = document.querySelector('#editor')
            // quill.on('text-change', (delta, oldDelta, source) => {
            //     var html = myEditor.children[0].innerHTML
            //     txtArea.value = html
            // })
            //
            // var customButton = document.querySelector('.ql-showHtml');
            // customButton.addEventListener('click', function() {
            //     if (txtArea.style.display === '') {
            //         var html = txtArea.value
            //         self.quill.pasteHTML(html)
            //     }
            //     txtArea.style.display = txtArea.style.display === 'none' ? '' : 'none'
            // });

            do {
                form = form.parentElement;
            } while (form.tagName.toLowerCase() !== 'form');
            let hiddenInput = editors[i];
            do {
                hiddenInput = hiddenInput.previousElementSibling;
            } while (hiddenInput.tagName.toLowerCase() !== 'input');
            form.onsubmit = function () {
                hiddenInput.value = quill.container.firstChild.innerHTML; //update text of hidden input before POST
            };
            quill.clipboard.dangerouslyPasteHTML(hiddenInput.value); //sets default values from hidden input
            //
            // editors[i].addEventListener('click', function () {
            //     quill.focus();
            // });
        }
    }
}

window._stack.push([function (di, DOM) {
    let setup = false;
    let router = di.getService('router');
    let page = di.getService('page');

    router.getDOMRoute('.quill-editor')
        .on('match', function (evt) {
            if (setup) return;
            setup = true;

            page.getSnippet('snippet--pageContent')
                .setup(function () {
                    quillInit();
                })
                .teardown(function () {
                    setup = false;
                });
        });
}, {
    DOM: 'Utils.DOM'
}]);
