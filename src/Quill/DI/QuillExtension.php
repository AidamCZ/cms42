<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Quill\DI;

use App\Components\Css\Providers\ICssAdminProvider;
use App\Components\Css\Providers\ICssVendorProvider;
use App\Components\Js\Providers\IJsAdminProvider;
use App\Extensions\CompilerExtension;
use Nette\PhpGenerator\ClassType;
use Quill\QuillControl;

class QuillExtension extends CompilerExtension implements IJsAdminProvider, ICssAdminProvider, ICssVendorProvider
{

    /**
     * @var array|mixed[]
     * for configurable options @see https://quilljs.com/docs/configuration
     */
    private $defaults = [
        'modules' => [
            'toolbar' => [
                [['font' => []]],
                [['size' => ['small', false, 'large', 'huge']]],  // custom dropdown
//                [['header' => [1, 2, 3, 4, 5, 6, false]]],

                ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
                ['blockquote', 'code-block'],

                [['header' => 1], ['header' => 2]],               // custom button values
                [['list' => 'ordered'], ['list' => 'bullet']],
                [['script' => 'sub'], ['script' => 'super']],      // superscript/subscript
                [['indent' => '-1'], ['indent' => '+1']],          // outdent/indent
                [['direction' => 'rtl']],                         // text direction

                [ 'link', 'image', 'video', 'formula' ],
                [['color' => []], ['background' => []]],
                [['align' => []]],

                ['clean'],
                ['showHtml'],
            ],
        ],
        'theme' => 'snow',
    ];


//    public function loadConfiguration(): void
//    {
//        $cb = $this->getContainerBuilder();
//        $config = $this->validateConfig($this->defaults);
//    }

    public function afterCompile(ClassType $class): void
    {
        $config = $this->getConfig($this->defaults);
        $init = $class->methods['initialize'];
        $init->addBody(QuillControl::class . '::register(?, ?);', [$config, 'addQuill']);
    }

    /** @return string[] */
    public function getJsAdminFiles(): array
    {
        return [
            'https://cdn.quilljs.com/1.3.6/quill.min.js',
            \dirname(__DIR__) . '/assets/js/main.js',
        ];
    }

    /** @return string[] */
    public function getCssAdminFiles(): array
    {
        return [
            'https://cdn.quilljs.com/1.3.6/quill.snow.css',
            'https://cdn.quilljs.com/1.3.6/quill.bubble.css',
            \dirname(__DIR__) . '/assets/css/main.css',
        ];
    }

    /** @return string[] */
    public function getCssVendorFiles(): array
    {
        return [\dirname(__DIR__) . '/assets/css/front.css'];
    }

}
