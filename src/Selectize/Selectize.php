<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Selectize;

use Nette\Forms\Container;

class Selectize extends \App\Form\Control\Selectize
{

    /** @param array|mixed[] $values */
    public function setDefaultValues(array $values = []): void
    {
        $this->setOptions(['defaultValues' => $values]);
    }

    /** @phpcsSuppress SlevomatCodingStandard */
    public static function register($method = 'addSelectize', $config): void
    {
        Container::extensionMethod($method, function(Container $container, $name, $label, $entity = null, ?array $options = null) use ($config)
        {
            $container[$name] = new Selectize($label, $entity, \is_array($options) ?
                array_replace($config, $options) : $config);
            return $container[$name];
        });
    }

}
