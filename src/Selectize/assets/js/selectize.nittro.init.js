window._stack.push([function (di, DOM) {
    let setup = false;
    let router = di.getService('router');
    let page = di.getService('page');

    router.getDOMRoute('.selectize')
        .on('match', function (evt) {
            if (setup) return;
            setup = true;

            page.getSnippet('snippet--pageContent')
                .setup(function () {
                    selectize();
                })
                .teardown(function () {
                    setup = false;
                });
        });
}, {
    DOM: 'Utils.DOM'
}]);
