<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Pages\Components\PagesGrid;

use App\Components\BaseControl;
use App\Components\Datagrid\Datagrid;
use Kdyby\Doctrine\EntityManager;
use Nette\Forms\Container;
use Nette\Utils\Paginator;
use Pages\Page;
use Pages\PagesFacade;
use Pages\Query\PagesQuery;
use Security\Authorizator;

class PagesGrid extends BaseControl
{

    private const TEMPLATE = __DIR__ . '/templates/PagesGrid.latte';

    /** @var EntityManager */
    private $em;

    /** @var Page[] */
    private $pages;

    /** @var PagesFacade */
    private $pagesFacade;

    public function __construct(EntityManager $em, PagesFacade $pagesFacade)
    {
        parent::__construct();
        $this->em = $em;

        $this->pages = $em->getRepository(Page::class)->findAll();
        $this->pagesFacade = $pagesFacade;
    }

    public function render(): void
    {
        $this->template->setFile(self::TEMPLATE);
        $this->template->render();
    }

    protected function createComponentGrid(): Datagrid
    {
        $grid = $this->getDatagrid();
        $grid->addColumn('title', 'Title');
        $grid->addColumn('isPublished', 'Published');
        $grid->addColumn('preview', 'Preview');
        $grid->setRowPrimaryKey('id');

        $grid->setEditFormCallback([$this, 'savePage']);
        $grid->setDeleteCallback([$this, 'deletePage']);
        $grid->setDataSourceCallback([$this, 'getData']);

        $grid->addCellsTemplate(__DIR__ . '/templates/@cells.datagrid.latte');
//        $grid->setFilterFormFactory(function () {
//            $form = new Container();
//            $form->addText('title');
//            $form->addSubmit('filter', 'Filter data');
//                ->getControlPrototype()->class = 'btn btn-primary';
//            $form->addSubmit('cancel', 'Cancel filter');
//                ->getControlPrototype()->class = 'btn';
//
//            return $form;
//        });

        $grid->addGlobalAction('delete', 'Delete', function (array $ids, Datagrid $grid): void {
            $this->checkPermissions(Authorizator::DELETE);
            $pages = $this->em->getRepository(Page::class)->findBy(['id' => $ids]);
            //TODO cleanCache
            $this->em->remove($pages);
            $this->em->flush();
            $grid->redrawControl('rows');
        });

        $grid->setEditFormFactory(function ($row) {
            $form = $this->getDefaultFormFactory();
            /** @var Page $row */
            !$row ?: $form->setDefaults([
//                'title' => $row->getTitle(),
                'isPublished' => $row->isPublished(),
            ]);
            return $form;
        });

        $grid->setPagination(10, [$this, 'getDataCount']);

        return $grid;
    }

    /**
     * @param array|mixed[] $filter
     * @param array|mixed[] $order
     * @return Page[]
     */
    public function getData(?array $filter, ?array $order, ?Paginator $paginator = null): ?array
    {
//        $orderBy = []; //TODO to default setDataSourceCallback? in App\Components\Datagrid\Datagrid
//        if (\is_array($order) && \count($order) === 2) {
//            $orderBy[$order[0]] = $order[1];
//        }
//        $qb = $this->em->getRepository(Page::class)->fetch($qb);
//        $qb = $this->em->getRepository(Page::class)->createQueryBuilder('p');
//        if ($filter) {
//            foreach ($filter as $column => $value) {
//                if (\is_string($value)) {
//                    $qb->andWhere('p.' . $column . ' LIKE :' . $column)->setParameter($column, '%' . $value . '%');
//                }
//            }
//        }
//        foreach ($orderBy as $key => $value) {
//            $qb->addOrderBy('p.' .$key, $value);
//        }
//        $resultSet = new ResultSet($qb->getQuery($this->em->getRepository(Page::class)));
        $qb = (new PagesQuery())->withPageLocales();
        $resultSet = $this->em->getRepository(Page::class)->fetch($qb);
        if ($paginator) {
            $resultSet->applyPaginator($paginator);
        }
        return $resultSet->toArray();
    }

    public function getDataCount(): int
    {
        return \count($this->pages);
    }

    public function deletePage(string $id): void
    {
        $this->checkPermissions(Authorizator::DELETE);
        /** @var Page $page */
        $page = $this->em->getPartialReference(Page::class, $id);
        if ($page !== null) {
            //TODO cleanCache
            $this->pagesFacade->deletePage($page);
        }
        $this->postGet('this');
    }

    public function savePage(Container $form): void
    {
        $this->checkPermissions(Authorizator::UPDATE);
        $values = (array)$form->getValues(true);
        /** @var Page $page */
        $page = $this->em->getRepository(Page::class)->find($values['id']);
        if ($page !== null) {
            $this->setDefaultPageValues($page, $values);
            //TODO cleanCache
            $this->em->persist($page);
            $this->em->flush();
        }
    }

    public function getDefaultFormFactory(): Container
    {
        $form = new Container();
//        $form->addText('title')->setRequired();
        $form->addCheckbox('isPublished');
        return $form;
    }

    /** @param mixed[] $values */
    private function setDefaultPageValues(Page $page, array $values): void
    {
//        $page->setTitle($values['title']);
        $page->publish($values['isPublished']);
    }

}
