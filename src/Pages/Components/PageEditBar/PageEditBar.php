<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Pages\Components\PageEditBar;

use App\Components\BaseControl;
use Pages\Page;
use Pages\PagesFacade;
use Security\Authorizator;

//TODO testy (Unit)
/** @method onDelete(PageEditBar $pageEditBar) */
class PageEditBar extends BaseControl
{

    private const TEMPLATE = __DIR__ . '/PageEditBar.latte';

    /** @var callable[] */
    public $onDelete = [];

    /** @var Page */
    private $page;

    /** @var PagesFacade */
    private $pagesFacade;

    public function __construct(Page $page, PagesFacade $pagesFacade)
    {
        parent::__construct();
        $this->page = $page;
        $this->pagesFacade = $pagesFacade;
    }

    public function render(): void
    {
        if (!$this->getPresenter()->user->isAllowed('Pages:Admin:Pages:edit', Authorizator::READ)) {
            return;
        }
        $this->template->page = $this->page;
        $this->template->setFile(self::TEMPLATE);
        $this->template->render();
    }

    /** @secured */
    public function handlePublish(): void
    {
        $this->checkPermissions(Authorizator::UPDATE, 'Pages:Admin:Pages');
        if ($this->page === null) {
            $this->presenter->sendPayload();
            return;
        }
        $this->pagesFacade->publish($this->page, !$this->page->isPublished());
        if ($this->presenter->isAjax()) {
            $this->redrawControl('publishButton');
            $this->redrawControl('publishTag');
            $this->postGet('this');
        } else {
            $this->redirect('this');
        }
    }

    /** @secured */
    public function handleDelete(): void
    {
        $this->checkPermissions(Authorizator::DELETE, 'Pages:Admin:Pages');
        if ($this->page === null) {
            $this->presenter->sendPayload();
            return;
        }
        $this->pagesFacade->deletePage($this->page);
        $this->onDelete($this);
    }

}
