<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Pages\Components\UrlAliases;

use Url\Url;

interface IUrlAliasesFactory
{

    /** @param \Url\Url[] $urls */
    public function create(Url $mainUrl, array $urls): UrlAliases;

}
