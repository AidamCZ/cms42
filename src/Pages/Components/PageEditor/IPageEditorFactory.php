<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Pages\Components\PageEditor;

use Pages\Page;

interface IPageEditorFactory
{

    public function create(?Page $page = null, ?string $localeCode = null): PageEditor;

}
