<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Pages\Components\DynamicControls;

interface IDynamicControlsFactory
{

    public function create(): DynamicControls;

}
