<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Pages\Components\DynamicControls;

use App\Components\BaseControl;
use App\Components\TFormFactory;
use Nette\Forms\Form;
use Pages\Helpers\RenderControls;
use function md5;

class DynamicControls extends BaseControl
{

    use TFormFactory;

    private const TEMPLATE = __DIR__ . '/DynamicControls.latte';

    /** @var array|array[] array of arrays (Value objects od RenderableControls), it must be array for Selectize */
    private $dynamicControls;

    public function __construct(RenderControls $renderControls)
    {
        parent::__construct();
        $dynamicControls = $renderControls->getRenderableControls();
        foreach ($dynamicControls as $control) {
            $this->dynamicControls[] = $control->toArray();
        }
    }

    public function render(): void
    {
        $this->template->setFile(self::TEMPLATE);
        $this->template->render();
    }

    protected function createComponentForm(): Form
    {
        $form = $this->getAdminForm();

        $form->addSelectize('control', 'Control', $this->dynamicControls, [
            'valueField' => 'hash',
            'labelField' => 'name',
            'searchField' => 'classPath',
            'maxItems' => 1,
        ]);

        return $form;
    }

    public static function generateName(string $classPath): string
    {
        return '[__' . md5($classPath) . '__]';
    }

}
