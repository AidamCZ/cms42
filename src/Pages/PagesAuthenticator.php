<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Pages;

use Nette\Http\Session;
use Nette\Http\SessionSection;
use Nette\Security\Passwords;
use Nette\SmartObject;
use Nette\Utils\Strings;
use Pages\Exceptions\BadPassword;

//TODO remember user when he is logged in
class PagesAuthenticator
{

    use SmartObject;

    /** @var SessionSection */
    private $session;

    public function __construct(Session $session)
    {
        $this->session = $session->getSection(self::class);
    }

    public function isAuthenticated(Page $page): bool
    {
        return (bool)$this->session[$this->getKey($page)];
    }

    /** @throws BadPassword */
    public function authenticate(Page $page, string $password): void
    {
        if (!Passwords::verify($password, $page->getPassword())) {
            throw new BadPassword('Bad password');
        }
        $key = $this->getKey($page);
        $this->session->setExpiration('14 days', $key);
        $this->session[$key] = true;
    }

    private function getKey(Page $page): string
    {
        return md5('page_password' . $page->getId() . '-' . Strings::substring($page->getPassword(), -4));
    }

}
