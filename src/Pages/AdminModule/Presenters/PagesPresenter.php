<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Pages\AdminModule\Presenters;

use App\AdminModule\Presenters\BasePresenter;
use App\Components\Flashes\Flashes;
use Kdyby\Doctrine\EntityManager;
use Pages\Components\PageEditor\IPageEditorFactory;
use Pages\Components\PageEditor\PageEditor;
use Pages\Components\PagesGrid\IPagesGridFactory;
use Pages\Components\PagesGrid\PagesGrid;
use Pages\Page;
use Pages\Query\PagesQuery;

class PagesPresenter extends BasePresenter
{

    /** @var Page|null */
    private $editedPage;

    /** @var EntityManager */
    private $em;

    /** @var string|null */
    private $localeCode;

    public function __construct(EntityManager $em)
    {
        parent::__construct();
        $this->em = $em;
    }

    /**
     * TODO ajax auto save
     * TODO PagesQuery
     */
    public function actionEdit(?int $id = null, ?string $localeCode = null): void
    {
        if ($id === null) {
            $this->editedPage = null;
        } else {
            $this->loadPage($id);
        }
        $this->localeCode = $localeCode;
        $this->setTitle('Pages – Editor');
        $this->addHint('Page Editor', 'You can easily edit and publish your pages');
        $this->addHint('Title', 'Title is shown at the top of each page. You can of course hide it using checkbox');
        $this->addHint('HTML title', 'HTML title is displayed on the browser tabs and in the search results (Google)');
        $this->addHint('URL', 'You can set one primary URL and unlimited amount of aliases (if it is not already taken)');
        $this->addHint('Dynamic Controls',
            'This feature allows you to paste special renderable components and forms. Just choose one of the components
            and click "Generate control". It generates the code that you can paste into the Content box. There are only
            few renderable components by default. For more components you have to install additional plugins from the
            Plugins Center');
        $this->addHint('Custom CSS/JS', 'This boxes are useful when you need special design or features on you page');
        $this->addHint('Revert to previous version',
            'Click "Show previous versions" and click "Select" to apply changes');
        $this->addHint('Change Language',
            'You can change language for current page in the right section. You can also choose if you want to 
            overwrite existing localization or create new one');
    }

    public function actionDefault(): void
    {
        $this->setTitle('Pages – List');
        $this->addHint('List of pages', 'On this page you can publish and delete pages. Enter page editor by clicking title.');
    }

    protected function createComponentPageEditor(IPageEditorFactory $factory): PageEditor
    {
        $control = $factory->create($this->editedPage, $this->localeCode);

        $control->onSaveAndStay[] = function (PageEditor $control, Page $page): void {
            $this->flashMessage('Page was successfully saved', Flashes::SUCCESS);
            $this->redirect('this', $page->getId());
        };

        $control->onNoLocales[] = function (PageEditor $control): void {
            $message = 'There is no available localization, please create one';
            $this->flashMessage($message, Flashes::ERROR);
            $this->monolog->addInfo($message);
            $this->redirect(':Locale:Admin:Translations:list');
        };

        $control->onSaveAndRedirect[] = function (PageEditor $control, Page $page): void {
            $this->flashMessage('Page was successfully saved', Flashes::SUCCESS);
            $this->fullRedirect(':Pages:Front:Pages:', ['id' => $page->getId()]);
        };

        $control->onDelete[] = function (PageEditor $control): void {
            $this->flashMessage('Page was successfully deleted', Flashes::SUCCESS);
            $this->redirect(':Pages:Admin:Pages:');
        };

        $control->onException[] = function (PageEditor $control, \Throwable $exception): void {
            if (!\Tracy\Debugger::$productionMode) {
                $this->flashMessage($exception->getMessage(), Flashes::ERROR);
            } else {
                $this->flashMessage(
                    'Some errors occurs and page could not be saved Please try again or contact technical support',
                    Flashes::ERROR);
            }
            $this->postGet('this');
            $this->sendPayload();
        };
        return $control;
    }

    protected function createComponentPagesGrid(IPagesGridFactory $factory): PagesGrid
    {
        return $factory->create();
    }

    private function loadPage(int $id): void
    {
//        $pages = $this->editedPage = $this->em->getRepository(Page::class)->createQueryBuilder('p', 'p.id')
//            ->where('p.id = :id')->setParameter('id', $id)
//            ->leftJoin('p.url', 'u')->addSelect('u')
//            ->leftJoin('p.content', 'c')->addSelect('c')->getQuery()->getResult();
        $qb = (new PagesQuery())->whereId($id)->withPageLocales()->withUrls()->withContents();
        $pages = $this->editedPage = $this->em->getRepository(Page::class)->fetch($qb)->toArray();
        $this->editedPage = $pages ? $pages[$id] : null;
    }

}
