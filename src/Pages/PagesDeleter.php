<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Pages;

use Kdyby\Doctrine\EntityManager;
use Url\UrlFacade;

class PagesDeleter
{

    /** @var EntityManager */
    private $em;

    /** @var UrlFacade */
    private $urlFacade;

    public function __construct(EntityManager $em, UrlFacade $urlFacade)
    {
        $this->em = $em;
        $this->urlFacade = $urlFacade;
    }

    public function deletePage(Page $page): void
    {
        $url = $page->getUrl();
        $this->em->transactional(function () use ($page, $url): void {
            if ($url) {
                $this->urlFacade->delete($url);
            }
            $this->em->remove($page);
//            $this->em->flush();
        });
    }

    public function deletePageLocale(PageLocale $pageLocale): void
    {
        $this->em->remove($pageLocale);
        $this->em->flush();
    }

}
