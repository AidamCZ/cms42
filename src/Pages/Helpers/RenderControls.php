<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Pages\Helpers;

use Latte\Engine;
use Latte\Loaders\StringLoader;
use Nette\Application\UI\Presenter;
use Nette\Bridges\ApplicationLatte\Template;
use Nette\InvalidStateException;
use Nette\SmartObject;
use Pages\Exceptions\ControlIsNotRenderable;
use Pages\Exceptions\PresenterIsNotSet;
use function mb_substr;
use function preg_replace_callback;

//TODO testy (Unit)
//TODO cache
//TODO test rychlosti
/** This class must be used before render */
class RenderControls
{

    use SmartObject;

    /** @var Engine */
    private $latte;

    /** @var Presenter */
    private $presenter;

    /** @var RenderableControl[] Classes */
    private $renderableControls = [];

    public function addRenderableControl(RenderableControl $control): void
    {
        $this->renderableControls[$control->getHash()] = $control;
    }

    public function render(string $content): string
    {
        if (!$this->renderableControls) {
            return $content; //In case of empty renderableControls
        }
        if ($this->presenter === null) {
            throw new PresenterIsNotSet('You must set presenter using method RenderControls::setPresenter($presenter)');
        }

        //TODO dvojtečka pro custom render a parametry za voláním control funkce
        //TODO cachovat komponenty z DICu
        $parsedContent = preg_replace_callback('/\[\_\_[a-zA-Z0-9]+\_\_\]/', function ($control): string {
            try {
                //PARSING
                $hash = mb_substr($control[0], 3, -3);
                $controlInLatte = '{control ' . $hash . '}';
                //CHECKING IF IT IS RENDERABLE
                if (!$this->isRenderable($hash)) {
                    throw new Contro+lIsNotRenderable();
                }
                //ADDING COMPONENT AND RENDERING
                $this->presenter->addComponent(
                    $this->presenter->context->getByType($this->renderableControls[$hash]->getClassPath())->create(), $hash
                );
                return $this->latte->renderToString($controlInLatte);
            } catch (\InvalidArgumentException $exception) {
                //TODO component does not exists
            } catch (InvalidStateException $exception) {
                //TODO component already exists
            } catch (ControlIsNotRenderable $exception) {
                //TODO
            } catch (\Throwable $exception) {
//                throw $exception;
            }
            return $control[0]; //TODO vypisovat něco jiného (nic?)
        }, $content);
        return $parsedContent;
    }

    public function setPresenter(Presenter $presenter): void
    {
        /** @var Template $template */
        $template = $presenter->getTemplateFactory()->createTemplate($presenter);
        $latte = $template->getLatte();
        $latte->setLoader(new StringLoader());

        $this->latte = $latte;
        $this->presenter = $presenter;
    }

    private function isRenderable(string $class): bool
    {
        return \array_key_exists($class, $this->renderableControls);
    }

    /** @return RenderableControl[] */
    public function getRenderableControls(): array
    {
        return $this->renderableControls;
    }

}
