<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Pages;

use Kdyby\Doctrine\EntityManager;
use Locale\Locale;
use Navigation\MenuItem;
use Nette\SmartObject;
use Nette\Utils\ArrayHash;
use Url\Url;
use Url\UrlFacade;

/** @method onSetUrl() */
class PagesFiller
{

    use SmartObject;

    /** @var \Closure[] */
    public $onSetUrl = [];

    /** @var EntityManager */
    private $em;

    /** @var UrlFacade */
    private $urlFacade;

    /** @var PageUrlGenerator */
    private $urlGenerator;

    public function __construct(EntityManager $em, UrlFacade $urlFacade)
    {
        $this->em = $em;
        $this->urlGenerator = new PageUrlGenerator();
        $this->urlFacade = $urlFacade;
    }

    public function setLocale(Page $page, PageLocale $pageLocale, ArrayHash $values): PageLocale
    {
        $locale = $this->em->getRepository(Locale::class)->findOneBy(['code' => $values['locale']]);
        if ($locale !== null) {
            $pageLocaleFromDb = $this->getDbPageLocaleFromDb($page, $locale);
            if ($values['localeAction'] === 'add') {
                $pageLocale = $pageLocaleFromDb ?? new PageLocale($page, $locale);
            } else if ($pageLocaleFromDb !== null) {
//                    $this->deletePageLocale($pageLocale);
                $pageLocale = $pageLocaleFromDb;
            } else {
                $pageLocale->setLocale($locale);
            }
        }
        return $pageLocale;
    }

    public function setUrl(Page $page, ArrayHash $values): void
    {
        $url = $page->getUrl();
        if (!$url) { //Page does not have any url
            $url = $this->urlGenerator->generate($page, $values);
        } else if ($url->getUrlPath() !== $values->url) { //Page has different URL than new one from EditorForm (needs redirect)
            $oldUrl = $url;
            $url = $this->urlGenerator->generate($page, $values);

            $this->onSetUrl[] = function () use ($oldUrl, $url): void {
                $existingUrl = $this->em->getRepository(Url::class)->findOneBy([
                    'urlPath' => $url->getUrlPath(),
                    'redirect' => $oldUrl,
                ]);
                if ($existingUrl) { //if exists url contains same urlPath and redirects to the old url
                    $this->em->remove($existingUrl); //It can be removed
                    $this->em->flush($existingUrl);
                }

                /** @var MenuItem[]|null $menuItems */
                $menuItems = $this->em->getRepository(MenuItem::class)->findBy(['internalUrl' => $oldUrl]);
                if ($menuItems !== null) {
                    foreach ($menuItems as $menuItem) {
                        $menuItem->setInternalUrl($url);
                        $this->em->persist($menuItem);
                    }
                }

                /** @noinspection NullPointerExceptionInspection */
                $this->urlFacade->refreshRedirects($oldUrl, $url);
            };
        }

        $page->setUrl($url);

        $this->em->transactional(function () use ($page): void {
            $this->onSetUrl();
            $this->em->persist($page);
            $this->em->flush();
        });
    }

    public function setContent(PageLocale $pageLocale, ArrayHash $values): PageLocale
    {
        $oldContent = $pageLocale->getLatestContent();

        $newContent = new PageContent();
        $newContent->setContent($values->content);

        if ($oldContent === null || $oldContent->getContent() !== $values->content) {
            $pageLocale->addContent($newContent);
        }
        return $pageLocale;
    }


    private function getDbPageLocaleFromDb(Page $page, Locale $locale): ?PageLocale
    {
        $pageLocaleDb = $this->em->getRepository(PageLocale::class)->findOneBy([
            'page' => $page,
            'locale' => $locale,
        ]);
        return $pageLocaleDb; //could be null
    }

}
