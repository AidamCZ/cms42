<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Fixtures\DI;

use Fixtures\Command\LoadCommand;
use Fixtures\Command\PurgeCommand;
use Kdyby;
use Nette;

class FixturesExtension extends Nette\DI\CompilerExtension
{

    /** @var array|string[] */
    private $commands = [
        LoadCommand::class,
        PurgeCommand::class,
    ];

    public function loadConfiguration(): void
    {
        $containerBuilder = $this->getContainerBuilder();

        foreach ($this->commands as $i => $command) {
            $containerBuilder->addDefinition($this->prefix('cli.' . $i))
                ->addTag(Kdyby\Console\DI\ConsoleExtension::TAG_COMMAND)
                ->setInject(false)// lazy injects
                ->setClass($command);
        }
    }

}
