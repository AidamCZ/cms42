<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Fixtures\Basic;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Nette\Security\Passwords;
use Security\Role;
use Users\User;

class UserFixture extends AbstractFixture implements DependentFixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        $root = new User('root', Passwords::hash('root'));
        /** @var Role $role */
        $role = $this->getReference('role_root');
        $root->addRole($role);
        $manager->persist($root);

        $admin = new User('admin', Passwords::hash('admin'));
        /** @var Role $role */
        $role = $this->getReference('role_admin');
        $admin->addRole($role);
        $manager->persist($admin);

        /** @var Role $role */
        $role = $this->getReference('role_user');
        $user = new User('user', Passwords::hash('user'));
        $user->addRole($role);
        $manager->persist($user);

        $manager->flush();
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     * @return string[]
     */
    public function getDependencies(): array
    {
        return [RoleFixture::class];
    }

}
