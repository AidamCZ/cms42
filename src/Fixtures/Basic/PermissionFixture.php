<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Fixtures\Basic;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Security\Permission;
use Security\Resource;
use Security\Role;

class PermissionFixture extends AbstractFixture implements DependentFixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        /** @var Role $userRole */
        $userRole = $this->getReference('role_user');
        $adminRole = $this->getReference('role_admin');

        //RESOURCES
        $dashboardDefault = new Resource('Dashboard:Admin:Dashboard:default');
        $settings = new Resource('Settings:Admin:Settings');
        $settingsPermissions = new Resource('Settings:Admin:Settings:permissions');
        $usersDefault = new Resource('Users:Admin:Users:default');
        $plugins = new Resource('Plugins:Admin:Plugins');
        $pluginsCenter = new Resource('Plugins:Admin:PluginsCenter');
        $url = new Resource('Url:Admin:Url');
        $pages = new Resource('Pages:Admin:Pages');
        $stats = new Resource('Stats:Admin:Stats');
        $menu = new Resource('Navigation:Admin:Menu');
        $locale = new Resource('Locale:Admin:Translations');

        /**
         * @var array[]
         *  [ResourceName           allow, create, read,  update, delete, Role $role]
         */
        $permissions = [
            [$dashboardDefault,     true,  false,  true,  false,  false,  $userRole ],
            [$dashboardDefault,     true,  true,   true,  true,   true,   $adminRole],

            [$settings,             true,  false,  true,  false,  false,  $userRole ],
            [$settings,             true,  true,   true,  true,   true,   $adminRole],

            [$settingsPermissions,  true,  true,   true,  false,  false,  $adminRole],

            [$usersDefault,         true,  false,  true,  false,  false,  $userRole ],
            [$usersDefault,         true,  true,   true,  false,  false,  $adminRole],

            [$plugins,              true,  false,  true,  false,  false,  $userRole ],
            [$plugins,              true,  false,  true,  true,   true,   $adminRole],

            [$pluginsCenter,        true,  false,  true,  false,  false,  $userRole ],
            [$pluginsCenter,        true,  false,  true,  true,   true,   $adminRole],

            [$url,                  true,  false,  true,  false,  false,  $userRole ],
            [$url,                  true,  true,   true,  true,   false,  $adminRole],

            [$pages,                true,  false,  true,  false,  false,  $userRole ],
            [$pages,                true,  true,   true,  true,   true,   $adminRole],

            [$stats,                true,  false,  false, false,  false,  $userRole ],
            [$stats,                true,  false,  true,  false,  true,   $adminRole],

            [$menu,                 true,  false,  true,  false,  false,  $userRole ],
            [$menu,                 true,  true,   true,  true,   true,   $adminRole],

            [$locale,               true,  false,  true,  false,  false,  $userRole ],
            [$locale,               true,  true,   true,  true,   true,   $adminRole],
        ];

        foreach ($permissions as $permission) {
            $resource = $permission[0];
            $manager->persist($resource);

            $permissison = new Permission();
            $permissison
                ->setAllow($permission[1])
                ->setCreate($permission[2])
                ->setRead($permission[3])
                ->setUpdate($permission[4])
                ->setDelete($permission[5])
                ->setResource($resource)
                ->setRole($permission[6]);
            $manager->persist($permissison);
        }

        $manager->flush();
    }

    /** @return string[] */
    public function getDependencies(): array
    {
        return [RoleFixture::class];
    }

}
