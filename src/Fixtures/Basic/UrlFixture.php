<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Fixtures\Basic;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Url\Url;

class UrlFixture extends AbstractFixture implements DependentFixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        //FRONT
        $url = new Url();
        $url->setDestination('Front:Homepage');
        $url->setUrlPath('home');
        /** @var Url $homePageUrl */
        $homePageUrl = $this->getReference('page_home_url');
        $url->setRedirect($homePageUrl);
        $manager->persist($url);

        $siteMapUrl = new Url();
        $siteMapUrl->setDestination('Url:Front:Sitemap');
        $siteMapUrl->setUrlPath('sitemap.xml');
        $manager->persist($siteMapUrl);

        $url = new Url();
        $url->setDestination('Url:Front:Sitemap');
        $url->setUrlPath('sitemap');
        $url->setRedirect($siteMapUrl);
        $manager->persist($url);

        //AUTH
        $url = new Url();
        $url->setDestination('Auth:Sign', 'in');
        $url->setUrlPath('sign/in');
        $manager->persist($url);

        $url = new Url();
        $url->setDestination('Auth:Sign', 'out');
        $url->setUrlPath('sign/out');
        $manager->persist($url);

        //ADMIN
        $url = new Url();
        $url->setDestination('Dashboard:Admin:Dashboard');
        $url->setUrlPath('admin/dashboard');
        $manager->persist($url);

        $url = new Url();
        $url->setDestination('Settings:Admin:Settings');
        $url->setUrlPath('admin/settings');
        $manager->persist($url);

        $url = new Url();
        $url->setDestination('Settings:Admin:Settings', 'permissions');
        $url->setUrlPath('admin/settings/permissions');
        $manager->persist($url);

        $url = new Url();
        $url->setDestination('Users:Admin:Users');
        $url->setUrlPath('admin/users');
        $manager->persist($url);

        $url = new Url();
        $url->setDestination('Plugins:Admin:Plugins', 'settings');
        $url->setUrlPath('admin/plugins/settings');
        $manager->persist($url);

        $url = new Url();
        $url->setDestination('Plugins:Admin:Plugins');
        $url->setUrlPath('admin/plugins');
        $manager->persist($url);

        $url = new Url();
        $url->setDestination('Settings:Admin:Settings', 'rolesResources');
        $url->setUrlPath('admin/settings/roles-resources');
        $manager->persist($url);

        $url = new Url();
        $url->setDestination('Settings:Admin:Settings', 'localization');
        $url->setUrlPath('admin/settings/localization');
        $manager->persist($url);

        $url = new Url();
        $url->setDestination('Plugins:Admin:PluginsCenter');
        $url->setUrlPath('admin/plugins/center');
        $manager->persist($url);

        $url = new Url();
        $url->setDestination('Url:Admin:Url');
        $url->setUrlPath('admin/url');
        $manager->persist($url);

        $url = new Url();
        $url->setDestination('Pages:Admin:Pages');
        $url->setUrlPath('admin/pages');
        $manager->persist($url);

        $url = new Url();
        $url->setDestination('Pages:Admin:Pages', 'edit');
        $url->setUrlPath('admin/pages/edit');
        $manager->persist($url);

        $url = new Url();
        $url->setDestination('Pages:Front:Pages', 'protected');
        $url->setUrlPath('protected-page');
        $manager->persist($url);

        $url = new Url();
        $url->setDestination('Pages:Front:Pages', 'preview');
        $url->setUrlPath('admin/pages/preview');
        $manager->persist($url);

        $url = new Url();
        $url->setDestination('Stats:Admin:Stats', 'all');
        $url->setUrlPath('admin/stats/all');
        $manager->persist($url);

        $url = new Url();
        $url->setDestination('Stats:Admin:Stats', 'summary');
        $url->setUrlPath('admin/stats/summary');
        $manager->persist($url);

        $url = new Url();
        $url->setDestination('Navigation:Admin:Menu', 'list');
        $url->setUrlPath('admin/menu');
        $manager->persist($url);

        $url = new Url();
        $url->setDestination('Navigation:Admin:Menu', 'edit');
        $url->setUrlPath('admin/menu/edit');
        $manager->persist($url);

        $url = new Url();
        $url->setDestination('Navigation:Admin:Menu', 'new');
        $url->setUrlPath('admin/menu/new');
        $manager->persist($url);

        $url = new Url();
        $url->setDestination('Locale:Admin:Translations', 'list');
        $url->setUrlPath('admin/translations');
        $manager->persist($url);

        $url = new Url();
        $url->setDestination('Locale:Admin:Translations', 'edit');
        $url->setUrlPath('admin/translations/edit');
        $manager->persist($url);

        $manager->flush();
    }

    /** @return string[] */
    public function getDependencies(): array
    {
        return [PagesFixture::class];
    }

}
