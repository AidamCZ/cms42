<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Fixtures\Command;

use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Kdyby\Doctrine\EntityManager;
use Monolog\Logger;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;

class LoadCommand extends Command
{

    /** @var EntityManager @inject */
    public $em;

    /** @var Logger @inject */
    public $monolog;

    protected function configure(): void
    {
        $this
            ->setName('fixtures:load')
            ->setDescription('Load data fixtures to your database.')
            ->addOption('append', 'a', InputOption::VALUE_NONE,
                'Append the data fixtures instead of deleting all data from the database first.')
            ->addArgument('fixture_names', InputArgument::OPTIONAL | InputArgument::IS_ARRAY,
                'Select which fixtures should be executed, you can type multiple fixtures')
            ->setHelp(<<<EOT
The <info>fixtures:load</info> command loads data fixtures from your bundles:

  <info>php index.php fixtures:load</info>
  
At the end of command you can type name(s) of the fixture(s) which will be executed. Example: fixtures:load -a option url

If you want to append the fixtures instead of flushing the database first you can use the <info>--append</info> option:

  <info>php index.php fixtures:load --append</info>
EOT
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): ?int
    {
        /*
         * 1) Checking options and loading all fixtures
         */
        try {
            if ($input->isInteractive() && !$input->getOption('append')) {
                $helper = $this->getHelper('question');
                $question = new ConfirmationQuestion('This operation will purge the database. Do you wanna continue? [y/n]', false);

                if (!$helper->ask($input, $output, $question)) {
                    return 0;//no error
                }
            }

            $loader = new Loader();
            $loader->loadFromDirectory(__DIR__ . '/../Basic');
            $fixtures = $loader->getFixtures();
            $excluded = [];

            /*
             * 2) Filtering by arguments
             */
            $allowedFixtures = $input->getArgument('fixture_names');
            if ($allowedFixtures) {
                $resultFixtures = [];
                foreach ($allowedFixtures as $key => $allowedFixture) {
                    $allowedFixtures[$key] = rtrim(strtolower(trim($allowedFixture)), 'fixture');
                }
                foreach ($fixtures as $object) {
                    $className = \get_class($object);
                    $pos = strrpos($className, '\\');
                    $strippedFixture = strtolower(substr($className, $pos + 1));
                    if (\in_array(rtrim($strippedFixture, 'fixture'), $allowedFixtures, true)) {
                        $resultFixtures[$className] = $object;
                    }
                }
                $excludedFixtures = array_diff_key($fixtures, $resultFixtures);
                foreach ($excludedFixtures as $object) {
                    $fixtureName = \get_class($object);
                    $pos = strrpos($fixtureName, '\\');
                    $name = rtrim(strtolower(substr($fixtureName, (int)$pos + 1)), 'fixture'); //ez
                    $excluded[] = $name;
                    if (substr($name, \strlen($name)) !== 's') {
                        $excluded[] = $name . 's'; //TODO takhle pls ne
                    }
                }

                $fixtures = $resultFixtures;
            }
            /*
             * 3) Executing
             */
            $excluded[] = 'stats';
            $excluded[] = 'load_stamps';
            $purger = new ORMPurger(null, $excluded);
            $executor = new ORMExecutor($this->em, $purger);
            $executor->setLogger(function ($message) use ($output): void {
                $output->writeln(sprintf('<info>%s</info>', $message));
                $this->monolog->addDebug(sprintf('Fixture: %s', $message));
            });

            $executor->execute($fixtures, $input->getOption('append'));
            $output->writeln(sprintf('%d fixtures were executed', \count($fixtures)));

            return 0;//no error
        } catch (\Throwable $exception) {
            $output->writeln($exception->getMessage());
            $this->monolog->addError(sprintf('Fixture: %s', $exception->getMessage()));
            return 1;//error(s)
        }
    }

}
