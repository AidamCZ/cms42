<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Fixtures\Command;

use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Kdyby\Doctrine\EntityManager;
use Monolog\Logger;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;

class PurgeCommand extends Command
{

    /** @var EntityManager @inject */
    public $em;

    /** @var Logger @inject */
    public $monolog;

    protected function configure(): void
    {
        $this
            ->setName('fixtures:purge')
            ->setDescription('Purge all data in your database.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): ?int
    {
        try {
            if ($input->isInteractive()) {
                $helper = $this->getHelper('question');
                $question = new ConfirmationQuestion('This operation will purge the database. Do you wanna continue? [y/n]', false);

                if (!$helper->ask($input, $output, $question)) {
                    return 0;//no error
                }
            }

            $purger = new ORMPurger($this->em);
            $purger->purge();
            $this->monolog->addNotice('Database was purged');

            return 0;//no error
        } catch (\Throwable $exception) {
            $output->writeln($exception->getMessage());
            $this->monolog->addError(sprintf('Fixture: %s', $exception->getMessage()));
            return 1;//error(s)
        }
    }

}
