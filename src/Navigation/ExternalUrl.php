<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Navigation;

use Nette\Utils\Validators;

class ExternalUrl
{

    /** @var string */
    private $url;

    public function __construct(string $url)
    {
        $this->setUrl($url);
    }

    public function setUrl(string $url): void
    {
        if (!Validators::isUrl($url)) {
            throw new \InvalidArgumentException($url . ' – Invalid URL format.');
        }
        $this->url = $url;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function __toString(): string
    {
        return $this->getUrl();
    }

}
