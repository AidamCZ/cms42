<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Navigation\Components\AdminMenu;

use App\Components\BaseControl;

class AdminMenu extends BaseControl
{

    private const TEMPLATE = __DIR__ . '/templates/AdminMenu.latte';

    public function render(): void
    {
        $this->template->setFile(self::TEMPLATE);
        $this->template->render();
    }

}
