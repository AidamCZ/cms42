<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Navigation\Components\AdminMenu;

interface IAdminMenuFactory
{

    public function create(): AdminMenu;

}
