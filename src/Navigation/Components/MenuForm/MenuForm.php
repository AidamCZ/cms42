<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Navigation\Components\MenuForm;

use App\Components\BaseControl;
use App\Components\TFormFactory;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Navigation\Exceptions\InvalidMenuItemValues;
use Navigation\Menu;
use Navigation\MenuEditorParser;
use Navigation\MenuFacade;
use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;
use Security\Authorizator;

/**
 * @method onDuplicateName(MenuForm $self, string $name)
 * @method onException(MenuForm $self, \Throwable $exception, ArrayHash $values)
 * @method onSuccess(MenuForm $self, Menu $menu)
 * @method onInvalidMenuItemValues(array $values, InvalidMenuItemValues $exception)
 */
class MenuForm extends BaseControl
{

    use TFormFactory;

    private const TEMPLATE = __DIR__ . '/MenuForm.latte';

    /** @var callable[] */
    public $onDuplicateName = [];

    /** @var callable[] */
    public $onInvalidMenuItemValues = [];

    /** @var callable[] */
    public $onException = [];

    /** @var Menu */
    private $menu;

    /** @var MenuFacade */
    private $menuFacade;

    /** @var bool */
    private $edit;

    /** @var MenuEditorParser */
    private $parser;

    public function __construct(?Menu $menu, MenuFacade $menuFacade, MenuEditorParser $parser)
    {
        parent::__construct();
        $this->edit = $menu !== null;
        $this->menu = $menu ?? new Menu();
        $this->menuFacade = $menuFacade;
        $this->parser = $parser;
    }

    public function render(): void
    {
        $this->template->setFile(self::TEMPLATE);
        $this->template->render();
    }

    protected function createComponentForm(): Form
    {
        $form = $this->getAdminForm();

        $form->addText('name', 'Name')
            ->setRequired();

        $form->addMenuEditor('menuItems', $this->parser->encode($this->menu->getItems()->toArray()));

        $form->addSubmit('save', 'Save');
        $this->setDefaults($form);

        $form->onSuccess[] = [$this, 'formSucceeded'];
        return $form;
    }

    /** @param string[] $values */
    public function formSucceeded(Form $form, array $values): void
    {
        $this->checkPermissions($this->edit ? Authorizator::UPDATE : Authorizator::CREATE);
        try {
            $this->menu = $this->menuFacade->fillWithValues($this->menu, $values, true);
        } catch (InvalidMenuItemValues $exception) {
            $this->onInvalidMenuItemValues($values, $exception);
            return;
        }
        try {
            $this->menuFacade->save($this->menu);
            $this->onSuccess($this, $this->menu);
        } catch (UniqueConstraintViolationException $exception) {
            $this->onDuplicateName($this, $values['name']);
            return;
        }
//        $this->redirect('this'); TODO
    }

    private function setDefaults(Form $form): void
    {
        if (!$this->edit) {
            return; //only for existing menus
        }
        $m = $this->menu;
        $form->setDefaults(['name' => $m->getName()]);
    }

}
