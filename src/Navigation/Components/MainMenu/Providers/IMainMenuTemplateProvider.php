<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Navigation\Components\MainMenu\Providers;

interface IMainMenuTemplateProvider
{

    public function getMainMenuTemplate(): string;

}
