<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Navigation;

use function implode;
use function in_array;

class MenuItemTarget
{

    public const BLANK = '_blank',
        SELF = '_self',
        PARENT = '_parent',
        TOP = '_top';

    /** @var string */
    private $target;

    public function __construct(string $target = self::SELF)
    {
        $this->setTarget($target);
    }

    public function setTarget(string $target): void
    {
        if (!in_array($target, $this->getAllowedTypes(), true)) {
            throw new \InvalidArgumentException(
                'Invalid target. Choose on of the following: ' .
                implode(', ', $this->getAllowedTypes())
            );
        }
        $this->target = $target;
    }

    public function __toString(): string
    {
        return $this->getTarget();
    }

    public function getTarget(): string
    {
        return $this->target;
    }

    /** @return string[] */
    private function getAllowedTypes(): array
    {
        return [self::BLANK, self::SELF, self::PARENT, self::TOP];
    }

}
