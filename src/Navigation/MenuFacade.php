<?php declare(strict_types=1);

/** Copyright (c) 2018 Adam Zelycz (https://www.adamzelycz.cz) */

namespace Navigation;

use Kdyby\Doctrine\EntityManager;
use Navigation\Exceptions\InvalidMenuItemValues;
use Navigation\Query\MenuQuery;
use Nette\Caching\Cache;
use Nette\Caching\IStorage;
use function array_shift;

class MenuFacade
{

    public const CACHE_NAMESPACE = 'CMS.Menu';

    /** @var EntityManager */
    private $em;

    /** @var Cache */
    private $cache;

    /** @var MenuEditorParser */
    private $parser;

    public function __construct(EntityManager $em, IStorage $storage, MenuEditorParser $parser)
    {
        $this->em = $em;
        $this->cache = new Cache($storage, self::CACHE_NAMESPACE);
        $this->parser = $parser;
    }

    public function select(Menu $menu): void
    {
//        $this->em->beginTransaction();
        /** @var Menu[] $menus */
        $menus = $this->em->getRepository(Menu::class)->findAll();
        foreach ($menus as $men) {
            $men->select(false);
        }
//        $this->em->persist($menus);
        $menu->select();
        $this->cleanCache();
//        $this->em->commit();
    }

    public function getSelectedMenu(): ?Menu
    {
        $menuArray = $this->cache->load('selected_menu', function (& $dependencies): ?array {
            $dependencies = [Cache::TAGS => ['menu']];
            $menu = $this->em->getRepository(Menu::class)->fetch((new MenuQuery())->whereIsSelected()->withItems()->withItemsUrls());
            return $menu->toArray();
        });
        return array_shift($menuArray);
    }

    public function save(Menu $menu): void
    {
        $this->em->persist($menu);
        $this->em->flush();
        $this->cleanCache();
    }

    /** @param string[] $values */
    public function fillWithValues(Menu $menu, array $values, bool $deleteOldItems = false): Menu
    {
        $menu->setName($values['name']);
        try {
            $items = $this->parser->decode($values['menuItems']);
        } catch (Exceptions\BadUrlFormat $exception) {
            throw new InvalidMenuItemValues($exception->getMessage(), 0, $exception);
        }
        if ($deleteOldItems) {
            $this->deleteMenuItems($menu);
        }
        $menu->setItems($items);
        return $menu;
    }

    public function deleteMenuItems(Menu $menu): void
    {
        foreach ($menu->getItems() as $item) {
            $this->em->remove($item);
        }
//        $this->em->flush();
    }

    public function delete(Menu $menu): void
    {
        $this->em->remove($menu);
        $this->em->flush();
        $this->cleanCache();
    }

    private function cleanCache(): void
    {
        $this->cache->clean([Cache::TAGS => ['menu']]);
    }

}
