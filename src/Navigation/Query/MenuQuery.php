<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Navigation\Query;

use Kdyby\Doctrine\QueryBuilder;
use Kdyby\Doctrine\QueryObject;
use Kdyby\Persistence\Queryable;

class MenuQuery extends QueryObject
{

    /** @var array|\Closure[] */
    private $filter = [];

    /** @var array|\Closure[] */
    private $select = [];

    public function withItems(): MenuQuery
    {
        $this->select[] = function (QueryBuilder $qb): void {
            $qb->addSelect('mi')
                ->leftJoin('m.items', 'mi')
                ->addOrderBy('mi.id');
        };
        return $this;
    }

    public function withItemsUrls(): MenuQuery
    {
        $this->select[] = function (QueryBuilder $qb): void {
            $qb->addSelect('miu')
                ->leftJoin('mi.internalUrl', 'miu');
        };
        return $this;
    }

    public function whereId(int $id): MenuQuery
    {
        $this->filter[] = function (QueryBuilder $qb) use ($id): void {
            $qb->andWhere('m.id = :id')->setParameter('id', $id);
        };
        return $this;
    }

    public function whereIsSelected(): MenuQuery
    {
        $this->filter[] = function (QueryBuilder $qb): void {
            $qb->andWhere('m.selected = true');
//                ->setMaxResults(1); //Only one item can be selected;
        };
        return $this;
    }

    /** @return \Doctrine\ORM\Query|\Doctrine\ORM\QueryBuilder */
    protected function doCreateQuery(Queryable $repository)
    {
        $qb = $this->createBasicDql($repository);
        foreach ($this->select as $modifier) {
            $modifier($qb);
        }
        return $qb->addOrderBy('m.name');
    }

    /** @return \Doctrine\ORM\Query|\Doctrine\ORM\QueryBuilder|QueryBuilder */
    protected function doCreateCountQuery(Queryable $repository)
    {
        $qb = $this->createBasicDql($repository)
            ->select('COUNT(m.id) AS total_count');
        return $qb;
    }

    private function createBasicDql(Queryable $repository): QueryBuilder
    {
        $qb = $repository->createQueryBuilder('m', 'm.id');
        foreach ($this->filter as $modifier) {
            $modifier($qb);
        }
        return $qb;
    }

}
