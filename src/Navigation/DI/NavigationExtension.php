<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Navigation\DI;

use App\Extensions\CompilerExtension;
use Kdyby\Doctrine\DI\IEntityProvider;

class NavigationExtension extends CompilerExtension implements IEntityProvider
{

    public function loadConfiguration(): void
    {
        $this->parseConfig($this->getContainerBuilder(), __DIR__ . '/services.neon');
    }

    public function beforeCompile(): void
    {
        $this->setPresenterMapping($this->getContainerBuilder(), ['Navigation' => 'Navigation\\*Module\\Presenters\\*Presenter']);
    }

    /** @return string[] */
    public function getEntityMappings(): array
    {
        return ['Navigation' => __DIR__ . '/../'];
    }

}
