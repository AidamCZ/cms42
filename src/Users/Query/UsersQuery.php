<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Users\Query;

use Kdyby\Doctrine\QueryBuilder;
use Kdyby\Doctrine\QueryObject;
use Kdyby\Persistence\Queryable;

class UsersQuery extends QueryObject
{

    /** @var array|\Closure[] */
    private $filter = [];

    /** @var array|\Closure[] */
    private $select = [];

    public function withRoles(): UsersQuery
    {
        $this->select[] = function (QueryBuilder $qb): void {
            $qb->addSelect('r')
                ->leftJoin('u.roles', 'r');
        };
        return $this;
    }

    /** @return \Doctrine\ORM\Query|\Doctrine\ORM\QueryBuilder */
    protected function doCreateQuery(Queryable $repository)
    {
        $qb = $this->createBasicDql($repository);
        foreach ($this->select as $modifier) {
            $modifier($qb);
        }
        return $qb->addOrderBy('u.username');
    }

    /** @return \Doctrine\ORM\Query|\Doctrine\ORM\QueryBuilder|QueryBuilder */
    protected function doCreateCountQuery(Queryable $repository)
    {
        $qb = $this->createBasicDql($repository)
            ->select('COUNT(u.id) AS total_count');
        return $qb;
    }

    private function createBasicDql(Queryable $repository): QueryBuilder
    {
        $qb = $repository->createQueryBuilder('u', 'u.id');
        foreach ($this->filter as $modifier) {
            $modifier($qb);
        }
        return $qb;
    }

}
