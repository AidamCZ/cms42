<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Users\AdminModule\Presenters;

use App\AdminModule\Presenters\BasePresenter;
use App\Components\Flashes\Flashes;
use Users\Components\UsersGrid\IUsersGridFactory;
use Users\Components\UsersGrid\UsersGrid;
use Users\User;

class UsersPresenter extends BasePresenter
{

    public function actionDefault(): void
    {
        $this->setTitle('Users');
    }

    protected function createComponentUsersGrid(IUsersGridFactory $factory): UsersGrid
    {
        $control = $factory->create();
        $control->onEdit[] = function (User $user): void {
            $this->flashMessage('Your changes have been saved', Flashes::SUCCESS);
            $this->monolog->addInfo('User has been edited', [$user]);
            $this->postGet('this');
        };
        $control->onCreate[] = function (User $user): void {
            $this->flashMessage('User has been created', Flashes::SUCCESS);
            $this->monolog->addInfo('User has been created', [$user]);
            $this->postGet('this');
        };
        $control->onDelete[] = function (User $user): void {
            $this->flashMessage('User was deleted');
            $this->monolog->addInfo('User was deleted', [$user]);
            $this->postGet('this');
        };
        $control->onUserAlreadyExists[] = function (User $user): void {
            $this->flashMessage('User with this username already exists', Flashes::ERROR);
            $this->postGet('this');
            $this->sendPayload();
        };
        $control->onBadPermissions[] = function (UsersGrid $usersGrid): void {
            $this->flashMessage('You do not have enough permission to do that', Flashes::ERROR);
            $this->postGet('this');
            $this->sendPayload();
        };
        $control->onError[] = function (UsersGrid $usersGrid): void {
            $this->flashMessage('Something went wrong, please refresh page and try again', Flashes::ERROR);
            $this->monolog->addError('ERROR: UsersGrid');
            $this->postGet('this');
            $this->sendPayload();
        };
        return $control;
    }

}
