<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Users\Components\SignInForm;

use App\Components\BaseControl;
use App\Components\Flashes\Flashes;
use App\Components\TFormFactory;
use Kdyby\Translation\Translator;
use Monolog\Logger;
use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;

/**
 * @method onSignIn(SignInForm $self)
 * @method onBadLogin(SignInForm $self, \Nette\Security\AuthenticationException $exception)
 */
class SignInForm extends BaseControl
{

    use TFormFactory;

    private const TEMPLATE = __DIR__ . '/SignInForm.latte';

    /** @var callable[] */
    public $onSignIn;

    /** @var callable[] */
    public $onBadLogin;

    /** @var Translator */
    private $translator;

    /** @var Logger */
    private $monolog;

    public function __construct(Translator $translator, Logger $monolog)
    {
        parent::__construct();
        $this->translator = $translator;
        $this->monolog = $monolog;
    }

    public function render(): void
    {
        $this->template->setFile(self::TEMPLATE);
        $this->template->render();
    }

    protected function createComponentSignInForm(): Form
    {
        $form = $this->getForm();
        $form->setTranslator($this->translator->domain('forms.signIn'));

        $form->addText('username', 'username.label')
            ->setHtmlAttribute('autofocus')
            ->setRequired('username.required');

        $form->addPassword('password', 'password.label')
            ->setRequired('password.required');
        $form->addCheckbox(
            'remember', $this->translator->translate('forms.signIn.remember.caption') //BootstrapRender bug
        )
            ->setDefaultValue(false);

        $form->addSubmit('submit', 'send.caption');

        $form->onSuccess[] = [$this, 'signInFormSucceeded'];

        return $form;
    }

    public function signInFormSucceeded(Form $form, ArrayHash $values): void
    {
        try {
            $this->presenter->getUser()->login($values->username, $values->password);
            $this->presenter->getUser()->setExpiration(
                $values->remember ? '+ 14 DAYS' : '+ 30 MINUTES'
            );
            $this->monolog->addInfo('User logged in', [$values->username]);
            $this->onSignIn($this);

            $this->presenter->flashMessage('You were signed in', Flashes::SUCCESS);
            $this->presenter->redirect(':Front:Homepage:');
        } catch (\Nette\Security\AuthenticationException $exception) {
            $this->monolog->addError('Bad Login Credentials', [$values->username]);
            $this->onBadLogin($this, $exception);

            $this->flashMessage('Bad credentials', Flashes::ERROR);
            $this->postGet('this');
        }
    }

}
