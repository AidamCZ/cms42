<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Users\Components\UsersGrid;

use App\Components\BaseControl;
use App\Components\TFormFactory;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use IPub\VisualPaginator\Components as VisualPaginator;
use Kdyby\Doctrine\EntityManager;
use Kdyby\Doctrine\ResultSet;
use Nette\Application\UI\Form;
use Nette\Security\Passwords;
use Security\Authorizator;
use Security\Role;
use Users\Query\UsersQuery;
use Users\User;
use Users\UsersFacade;

/**
 * @method onEdit(User $user)
 * @method onDelete(User $user)
 * @method onCreate(User $user)
 * @method onUserAlreadyExists(User $user)
 * @method onError(UsersGrid $self)
 * TODO testy (akceptační)
 * TODO při změně usera refreshnout identitu (respektive asi kontrolovat podle DB každý request?)
 */
class UsersGrid extends BaseControl
{

    use TFormFactory;

    private const TEMPLATE = __DIR__ . '/UsersGrid.latte';

    /** @var callable[] */
    public $onDelete;
    /** @var callable[] */
    public $onCreate;
    /** @var callable[] */
    public $onEdit;
    /** @var callable[] */
    public $onError;
    /** @var callable[] */
    public $onUserAlreadyExists;

    /** @var EntityManager */
    private $em;
    /** @var ResultSet */
    private $users;
    /** @var UsersFacade */
    private $usersFacade;

    public function __construct(EntityManager $em, UsersFacade $usersFacade)
    {
        parent::__construct();
        $this->em = $em;

        $this->users = $em->getRepository(User::class)->fetch((new UsersQuery())->withRoles());
        $this->usersFacade = $usersFacade;
    }

    public function render(): void
    {
        if ($this->users instanceof ResultSet) {
            $this->users->applyPaginator($this['page']->getPaginator(), 5);
        }
        $this->template->users = $this->users;
        $this->template->setFile(self::TEMPLATE);
        $this->template->render();
    }

    /** @secured */
    public function handleRemove(int $id): void
    {
        $this->checkPermissions(Authorizator::DELETE);

        /** @var User $user */
        $user = $this->em->getPartialReference(User::class, $id);
        if ($user !== null) {
            $this->em->remove($user);
            $this->em->flush($user);
            $this->onDelete($user);
            $this->redrawControl('users');
        } else {
            $this->onError($this);
        }
    }

    /** @param array|mixed[] $values */
    public function userFormSucceeded(Form $form, array $values): void
    {
        if (empty($values['id'])) { //NEW USER
            $this->checkPermissions(Authorizator::CREATE);
            $user = new User($values['username'], Passwords::hash($values['password']));
            $user->setIp($values['ip']);
            if ($values['roles']) {
                foreach ((array)$values['roles'] as $roleId) {
                    /** @var Role $role */
                    $role = $this->em->getRepository(Role::class)->find($roleId);
                    if ($role !== null) {
                        $user->addRole($role);
                    }
                }
            }

            try {
                $this->usersFacade->registerUser($user);
            } catch (UniqueConstraintViolationException $exception) {
                $this->onUserAlreadyExists($user);
                $this->presenter->redirect('this');
            }
            $values['id'] = $user->getId();
            $this->users = [$user];

            $this->onCreate($user);
        } else { //EXISTING USER
            $this->checkPermissions(Authorizator::UPDATE);
            /** @var User $user */
            $user = $this->em->getRepository(User::class)->find($values['id']);
            if ($user !== null) {
                $user->setIp($values['ip']);
                $user->setUsername($values['username']);
                if ($values['password']) {
                    $user->setPassword(Passwords::hash($values['password']));
                }

                $this->em->transactional(function () use ($user, $values): void {
                    $user->cleanRoles();
                    foreach ((array)$values['roles'] as $roleId) {
                        /** @var Role $role */
                        $role = $this->em->getRepository(Role::class)->find($roleId);
                        if ($role !== null) {
                            $user->addRole($role);
                        }
                    }
                });

                $this->em->persist($user);
                $this->em->flush();
                $this->users = [$user];

                $this->onEdit($user);
            } else {
                $this->onError($this);
            }
        }
        $this->redrawControl('users');
    }

    protected function createComponentUserForm(): Form
    {
        $form = $this->getForm();
        $form->addProtection();
        $form->addHidden('id');
        $form->addText('username', 'Username')
            ->setRequired(true)
            ->addRule(Form::MAX_LENGTH, 'Maximum length is %d', 255);
        $form->addPassword('password', 'Password')
            ->addConditionOn($form['id'], Form::BLANK)
            ->setRequired();
        $form->addCheckboxList('roles', 'Role', $this->em->getRepository(Role::class)->findPairs([], 'name', [], 'id'));
        $form->addText('ip', 'Ip')
            ->setRequired(false)
            ->addRule(Form::MAX_LENGTH, 'Maximum length is %d', 255);
        $form->addSubmit('submit', 'Save');
        $form->onSuccess[] = [$this, 'userFormSucceeded'];
        return $form;
    }

    protected function createComponentPage(): VisualPaginator\Control
    {
        $control = new VisualPaginator\Control();
        $presenter = $this->getPresenter();
        $control->setTemplateFile('bootstrap.latte');
        $control->onShowPage[] = function ($component, $page) use ($presenter): void {
            $presenter->redirect('this', ['page' => $page]);
        };
        return $control;
    }

}
