<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Users;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Kdyby\Doctrine\EntityManager;
use Monolog\Logger;
use Nette\SmartObject;

class UsersFacade
{

    use SmartObject;

    /** @var EntityManager */
    private $em;
    /** @var Logger */
    private $monolog;

    public function __construct(EntityManager $em, Logger $monolog)
    {
        $this->em = $em;
        $this->monolog = $monolog;
    }

    public function registerUser(User $user): void
    {
        try {
            $this->em->persist($user);
            $this->em->flush();
        } catch (UniqueConstraintViolationException $exception) {
            $this->monolog->addError('Registration failed. User with this username already exists', [$user]);
            throw $exception;
        }
    }

}
