<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Settings;

use Kdyby\Doctrine\EntityManager;
use Nette\Caching\Cache;
use Nette\Caching\IStorage;
use Nette\SmartObject;

/**
 * Class SettingsFacade
 * @package Settings
 */
class SettingsFacade
{

    use SmartObject;

    private const CACHE_NAMESPACE = 'CMS.Settings';

    /** @var EntityManager */
    private $em;

    /** @var Cache */
    private $cache;

    public function __construct(EntityManager $em, IStorage $storage)
    {
        $this->em = $em;
        $this->cache = new Cache($storage, self::CACHE_NAMESPACE);
    }

    /**
     * Vrací textocý řetězec obsahující hodnotu nastavení
     * @param string $key
     * @param string|null $pluginIdentifier
     * @return null|string
     */
    public function getOptionValue(string $key, ?string $pluginIdentifier = null): ?string
    {
        $cacheKey = $pluginIdentifier !== null ? $pluginIdentifier . '/' . $key : $key;
        $value = $this->cache->load($cacheKey, function (& $dependencies) use ($key, $pluginIdentifier, $cacheKey) {
            /** @var Option|null|mixed $option */
            $option = $this->getOption($key, $pluginIdentifier);

            if ($option === null) {
                return null;
            }
            $dependencies = [Cache::TAGS => [$cacheKey]];
            return $option->getValue();
        });
        return $value;
    }

    /**
     * Mění obsah jednoho nastavení podle zadaného klíče a ukládá hodnotu do cache
     * @param string $key
     * @param string $value
     * @param string|null $name
     * @param string|null $pluginIdentifier
     */
    public function setOption(string $key, ?string $value, ?string $name = null, ?string $pluginIdentifier = null): void
    {
        /** @var Option|null|mixed $option */
        $option = $this->getOption($key, $pluginIdentifier);

        if ($option === null) {
            return;
        }
        if ($option->getValue() !== $value) {
            $option->setValue($value);
            $this->em->persist($option);
            $this->em->flush();
            $this->cache->clean([Cache::TAGS => [$pluginIdentifier !== null ? $pluginIdentifier . '/' . $key : $key]]);
        }
    }

    private function getOption(string $key, ?string $pluginIdentifier = null): ?Option
    {
        if ($pluginIdentifier === null) {
            $option = $this->em->getRepository(Option::class)->findOneBy(
                ['key eq' => $key, 'plugin' => null]
            );
        } else {
            $option = $this->em->getRepository(Option::class)->findOneBy(
                ['key eq' => $key, 'plugin.identifier' => $pluginIdentifier]
            );
        }
        return $option;
    }

}
