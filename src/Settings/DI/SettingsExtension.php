<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Settings\DI;

use App\Extensions\CompilerExtension;
use Kdyby\Doctrine\DI\IEntityProvider;

/**
 * Class SettingsExtension
 * @package Settings\DI
 */
class SettingsExtension extends CompilerExtension implements IEntityProvider
{

    public function loadConfiguration(): void
    {
        $builder = $this->getContainerBuilder();
        $this->parseConfig($builder, __DIR__ . '/services.neon');
    }

    public function beforeCompile(): void
    {
        $this->setPresenterMapping(
            $this->getContainerBuilder(), ['Settings' => 'Settings\\*Module\\Presenters\\*Presenter']
        );
    }

    public function getEntityMappings(): array
    {
        return [
            'Settings' => [__DIR__ . '/../'],
            'Settings\Rules' => [__DIR__ . '/../Rules'],
        ];
    }

}
