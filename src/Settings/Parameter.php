<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Settings;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Kdyby\Doctrine\Entities\MagicAccessors;

/**
 * @ORM\Entity
 * @ORM\Table(name="option_parameters")
 * @method string getValue()
 * @method string getTitle()
 * @method setOption(Option $option)
 * @method setValue(string $value)
 * @method setTitle(string $title)
 * This entity is used when option has many parameters (selectbox, list, ....)
 */
class Parameter
{

    use Identifier;
    use MagicAccessors;

    /**
     * @ORM\ManyToOne(targetEntity="Option", inversedBy="parameters")
     * @var Option
     */
    protected $option;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    protected $value;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    protected $title;

    public function __construct(string $value, string $title)
    {
        $this->value = $value;
        $this->title = $title;
    }

    /** @return array|mixed[] */
    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'value' => $this->value,
            'title' => $this->title,
        ];
    }

}
