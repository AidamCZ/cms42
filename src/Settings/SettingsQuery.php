<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Settings;

use Kdyby\Doctrine\QueryBuilder;
use Kdyby\Doctrine\QueryObject;
use Kdyby\Persistence\Queryable;

class SettingsQuery extends QueryObject
{

    /** @var array|\Closure[] */
    private $filter = [];

    /** @var array|\Closure[] */
    private $select = [];

    public function wherePlugin(?string $identifier = null): SettingsQuery
    {
        $this->filter[] = function (QueryBuilder $qb) use ($identifier): void {
            $qb->andWhere('s.plugin = :identifier')->setParameter('identifier', $identifier);
        };
        return $this;
    }

    public function isPlugin(): SettingsQuery
    {
        $this->filter[] = function (QueryBuilder $qb): void {
            $qb->whereCriteria(['s.plugin not' => null]);
        };
        return $this;
    }

    public function isNotPlugin(): SettingsQuery
    {
        $this->filter[] = function (QueryBuilder $qb): void {
            $qb->whereCriteria(['s.plugin' => null]);
        };
        return $this;
    }

    /** @return \Doctrine\ORM\Query|\Doctrine\ORM\QueryBuilder */
    protected function doCreateQuery(Queryable $repository)
    {
        $qb = $this->createBasicDql($repository);
        foreach ($this->select as $modifier) {
            $modifier($qb);
        }
        return $qb->addOrderBy('s.key');
    }

    /** @return \Doctrine\ORM\Query|\Doctrine\ORM\QueryBuilder|QueryBuilder */
    protected function doCreateCountQuery(Queryable $repository)
    {
        $qb = $this->createBasicDql($repository)
            ->select('COUNT(s.id) AS total_count');
        return $qb;
    }

    private function createBasicDql(Queryable $repository): QueryBuilder
    {
        $qb = $repository->createQueryBuilder('s', 's.id');
        foreach ($this->filter as $modifier) {
            $modifier($qb);
        }
        return $qb;
    }

}
