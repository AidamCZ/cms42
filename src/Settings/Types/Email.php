<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Settings\Types;

use Czubehead\BootstrapForms\Inputs\TextInput;
use Nette\Forms\Controls\BaseControl;
use Nette\Forms\Form;
use Settings\Option;

class Email implements IType
{

    public function getName(): string
    {
        return 'email';
    }

    public function getInput(Option $option): BaseControl
    {
        /** @var TextInput $text */
        $text = (new Text())->getInput($option);
        return $text
            ->setHtmlType('email')
            ->addRule(Form::EMAIL);
    }

}
