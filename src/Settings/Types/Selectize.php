<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Settings\Types;

use Nette\Forms\Controls\BaseControl;
use Settings\Option;

class Selectize implements IType, ITypeSelectable
{

    public function getName(): string
    {
        return 'selectize';
    }

    public function getInput(Option $option): BaseControl
    {
        $parameters = $option->getParameters();
        $parametersArray = [];
        foreach ($parameters as $parameter) {
            $parametersArray[] = $parameter->toArray();
        }
        $render = $option->getRenderOptions();
        $control = new \Selectize\Selectize($option->getName() ?? $option->getKey(), $parametersArray, [
            'mode' => $render['selectize_mode'] ?? 'full',
            'create' => $render['selectize_create'] ?? true,
            'maxItems' => $render['selectize_maxItems'] ?? null,
            'delimiter' => $render['selectize_delimiter'] ?? '#/',
            'plugins' => $render['selectize_plugins'] ?? ['remove_button'],
            'valueField' => $render['selectize_valueField'] ?? 'id',
            'labelField' => $render['selectize_labelField'] ?? 'name',
            'searchField' => $render['selectize_searchField'] ?? 'name',
        ]);
        if ($option->getValue()) {
            $control->setDefaultValues([$option->getValue()]);
        }
        return $control;
    }

}
