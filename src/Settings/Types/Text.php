<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Settings\Types;

use Czubehead\BootstrapForms\Inputs\TextInput;
use Nette\Forms\Controls\BaseControl;
use Settings\Option;

class Text implements IType
{

    public function getName(): string
    {
        return 'text';
    }

    public function getInput(Option $option): BaseControl
    {
        return (new TextInput($option->getName() ?? $option->getKey()))
            ->setDefaultValue($option-> getValue());
    }

}
