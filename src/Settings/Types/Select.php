<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Settings\Types;

use Czubehead\BootstrapForms\Inputs\SelectInput;
use Nette\Forms\Controls\BaseControl;
use Settings\Option;

class Select implements IType, ITypeSelectable
{

    public function getName(): string
    {
        return 'select';
    }

    public function getInput(Option $option): BaseControl
    {
        $parameters = $option->getParameters();
        $parametersArray = [];
        foreach ($parameters as $parameter) {
            $parametersArray[$parameter->getValue()] = $parameter->getTitle();
        }
        return (new SelectInput($option->getName() ?? $option->getKey(), $parametersArray))
            ->setDefaultValue($option->getValue() ?: null);
    }

}
