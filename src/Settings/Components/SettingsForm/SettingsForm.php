<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Settings\Components\SettingsForm;

use App\Components\BaseControl;
use App\Components\TFormFactory;
use Nette\Application\UI\Form;
use Security\Authorizator;
use Settings\Option;
use Settings\SettingsFacade;

/**
 * Class SettingsForm
 * @package Settings\Components\SettingsForm
 * @method onSuccess(SettingsForm $self)
 * TODO testy (akceptační)
 */
class SettingsForm extends BaseControl
{

    use TFormFactory;

    private const TEMPLATE = __DIR__ . '/SettingsForm.latte';

    /** @var Option[] */
    private $options = [];

    /** @var SettingsFacade */
    private $settingsFacade;

    /**
     * @param Option[] $options
     */
    public function __construct(array $options, SettingsFacade $settingsFacade)
    {
        parent::__construct();
        $this->settingsFacade = $settingsFacade;
        foreach ($options as $option) {
            $this->options[$option->getKey()] = $option;
        }
    }

    public function render(): void
    {
        $this->template->options = $this->options;
        $this->template->setFile(self::TEMPLATE);
        $this->template->render();
    }

    protected function createComponentForm(): Form
    {
        $form = $this->getForm();
        $form->addProtection();
        $lastPlugin = null;
        /** @var Option $option */
        foreach ($this->options as $option) {
            $plugin = $option->getPlugin();
            if ($plugin !== null && $plugin !== $lastPlugin) {
                $form->addGroup($plugin->getName());
                $lastPlugin = $plugin;
            }
            $input = $option->getType()->getInput($option);
            foreach ($option->getRules() as $rule) {
                $input->addRule($rule->getValidator(), $rule->getErrorMessage(), $rule->getArgument());
            }
            $form->addComponent($input, $option->getKey());
        }
        $form->addSubmit('save', 'Save');
        $form->onSuccess[] = [$this, 'formSucceeded'];
        return $form;
    }

    /** @param array|mixed[] $values */
    public function formSucceeded(Form $form, array $values): void
    {
        $this->checkPermissions(Authorizator::UPDATE);
        foreach ($values as $key => $value) {
            $this->settingsFacade->setOption($key, $value);
        }
        $this->onSuccess($this);
        $this->presenter->redirect('this');
    }

}
