<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Settings\Components\SettingsBar;

use App\Components\BaseControl;

class SettingsBar extends BaseControl
{

    private const TEMPLATE = __DIR__ . '/SettingsBar.latte';

    /** @var array|mixed[] */
    private $items;

    /**
     * @param array|mixed[] $items
     * TODO ValueObject
     */
    public function __construct(array $items)
    {
        parent::__construct();
        $this->items = $items;
    }

    public function render(): void
    {
        $this->template->items = $this->items;
        $this->template->active = $parameters['active'] ?? null;
        $this->template->setFile(self::TEMPLATE);
        $this->template->render();
    }

}
