<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Settings\Components\SettingsBar;

interface ISettingsBarFactory
{

    /** @param array|mixed[] $items */
    public function create(array $items): SettingsBar;

}
