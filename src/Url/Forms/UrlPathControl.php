<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Url\Forms;

use Czubehead\BootstrapForms\Inputs\TextInput;
use Nette\Application\UI\Form;

class UrlPathControl extends TextInput
{

    public function __construct(?string $label = null, int $maxLength = 254)
    {
        parent::__construct($label, $maxLength);
        $this->addRule(Form::PATTERN, 'Please enter valid URL format. URL cannot contain special characters', '^[a-zA-Z0-9-_~!%/@#]*$');
        $this->setRequired(false);
    }

}
