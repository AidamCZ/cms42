<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Url;

use Kdyby\Doctrine\EntityManager;
use Monolog\Logger;
use Nette\Caching\Cache;
use Nette\Caching\IStorage;
use Nette\SmartObject;
use function sprintf;

class UrlFacade
{

    use SmartObject;

    /** @var EntityManager */
    private $em;

    /** @var Cache */
    private $cache;

    /** @var Logger */
    private $monolog;

    public function __construct(EntityManager $em, IStorage $storage, Logger $monolog)
    {
        $this->em = $em;
        $this->cache = new Cache($storage, Router::CACHE_NAMESPACE);
        $this->monolog = $monolog;
    }

    public function refreshRedirects(Url $from, ?Url $to): void
    {
        /** @var Url $oldUrl */
        foreach (array_merge($this->em->getRepository(Url::class)->findBy(['redirect' => $from]), [$from]) as $oldUrl) {
            $oldUrl->setRedirect($to); //may be null
            $this->em->persist($oldUrl);
            $this->cleanCache($oldUrl);
        }
        $this->em->flush();
    }

    public function delete(Url $url): void
    {
        $this->monolog->addDebug(sprintf('Deleting URL %s', $url->getUrlPath()));
        $this->em->remove($url);
        $this->em->flush();
        $this->cleanCache($url);
    }

    public function save(Url $url): void
    {
        $this->monolog->addDebug(sprintf('Url %s is being saved', $url->getUrlPath()));
        $this->em->persist($url);
        $this->em->flush();
        $this->cleanCache($url);
    }

    /** @return Url[] */
    public function getAliases(Url $url): array
    {
        return $this->em->getRepository(Url::class)->findBy(['redirect' => $url]);
    }

    public function cleanCache(Url $url): void
    {
        $this->cache->clean([Cache::TAGS => ['route/' . $url->getId()]]);
    }

}
