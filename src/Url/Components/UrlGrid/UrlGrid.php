<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Url\Components\UrlGrid;

use App\Components\BaseControl;
use App\Components\Datagrid\Datagrid;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Kdyby\Doctrine\EntityManager;
use Kdyby\Doctrine\ResultSet;
use Nette\Forms\Container;
use Nette\Utils\Paginator;
use Security\Authorizator;
use Url\Url;
use Url\UrlFacade;
use function mb_strtolower;

/**
 * @method onExistingUrl(Url $url)
 */
class UrlGrid extends BaseControl
{

    private const TEMPLATE = __DIR__ . '/templates/UrlGrid.latte';

    /** @var callable[] */
    public $onExistingUrl = [];

    /** @var EntityManager */
    private $em;

    /** @var Url[] */
    private $urls;

    /** @var UrlFacade */
    private $urlFacade;

    public function __construct(EntityManager $em, UrlFacade $urlFacade)
    {
        parent::__construct();
        $this->em = $em;

        $this->urls = $em->getRepository(Url::class)->findAll();
        $this->urlFacade = $urlFacade;
    }

    public function render(): void
    {
        $this->template->setFile(self::TEMPLATE);
        $this->template->render();
    }

    protected function createComponentGrid(): Datagrid
    {
        $grid = $this->getDatagrid();
        $grid->addColumn('presenter', 'Presenter')->enableSort();
        $grid->addColumn('action', 'Action');
        $grid->addColumn('urlPath', 'URL')->enableSort();
        $grid->addColumn('internalId', 'Internal ID');
        $grid->addColumn('redirect', 'Redirect');
        $grid->setRowPrimaryKey('id');

        $grid->setEditFormCallback([$this, 'saveUrl']);
        $grid->setAddCallback([$this, 'addUrl']);
        $grid->setDeleteCallback([$this, 'deleteUrl']);
        $grid->setDataSourceCallback([$this, 'getData']);

        $grid->addCellsTemplate(__DIR__ . '/templates/@cells.datagrid.latte');
        $grid->setFilterFormFactory(function () {
            $form = new Container();
            $form->addText('presenter');
            $form->addText('action');
            $form->addText('urlPath');
            $form->addSubmit('filter', 'Filter data');
//                ->getControlPrototype()->class = 'd-none btn btn-secondary';
            $form->addSubmit('cancel', 'Cancel filter');
//                ->getControlPrototype()->class = 'btn';

            return $form;
        });

        $grid->addGlobalAction('delete', 'Delete', function (array $ids, Datagrid $grid): void {
            $this->checkPermissions(Authorizator::DELETE);
            $urls = $this->em->getRepository(Url::class)->findBy(['id' => $ids]);
            $this->cleanCache($urls);
            $this->em->remove($urls);
            $this->em->flush();
            $grid->redrawControl('rows');
        });

        $grid->setEditFormFactory(function ($row) {
            $form = $this->getDefaultFormFactory();
            /** @var Url $row */
            !$row ?: $form->setDefaults([
                'presenter' => $row->getPresenter(),
                'action' => $row->getAction(),
                'urlPath' => $row->getUrlPath(),
                'internalId' => $row->getInternalId(),
                'redirect' => $row->getRedirect() !== null ? $row->getRedirect()->getId() : 0,
            ]);
            return $form;
        });

        $grid->setAddFormFactory([$this, 'getDefaultFormFactory']);

        $grid->setPagination(10, [$this, 'getDataCount']);

        return $grid;
    }

    /**
     * @param array|mixed[] $filter
     * @param array|mixed[] $order
     * @return Url[]
     */
    public function getData(?array $filter, ?array $order, ?Paginator $paginator = null): ?array
    {
        $orderBy = []; //TODO to default setDataSourceCallback? in App\Components\Datagrid\Datagrid
        if (\is_array($order) && \count($order) === 2) {
            $orderBy[$order[0]] = $order[1];
        }
        $qb = $this->em->getRepository(Url::class)->createQueryBuilder('u');
        if ($filter) {
            foreach ($filter as $column => $value) {
                if (\is_string($value)) {
                    $qb->andWhere('LOWER(u.' . $column . ') LIKE :' . $column)->setParameter($column, '%' . mb_strtolower($value) . '%');
                }
            }
        }
        foreach ($orderBy as $key => $value) {
            $qb->addOrderBy('u.' .$key, $value);
        }
        $resultSet = new ResultSet($qb->getQuery());
        if ($paginator) {
            $resultSet->applyPaginator($paginator);
        }
        return $resultSet->toArray();
    }

    public function getDataCount(): int
    {
        return \count($this->urls);
    }

    public function addUrl(Container $container): void
    {
        $this->checkPermissions(Authorizator::CREATE);
        $values = (array)$container->getValues(true);
        $url = new Url();
        $this->setDefaultUrlValues($url, $values);
        try {
            $this->em->persist($url);
            $this->em->flush();
        } catch (UniqueConstraintViolationException $exception) {
            $this->onExistingUrl($url);
        }
    }

    public function deleteUrl(string $id): void
    {
        $this->checkPermissions(Authorizator::DELETE);
        /** @var Url $url */
        $url = $this->em->getRepository(Url::class)->find($id);
        if ($url !== null) {
            $this->cleanCache([$url]);
            $this->em->remove($url);
            $this->em->flush();
        }
        $this->postGet('this');
    }

    public function saveUrl(Container $form): void
    {
        $this->checkPermissions(Authorizator::UPDATE);
        $values = (array)$form->getValues(true);
        /** @var Url $url */
        $url = $this->em->getRepository(Url::class)->find($values['id']);
        if ($url !== null) {
            $this->setDefaultUrlValues($url, $values);
            try {
                $this->cleanCache([$url]);
                $this->em->persist($url);
                $this->em->flush();
            } catch (UniqueConstraintViolationException $exception) {
                $this->onExistingUrl($url);
            }
        }
    }

    public function getDefaultFormFactory(): Container
    {
        $destinations = [''];
        foreach ($this->urls as $url) {
            $destinations[$url->getId()] = $url->getUrlPath() !== '' ? $url->getUrlPath() : '/';
        }
        $form = new Container();
        $form->addText('presenter');
        $form->addText('action');
        $form->addText('urlPath');
        $form->addInteger('internalId');
        $form->addSelectize('redirect', null, $destinations, [
            'mode' => 'select',
            'allowEmptyOption' => true,
            'maxItems' => 1,
        ]);
        return $form;
    }

    /** @param mixed[] $values */
    private function setDefaultUrlValues(Url $url, array $values): void
    {
        $url->setDestination($values['presenter'] ?: null, $values['action'] ?: null);
        $url->setUrlPath($values['urlPath']);
        $url->setInternalId($values['internalId'] ?: null);
        if ($values['redirect']) {
            $redirectUrl = $this->em->getPartialReference(Url::class, $values['redirect']);
            if ($redirectUrl instanceof Url) {
                $url->setRedirect($redirectUrl);
            }
        } else {
            $url->setRedirect(null);
        }
    }

    /** @param Url[] $urls */
    private function cleanCache(array $urls): void
    {
        /** @var Url $url */
        foreach ($urls as $url) {
            $this->urlFacade->cleanCache($url);
        }
    }

}
