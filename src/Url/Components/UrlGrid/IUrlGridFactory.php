<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Url\Components\UrlGrid;

interface IUrlGridFactory
{

    public function create(): UrlGrid;

}
