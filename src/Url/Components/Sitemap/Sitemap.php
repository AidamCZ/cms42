<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Url\Components\Sitemap;

use App\Components\BaseControl;
use Kdyby\Doctrine\EntityManager;
use Kdyby\Doctrine\EntityRepository;
use Kdyby\Doctrine\ResultSet;
use Locale\Locale;
use Locale\LocaleFacade;
use Pages\Page;
use Pages\Query\PagesQuery;

final class Sitemap extends BaseControl
{

    private const TEMPLATE = __DIR__ . '/Sitemap.latte',
        DEFAULT_URLS = [':Front:Homepage:', ':Auth:Sign:in'];

    /** @var EntityRepository */
    private $pageDao;

    /** @var LocaleFacade */
    private $localeFacade;

    public function __construct(EntityManager $em, LocaleFacade $localeFacade)
    {
        parent::__construct();
        $this->pageDao = $em->getRepository(Page::class);
        $this->localeFacade = $localeFacade;
    }

    public function render(): void
    {
        $this->template->urls = $this->getUrls();
        $this->template->setFile(self::TEMPLATE);
        $this->template->render();
    }

    /** @return array|array[] */
    private function getUrls(): array
    {
        $urls = [];
        $pages = $this->findPages();
        $locales = $this->findLocales();
        foreach ($pages as $page) {
            foreach ($locales as $locale) {
                $link = $this->getPageLink($locale, $page);
                $urls[$link] = [
                    'priority' => $locale->getDefault() ? 0.8 : 0.7,
                    'change' => 'weekly',
                    'alternate' => $this->getPageAlternates($locales, $page),
                ];
            }
        }
        //DEFAULT URLS (may overwrite some pages (e.g. homepage))
        foreach ($locales as $locale) {
            foreach (self::DEFAULT_URLS as $url) {
                $urls[$this->getLink($locale, $url)] = [
                    'priority' => $locale->getDefault() ? 1 : 0.9,
                    'change' => 'weekly',
                    'alternate' => $this->getAlternates($locales, $url),
                ];
            }
        }

        return $urls;
    }

    /** @return Page[] */
    private function findPages(): array
    {
        /** @var ResultSet $result */
        $result = $this->pageDao->fetch((new PagesQuery())->isPublished());
        return $result->toArray();
    }

    /** @return Locale[] */
    private function findLocales(): array
    {
        return $this->localeFacade->getAll();
    }

    private function getPageLink(Locale $locale, Page $page): string
    {
        return $this->presenter->link('//:Pages:Front:Pages:default', [
            'locale' => $locale->getCode(),
            'id' => $page->getId(),
        ]);
    }

    private function getLink(Locale $locale, string $presenter): string
    {
        return $this->presenter->link('//' . $presenter, ['locale' => $locale->getCode()]);
    }

    /**
     * @param Locale[] $locales
     * @return array|array[]
     */
    private function getPageAlternates(array $locales, Page $page): array
    {
        $alternates = [];
        foreach ($locales as $alternateLocale) {
            $alternates[] = [
                'link' => $this->getPageLink($alternateLocale, $page),
                'code' => $alternateLocale->getCode(),
            ];
        }
        return $alternates;
    }

    /**
     * @param Locale[] $locales
     * @return array|array[]
     */
    private function getAlternates(array $locales, string $presenter): array
    {
        $alternates = [];
        foreach ($locales as $alternateLocale) {
            $alternates[] = [
                'link' => $this->getLink($alternateLocale, $presenter),
                'code' => $alternateLocale->getCode(),
            ];
        }
        return $alternates;
    }

}
