<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Plugins\Remove;

use App\Helpers\FileSystem;
use Nette\SmartObject;
use Plugins\AllPluginsContainer;
use Plugins\Exceptions\PluginIsNotInstalled;
use Plugins\Exceptions\PluginRemoveException;
use Plugins\Install\Installer;
use Plugins\Plugin;

class Remover
{

    use SmartObject;

    /** @var Installer */
    private $installer;

    /** @var string */
    private $pluginsDir;

    /** @var Plugin[] */
    private $allPlugins;

    public function __construct(string $pluginsDir, Installer $installer, AllPluginsContainer $allPluginsContainer)
    {
        $this->installer = $installer;
        $this->pluginsDir = $pluginsDir;
        $this->allPlugins = $allPluginsContainer->getAllPlugins();
    }

    public function remove(string $identifier, bool $quiet = false): void
    {
        try {
            $this->installer->uninstall($identifier);
        } catch (PluginIsNotInstalled $e) {
            if (!array_key_exists($identifier, $this->allPlugins)) {
                throw new PluginRemoveException('This plugin is not available');
            }
        }
        $packageDir = $this->pluginsDir . '/' . $identifier;
        if (file_exists($packageDir)) {
            FileSystem::purge($packageDir, true, false, true);
        }
        $vendor = explode('/', $identifier)[0];
        $vendorDir = $this->pluginsDir . '/' . $vendor;
        if (file_exists($vendorDir) && FileSystem::isDirEmpty($vendorDir)) {
            rmdir($vendorDir);
        }
    }

}
