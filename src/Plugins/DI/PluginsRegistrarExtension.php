<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Plugins\DI;

use App\Helpers\FileSystem;
use Composer\Autoload\ClassLoader;
use Nette\DI\Extensions\ExtensionsExtension;
use Nette\DI\Statement;
use Nette\Reflection\ClassType;
use Nette\Utils\Strings;
use Plugins\AllPluginsContainer;
use Plugins\Install\IInstallable;
use Plugins\Install\Installer;
use Plugins\IPlugin;
use Plugins\Plugin;

class PluginsRegistrarExtension extends ExtensionsExtension
{

    /** @var \Composer\Autoload\ClassLoader */
    private $loader;

    /** @var string */
    private $wwwDir;

    /** @var Plugin[] */
    private $availablePlugins;

    /** @var string[]|null[] */
    private $installClasses;

    public function __construct(string $wwwDir, ClassLoader $loader)
    {
        $this->loader = $loader;
        $this->wwwDir = $wwwDir;
    }

    public function loadConfiguration(): void
    {
        $cb = $this->getContainerBuilder();
        $classMap = $this->loader->getClassMap();
        $classMap = array_filter($classMap, function ($class) {
            // Just filter relevant classes because of errors in vendor dependencies
            return Strings::match($class, '/[pP]lugins\/.*Extension.php$/');
        });
        $iterator = 0;
        $installedPlugins = (string)file_get_contents(Installer::FILE);
        $installedPlugins = \json_decode($installedPlugins, true);
        foreach ($classMap as $class => $_) {
            $reflection = ClassType::from($class);
            if (
                $reflection->implementsInterface(IPlugin::class) &&
                $reflection->isInstantiable()
            ) {
                $pluginExtension = new $class();
                $plugin = $pluginExtension->getPlugin();
                $identifier = $plugin->getIdentifier();
                if ($reflection->implementsInterface(IInstallable::class)) {
                    $statement = new Statement($pluginExtension->getInstallClass(), [$plugin]);
                    $this->compiler::loadDefinitions($cb, [$statement], $this->prefix($iterator));

                    $this->installClasses[$identifier] = $pluginExtension->getInstallClass();
                } else {
                    $this->installClasses[] = null;
                }
                $this->availablePlugins[$identifier] = $plugin;
                if (array_key_exists($identifier, $installedPlugins) && $installedPlugins[$identifier] === $plugin->getVersion()) {
                    $this->compiler->addExtension($plugin->getIdentifier(), $pluginExtension);
                }
            }
            $iterator++;
        }
    }

    public function beforeCompile(): void
    {
        $cb = $this->getContainerBuilder();
        $definition = $cb->getDefinitionByType(AllPluginsContainer::class);
        $definition->addSetup('setAllPlugins', [$this->availablePlugins]);
        $definition->addSetup('setInstallClasses', [$this->installClasses]);
        FileSystem::purge($this->wwwDir . '/plugins', false, true); //TODO purge only directories if it is needed
    }

}
