<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Plugins\Api;

use Monolog\Logger;
use Nette\SmartObject;
use Nette\Utils\Json;
use Plugins\Plugin;

class PluginsApi
{

    use SmartObject;

    /** @var ApiClient */
    private $client;

    /** @var Logger */
    private $monolog;

    public function __construct(ApiClient $apiClient, Logger $monolog)
    {
        $this->client = $apiClient;
        $this->monolog = $monolog;
    }

    /**
     * @return array&Plugin[]
     * @throws Exceptions\ApiException
     * @throws \Nette\Utils\JsonException
     */
    public function listPlugins(): array
    {
        $response = (string)$this->client->get('plugins')->getBody();
        /** @var array $pluginsJson */
        $pluginsJson = Json::decode(Json::decode($response), Json::FORCE_ARRAY);
        $plugins = [];
        foreach ($pluginsJson as $plugin) {
            try {
                $plugins[] = Plugin::fromArray($plugin, true);
            } catch (\InvalidArgumentException $_) {
                $this->monolog->addError('Remote server returns plugin with too few info', [$plugin]);
            }
        }
        return $plugins;
    }

}
