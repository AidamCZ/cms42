<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Plugins\Api;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\HandlerStack;
use kamermans\OAuth2\GrantType\ClientCredentials;
use kamermans\OAuth2\OAuth2Middleware;
use kamermans\OAuth2\Persistence\FileTokenPersistence;
use Monolog\Logger;
use Plugins\Api\Exceptions\ApiException;
use Psr\Http\Message\ResponseInterface;

class ApiClient extends Client
{

    /** @var Logger */
    private $monolog;

    public function __construct(string $clientId, string $secret, Logger $monolog)
    {
        // Authorization client - this is used to request OAuth access tokens
        $reauthClient = new Client([
            // URL for access_token request
            'base_uri' => 'https://plugins-center.adamzelycz.cz/oauth2/access-token',
//            'base_uri' => 'http://plugins-center_nginx/oauth2/access-token',
            'verify' => true,
        ]);
        $reauthConfig = [
            'client_id' => $clientId,
            'client_secret' => $secret,
//            'scope' => 'your scope(s)',
//            'state' => time(),
        ];
        $grantType = new ClientCredentials($reauthClient, $reauthConfig);
        $oauth = new OAuth2Middleware($grantType);
        $oauth->setTokenPersistence(new FileTokenPersistence(__DIR__ . '/token.json')); //TODO custom (safe) persistence

        $stack = HandlerStack::create();
        $stack->push($oauth);

        parent::__construct([
            'base_uri' => 'https://plugins-center.adamzelycz.cz/api/', //Docker container
//            'base_uri' => 'http://plugins-center_nginx/api/',
            'timeout' => 5.0,
//            'allow_redirects' => [
//                'max' => 20,
//                'track_redirects' => true
//            ],
//            'verify' => '/etc/nginx/ssl/dhparam.pem',
//            'verify' => __DIR__ . '/../../../docker/nginx/ssl/dhparam.pem'
            'verify' => true,
            'auth' => 'oauth',
            'handler' => $stack,
        ]);
        $this->monolog = $monolog;
    }

    /**
     * @inheritdoc
     *
     * @throws ApiException
     */
    public function get($uri, array $options = []): ResponseInterface
    {
        try {
            return parent::get($uri, $options);
        } catch (GuzzleException $exception) {
            $this->monolog->addError($exception->getMessage(), [$uri, $options]);
            throw new ApiException('Error while contacting remote API server. Error was logged');
        }
    }

}
