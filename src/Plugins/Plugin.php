<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Plugins;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
use Kdyby\Doctrine\Entities\MagicAccessors;

/**
 * @ORM\Entity
 * @ORM\Table(name="plugins")
 * @method setDescription(string $description)
 * @method setVersion(string $version)
 * @method setAuthor(string $author)
 * @method string getAuthor()
 * @method setName(string $name)
 * @method string getName()
 * @method string getVersion()
 * @method setId(int $id) TODO useless
 * @method int getId()
 *
 * TODO category, ranking
 */
class Plugin implements JsonSerializable
{

    use MagicAccessors;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue()
     * @var int
     */
    protected $id;

    /**
     * @ORM\Column(type="string", options={"comment"="Název pluginu"})
     * @var string
     */
    protected $name;

    /**
     * TODO ValueObject and split to Vendor table and package column and version
     * @ORM\Column(type="string", unique=true, options={"comment"="Jednoznačný identifikátor pluginu (název)"})
     * @var string
     */
    protected $identifier;

    /**
     * @ORM\Column(type="text", options={"comment"="Popis pluginu"})
     * @var string
     */
    protected $description;

    /**
     * @ORM\Column(type="string", options={"comment"="Přezdívka autora"})
     * @var string
     */
    protected $author;

    /**
     * @ORM\Column(type="string", options={"comment"="Verze pluginu ve formátu [1.0]"})
     * @var string
     */
    protected $version;

    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    public function setIdentifier(string $vendor, string $package): void
    {
        $pattern = '/^[a-z][\-]?([a-z0-9][\-]?)+[a-z0-9]$/';
        if (!preg_match($pattern, $vendor) || !preg_match($pattern, $package)) {
            throw new \InvalidArgumentException(sprintf('%s or %s does not match following pattern: %s',
                $vendor, $package, $pattern));
        }
        $this->identifier = $vendor . '/' . $package;
    }

    public function getVendor(): string
    {
        return $this->cutIdentifier()[0];
    }

    public function getPackage(): string
    {
        return $this->cutIdentifier()[1];
    }

    /** @return string[] */
    private function cutIdentifier(): array
    {
        return explode('/', $this->identifier);
    }

    /** @return mixed[] */
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'identifier' => $this->identifier,
            'description' => $this->description,
            'author' => $this->author,
            'version' => $this->version,
        ];
    }

    /**
     * @param mixed[] $pluginArray
     * @param bool $withId
     * @return Plugin
     */
    public static function fromArray(array $pluginArray, bool $withId = false): self
    {
        $fields = ['name', 'description', 'identifier', 'author', 'version'];
        foreach ($fields as $field) {
            if (!array_key_exists($field, $pluginArray)) {
                throw new \InvalidArgumentException(sprintf('Plugin Array does not contains %s key', $field));
            }
        }

        $plugin = new static();
        if ($withId && array_key_exists('id', $pluginArray)) {
            $plugin->setId($pluginArray['id']);
        }
        $identifier = explode('/', $pluginArray['identifier']);
        $plugin->setIdentifier($identifier[0], $identifier[1]);
        $plugin->setDescription($pluginArray['description']);
        $plugin->setName($pluginArray['name']);
        $plugin->setAuthor($pluginArray['author']);
        $plugin->setVersion($pluginArray['version']);

        return $plugin;
    }

}
