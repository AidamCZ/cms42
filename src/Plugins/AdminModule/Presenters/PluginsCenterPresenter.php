<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Plugins\AdminModule\Presenters;

use App\AdminModule\Presenters\BasePresenter;
use App\Components\Flashes\Flashes;
use Plugins\AllPluginsContainer;
use Plugins\Api\Exceptions\ApiException;
use Plugins\Api\PluginsApi;
use Plugins\Exceptions\PluginDownloadException;
use Plugins\Plugin;
use Plugins\PluginsFacade;

class PluginsCenterPresenter extends BasePresenter
{

    /** @var PluginsApi */
    private $pluginsApi;

    /** @var PluginsFacade */
    private $pluginsFacade;

    /** @var Plugin[] */
    private $allPlugins;

    public function __construct(
        PluginsApi $pluginsApi,
        PluginsFacade $pluginsFacade,
        AllPluginsContainer $allPluginsContainer
    )
    {
        parent::__construct();
        $this->pluginsApi = $pluginsApi;
        $this->pluginsFacade = $pluginsFacade;
        $this->allPlugins = $allPluginsContainer->getAllPlugins();
    }

    public function actionDefault(): void
    {
        $this->setTitle('Plugins Center');
    }

    public function renderDefault(): void
    {
        $remotePlugins = [];
        try {
            $remotePlugins = $this->pluginsApi->listPlugins();
        } catch (ApiException $exception) {
            $this->flashMessage($exception->getMessage(), Flashes::ERROR);
        }
        $this->template->localPlugins = $this->allPlugins;
        $this->template->remotePlugins = $remotePlugins;
    }

    /** @secured */
    public function handleDownload(int $id, string $vendor, string $package): void
    {
        //TODO download in different tab
        $plugin = new Plugin();
        $plugin->setId($id);
        $plugin->setIdentifier($vendor, $package);
        //TODO version
        try {
            $this->pluginsFacade->download($plugin);
            $this->flashMessage(sprintf('Plugin %s was successfully downloaded, you can install it in Plugin Overview',
                $plugin->getIdentifier()),
                Flashes::SUCCESS);
        } catch (PluginDownloadException $exception) {
            $this->monolog->addError($exception->getMessage(), [$plugin]);
            $this->flashMessage($exception->getMessage(), Flashes::ERROR);
        }
        $this->fullRedirect('this');
    }

}
