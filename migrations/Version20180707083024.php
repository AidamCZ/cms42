<?php declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180707083024 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE stats ALTER date_time TYPE TIMESTAMP(0) WITH TIME ZONE');
        $this->addSql('ALTER TABLE stats ALTER date_time DROP DEFAULT');
        $this->addSql('COMMENT ON COLUMN stats.date_time IS \'(DC2Type:datetimetz_immutable)\'');
        $this->addSql('ALTER TABLE option_parameters ADD title VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE option_parameters DROP title');
        $this->addSql('ALTER TABLE stats ALTER date_time TYPE TIMESTAMP(0) WITHOUT TIME ZONE');
        $this->addSql('ALTER TABLE stats ALTER date_time DROP DEFAULT');
        $this->addSql('COMMENT ON COLUMN stats.date_time IS \'(DC2Type:datetime_immutable)\'');
    }
}
