<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180316155600 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE options_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE settings_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('DROP TABLE options');
        $this->addSql('CREATE TABLE settings (id INT NOT NULL, plugin_id INT DEFAULT NULL, option_key VARCHAR(255) NOT NULL, value VARCHAR(255) NOT NULL, name VARCHAR(255) DEFAULT NULL, type VARCHAR(255) NOT NULL, hint VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_E545A0C5EC942BCF ON settings (plugin_id)');
        $this->addSql('CREATE UNIQUE INDEX key_plugin ON settings (option_key, plugin_id)');
        $this->addSql('CREATE UNIQUE INDEX key_plugin_null ON settings (option_key) WHERE (plugin_id IS NULL)');
        $this->addSql('COMMENT ON COLUMN settings.option_key IS \'Key of the option\'');
        $this->addSql('COMMENT ON COLUMN settings.value IS \'Value of selected key\'');
        $this->addSql('COMMENT ON COLUMN settings.type IS \'Type of form input - possible values - text, colour\'');
        $this->addSql('COMMENT ON COLUMN settings.hint IS \'Small text, that will be displayed under input field\'');
        $this->addSql('ALTER TABLE settings ADD CONSTRAINT FK_E545A0C5EC942BCF FOREIGN KEY (plugin_id) REFERENCES plugins (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE urls RENAME COLUMN action TO url_action');
        $this->addSql('ALTER TABLE permissions ADD permission_read BOOLEAN NOT NULL');
        $this->addSql('ALTER TABLE permissions ADD permission_create BOOLEAN NOT NULL');
        $this->addSql('ALTER TABLE permissions ADD permission_update BOOLEAN NOT NULL');
        $this->addSql('ALTER TABLE permissions ADD permission_delete BOOLEAN NOT NULL');
        $this->addSql('ALTER TABLE permissions DROP read');
        $this->addSql('ALTER TABLE permissions DROP "create"');
        $this->addSql('ALTER TABLE permissions DROP update');
        $this->addSql('ALTER TABLE permissions DROP delete');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE settings_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE options_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('DROP TABLE settings');
        $this->addSql('CREATE TABLE options (id INT NOT NULL, plugin_id INT DEFAULT NULL, option_key VARCHAR(255) NOT NULL, value VARCHAR(255) NOT NULL, name VARCHAR(255) DEFAULT NULL, type VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_d035fa87ec942bcf ON options (plugin_id)');
        $this->addSql('CREATE UNIQUE INDEX key_plugin ON options (option_key, plugin_id)');
        $this->addSql('CREATE UNIQUE INDEX key_plugin_null ON options (option_key) WHERE (plugin_id IS NULL)');
        $this->addSql('COMMENT ON COLUMN options.option_key IS \'Key of the option\'');
        $this->addSql('COMMENT ON COLUMN options.value IS \'Value of selected key\'');
        $this->addSql('COMMENT ON COLUMN options.type IS \'Type of form input - possible values - text, colour\'');
        $this->addSql('ALTER TABLE options ADD CONSTRAINT fk_d035fa87ec942bcf FOREIGN KEY (plugin_id) REFERENCES plugins (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE urls RENAME COLUMN url_action TO action');
        $this->addSql('ALTER TABLE permissions ADD read BOOLEAN NOT NULL');
        $this->addSql('ALTER TABLE permissions ADD "create" BOOLEAN NOT NULL');
        $this->addSql('ALTER TABLE permissions ADD update BOOLEAN NOT NULL');
        $this->addSql('ALTER TABLE permissions ADD delete BOOLEAN NOT NULL');
        $this->addSql('ALTER TABLE permissions DROP permission_read');
        $this->addSql('ALTER TABLE permissions DROP permission_create');
        $this->addSql('ALTER TABLE permissions DROP permission_update');
        $this->addSql('ALTER TABLE permissions DROP permission_delete');
    }
}
