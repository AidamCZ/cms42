<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180625235325 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE stats ALTER user_agent TYPE TEXT');
        $this->addSql('ALTER TABLE stats ALTER user_agent DROP DEFAULT');
        $this->addSql('ALTER TABLE stats ALTER referer TYPE TEXT');
        $this->addSql('ALTER TABLE stats ALTER referer DROP DEFAULT');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE stats ALTER user_agent TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE stats ALTER user_agent DROP DEFAULT');
        $this->addSql('ALTER TABLE stats ALTER referer TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE stats ALTER referer DROP DEFAULT');
    }
}
