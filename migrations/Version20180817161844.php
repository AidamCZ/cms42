<?php declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180817161844 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE pages_locale_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE pages_locale (id INT NOT NULL, page_id INT DEFAULT NULL, locale_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, html_title VARCHAR(255) DEFAULT NULL, description VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_EC0ABD97C4663E4 ON pages_locale (page_id)');
        $this->addSql('CREATE INDEX IDX_EC0ABD97E559DFD1 ON pages_locale (locale_id)');
        $this->addSql('COMMENT ON COLUMN pages_locale.html_title IS \'Custom HTML title tag, same as title if NULL\'');
        $this->addSql('COMMENT ON COLUMN pages_locale.description IS \'Custom HTML description\'');
        $this->addSql('ALTER TABLE pages_locale ADD CONSTRAINT FK_EC0ABD97C4663E4 FOREIGN KEY (page_id) REFERENCES pages (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE pages_locale ADD CONSTRAINT FK_EC0ABD97E559DFD1 FOREIGN KEY (locale_id) REFERENCES locales (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE pages DROP title');
        $this->addSql('ALTER TABLE pages DROP html_title');
        $this->addSql('ALTER TABLE pages DROP description');
        $this->addSql('ALTER TABLE page_content DROP CONSTRAINT fk_4a5db3cc4663e4');
        $this->addSql('DROP INDEX idx_4a5db3cc4663e4');
        $this->addSql('ALTER TABLE page_content RENAME COLUMN page_id TO page_locale_id');
        $this->addSql('ALTER TABLE page_content ADD CONSTRAINT FK_4A5DB3CDEB7601E FOREIGN KEY (page_locale_id) REFERENCES pages_locale (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_4A5DB3CDEB7601E ON page_content (page_locale_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE page_content DROP CONSTRAINT FK_4A5DB3CDEB7601E');
        $this->addSql('DROP SEQUENCE pages_locale_id_seq CASCADE');
        $this->addSql('DROP TABLE pages_locale');
        $this->addSql('ALTER TABLE pages ADD title VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE pages ADD html_title VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE pages ADD description VARCHAR(255) DEFAULT NULL');
        $this->addSql('COMMENT ON COLUMN pages.html_title IS \'Custom HTML title tag\'');
        $this->addSql('COMMENT ON COLUMN pages.description IS \'Custom HTML description\'');
        $this->addSql('DROP INDEX IDX_4A5DB3CDEB7601E');
        $this->addSql('ALTER TABLE page_content RENAME COLUMN page_locale_id TO page_id');
        $this->addSql('ALTER TABLE page_content ADD CONSTRAINT fk_4a5db3cc4663e4 FOREIGN KEY (page_id) REFERENCES pages (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_4a5db3cc4663e4 ON page_content (page_id)');
    }
}
