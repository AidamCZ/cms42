<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180303222959 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE users_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE urls_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE options_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE permissions_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE resources_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE roles_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE plugins_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE locales_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE users (id INT NOT NULL, username VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, ip VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1483A5E9F85E0677 ON users (username)');
        $this->addSql('COMMENT ON COLUMN users.username IS \'Login of user\'');
        $this->addSql('COMMENT ON COLUMN users.password IS \'Hashed password of user\'');
        $this->addSql('COMMENT ON COLUMN users.ip IS \'Last Ip used for connection of user\'');
        $this->addSql('CREATE TABLE user_role (user_id INT NOT NULL, role_id INT NOT NULL, PRIMARY KEY(user_id, role_id))');
        $this->addSql('CREATE INDEX IDX_2DE8C6A3A76ED395 ON user_role (user_id)');
        $this->addSql('CREATE INDEX IDX_2DE8C6A3D60322AC ON user_role (role_id)');
        $this->addSql('CREATE TABLE urls (id INT NOT NULL, redirect_id INT DEFAULT NULL, presenter VARCHAR(255) DEFAULT NULL, action VARCHAR(255) DEFAULT NULL, url_path VARCHAR(255) NOT NULL, internal_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2A9437A1150042B7 ON urls (url_path)');
        $this->addSql('CREATE INDEX IDX_2A9437A1B42D874D ON urls (redirect_id)');
        $this->addSql('COMMENT ON COLUMN urls.presenter IS \'Destination Presenter (with modules)\'');
        $this->addSql('COMMENT ON COLUMN urls.action IS \'Destination action of Presenter\'');
        $this->addSql('COMMENT ON COLUMN urls.url_path IS \'Fake path (URL)\'');
        $this->addSql('COMMENT ON COLUMN urls.internal_id IS \'Internal id passed to the action method (optional)\'');
        $this->addSql('CREATE TABLE options (id INT NOT NULL, plugin_id INT DEFAULT NULL, option_key VARCHAR(255) NOT NULL, value VARCHAR(255) NOT NULL, name VARCHAR(255) DEFAULT NULL, type VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_D035FA87EC942BCF ON options (plugin_id)');
        $this->addSql('CREATE UNIQUE INDEX key_plugin ON options (option_key, plugin_id)');
        $this->addSql('CREATE UNIQUE INDEX key_plugin_null ON options (option_key) WHERE (plugin_id IS NULL)');
        $this->addSql('COMMENT ON COLUMN options.option_key IS \'Key of the option\'');
        $this->addSql('COMMENT ON COLUMN options.value IS \'Value of selected key\'');
        $this->addSql('COMMENT ON COLUMN options.type IS \'Type of form input - possible values - text, colour\'');
        $this->addSql('CREATE TABLE permissions (id INT NOT NULL, role_id INT DEFAULT NULL, resource_id INT DEFAULT NULL, allow BOOLEAN NOT NULL, "read" BOOLEAN NOT NULL, "create" BOOLEAN NOT NULL, "update" BOOLEAN NOT NULL, "delete" BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_2DEDCC6FD60322AC ON permissions (role_id)');
        $this->addSql('CREATE INDEX IDX_2DEDCC6F89329D25 ON permissions (resource_id)');
        $this->addSql('CREATE TABLE resources (id INT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_EF66EBAE5E237E06 ON resources (name)');
        $this->addSql('COMMENT ON COLUMN resources.name IS \'Unique resource string\'');
        $this->addSql('CREATE TABLE roles (id INT NOT NULL, parent_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_B63E2EC75E237E06 ON roles (name)');
        $this->addSql('CREATE INDEX IDX_B63E2EC7727ACA70 ON roles (parent_id)');
        $this->addSql('COMMENT ON COLUMN roles.name IS \'Název role\'');
        $this->addSql('CREATE TABLE plugins (id INT NOT NULL, name VARCHAR(255) NOT NULL, identifier VARCHAR(255) NOT NULL, description TEXT NOT NULL, author VARCHAR(255) NOT NULL, version VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_EC85F671772E836A ON plugins (identifier)');
        $this->addSql('COMMENT ON COLUMN plugins.name IS \'Název pluginu\'');
        $this->addSql('COMMENT ON COLUMN plugins.identifier IS \'Jednoznačný identifikátor pluginu (název)\'');
        $this->addSql('COMMENT ON COLUMN plugins.description IS \'Popis pluginu\'');
        $this->addSql('COMMENT ON COLUMN plugins.author IS \'Přezdívka autora\'');
        $this->addSql('COMMENT ON COLUMN plugins.version IS \'Verze pluginu ve formátu [1.0]\'');
        $this->addSql('CREATE TABLE locales (id INT NOT NULL, code VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, default_locale BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E59B54BB77153098 ON locales (code)');
        $this->addSql('ALTER TABLE user_role ADD CONSTRAINT FK_2DE8C6A3A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_role ADD CONSTRAINT FK_2DE8C6A3D60322AC FOREIGN KEY (role_id) REFERENCES roles (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE urls ADD CONSTRAINT FK_2A9437A1B42D874D FOREIGN KEY (redirect_id) REFERENCES urls (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE options ADD CONSTRAINT FK_D035FA87EC942BCF FOREIGN KEY (plugin_id) REFERENCES plugins (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE permissions ADD CONSTRAINT FK_2DEDCC6FD60322AC FOREIGN KEY (role_id) REFERENCES roles (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE permissions ADD CONSTRAINT FK_2DEDCC6F89329D25 FOREIGN KEY (resource_id) REFERENCES resources (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE roles ADD CONSTRAINT FK_B63E2EC7727ACA70 FOREIGN KEY (parent_id) REFERENCES roles (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE user_role DROP CONSTRAINT FK_2DE8C6A3A76ED395');
        $this->addSql('ALTER TABLE urls DROP CONSTRAINT FK_2A9437A1B42D874D');
        $this->addSql('ALTER TABLE permissions DROP CONSTRAINT FK_2DEDCC6F89329D25');
        $this->addSql('ALTER TABLE user_role DROP CONSTRAINT FK_2DE8C6A3D60322AC');
        $this->addSql('ALTER TABLE permissions DROP CONSTRAINT FK_2DEDCC6FD60322AC');
        $this->addSql('ALTER TABLE roles DROP CONSTRAINT FK_B63E2EC7727ACA70');
        $this->addSql('ALTER TABLE options DROP CONSTRAINT FK_D035FA87EC942BCF');
        $this->addSql('DROP SEQUENCE users_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE urls_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE options_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE permissions_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE resources_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE roles_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE plugins_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE locales_id_seq CASCADE');
        $this->addSql('DROP TABLE users');
        $this->addSql('DROP TABLE user_role');
        $this->addSql('DROP TABLE urls');
        $this->addSql('DROP TABLE options');
        $this->addSql('DROP TABLE permissions');
        $this->addSql('DROP TABLE resources');
        $this->addSql('DROP TABLE roles');
        $this->addSql('DROP TABLE plugins');
        $this->addSql('DROP TABLE locales');
    }
}
