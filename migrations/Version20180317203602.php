<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180317203602 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE pages ADD url_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE pages DROP url');
        $this->addSql('ALTER TABLE pages ADD CONSTRAINT FK_2074E57581CFDAE7 FOREIGN KEY (url_id) REFERENCES urls (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2074E57581CFDAE7 ON pages (url_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE pages DROP CONSTRAINT FK_2074E57581CFDAE7');
        $this->addSql('DROP INDEX UNIQ_2074E57581CFDAE7');
        $this->addSql('ALTER TABLE pages ADD url VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE pages DROP url_id');
    }
}
